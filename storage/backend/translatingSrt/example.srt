1
00:00:00,000 --> 00:00:02,100
Previously on "Lost"

2
00:00:03,134 --> 00:00:07,234
Ladies and gentlemen, the
survivors of Oceanic 815.

3
00:00:10,100 --> 00:00:12,100
I just killed a man
who's been perched...

4
00:00:12,200 --> 00:00:14,701
outside this facility for the last week.

5
00:00:14,801 --> 00:00:16,376
I want you to come with
me somewhere safe.

6
00:00:16,876 --> 00:00:18,617
You and I have common interests.

7
00:00:18,667 --> 00:00:21,277
We're not the only ones
who left the island.

8
00:00:21,667 --> 00:00:25,657
- We... were not supposed to leave.
- Bye, Jack.

9
00:00:25,801 --> 00:00:27,433
We have to go back, Kate.

10
00:00:27,533 --> 00:00:29,900
Hello, Jack. Did he tell you
that I was off the island?

11
00:00:30,300 --> 00:00:33,084
Yes, he did. he said
that I had to come back.

12
00:00:33,484 --> 00:00:36,167
We're gonna have to bring him, too.

13
00:00:53,500 --> 00:00:56,166
Ah. Hm. Baby's awake.

14
00:00:58,200 --> 00:01:00,033
It's your turn.

15
00:01:59,100 --> 00:02:00,734
Morning. Here you go, doc.

16
00:02:00,834 --> 00:02:02,767
I don't need a script.

17
00:02:04,367 --> 00:02:06,301
Let's go. I don't have all day.

18
00:02:07,767 --> 00:02:09,134
- Speed.
- Okay.

19
00:02:09,234 --> 00:02:11,700
Dharma orientation film number two,

20
00:02:11,800 --> 00:02:13,833
take one.

21
00:02:17,401 --> 00:02:20,034
And... action.

22
00:02:20,134 --> 00:02:22,600
Hello. I'm Dr. Marvin Candle,

23
00:02:22,700 --> 00:02:25,367
and this is the orientation
film for station two,

24
00:02:25,467 --> 00:02:26,700
The Arrow.

25
00:02:26,800 --> 00:02:29,467
Given your specific area of expertise,

26
00:02:29,567 --> 00:02:31,467
you should find it no surprise

27
00:02:31,567 --> 00:02:33,333
that this station's primary purpose

28
00:02:33,433 --> 00:02:35,467
is to develop defensive strategies

29
00:02:35,567 --> 00:02:37,101
and gather intelligence

30
00:02:37,201 --> 00:02:39,300
on the island's hostile
indigenous population--

31
00:02:39,400 --> 00:02:40,901
Dr. Chang!

32
00:02:41,001 --> 00:02:42,734
- Dr. Chang?
- Damn it! What the hell?!

33
00:02:42,834 --> 00:02:43,834
- Cut!

34
00:02:43,934 --> 00:02:45,233
Sir, we got a problem
down at The Orchid.

35
00:03:07,634 --> 00:03:09,234
Over here.

36
00:03:09,334 --> 00:03:12,234
We were cutting through the
rock, right on your specs.

37
00:03:12,334 --> 00:03:14,301
That's when the drill melted.

38
00:03:14,401 --> 00:03:16,267
- The drill melted?
- Yeah, yeah.

39
00:03:16,367 --> 00:03:18,234
3 meters from the margin
line on the plans.

40
00:03:18,334 --> 00:03:20,100
We went through six carbon drill bits,

41
00:03:20,200 --> 00:03:21,701
and the last one just
fried. Then my operator--

42
00:03:21,801 --> 00:03:23,734
My operator starts grabbing
his head and freaking out.

43
00:03:28,667 --> 00:03:30,367
We sonar-imaged the wall.

44
00:03:30,467 --> 00:03:33,467
There's an open chamber
about 20 meters in,

45
00:03:33,567 --> 00:03:36,067
behind the rock.

46
00:03:36,167 --> 00:03:38,933
There's something in there.

47
00:03:39,033 --> 00:03:41,634
And the only way to get to it

48
00:03:41,734 --> 00:03:43,800
is to lay charges here and here

49
00:03:43,900 --> 00:03:46,533
- and blast through it and take a look.
- Under no circumstances.

50
00:03:46,867 --> 00:03:50,667
This station is being built
here because of its proximity

51
00:03:51,067 --> 00:03:53,500
to what we believe to be an
almost limitless energy.

52
00:03:53,600 --> 00:03:55,967
And that energy, once we
can harness it correctly,

53
00:03:56,067 --> 00:03:59,301
it's going to allow us
to manipulate time.

54
00:03:59,401 --> 00:04:00,767
Right.

55
00:04:02,267 --> 00:04:05,001
Okay, so what? We're gonna
go back and kill Hitler?

56
00:04:05,101 --> 00:04:06,667
Don't be absurd.

57
00:04:06,767 --> 00:04:08,600
There are rules,

58
00:04:08,700 --> 00:04:10,867
rules that can't be broken.

59
00:04:13,233 --> 00:04:15,167
So what do you want me to do?

60
00:04:15,267 --> 00:04:16,801
You're gonna do nothing.

61
00:04:16,900 --> 00:04:19,933
If you drill even 1 centimeter further,

62
00:04:20,033 --> 00:04:22,233
you risk releasing that energy.

63
00:04:22,333 --> 00:04:24,233
If that's gonna happen,

64
00:04:27,367 --> 00:04:29,634
God help us all.

65
00:04:33,300 --> 00:04:36,067
All right, get him up. Let's go.

66
00:04:36,567 --> 00:04:37,900
Watch yourself!

67
00:04:38,000 --> 00:04:40,434
Sorry, sir. It won't happen again.

68
00:04:42,933 --> 00:04:45,300
All right. I'll grab
the rest of his stuff.

69
00:04:45,400 --> 00:04:47,700
Let's go.

70
00:04:52,600 --> 00:04:55,034
Did you hear that?

71
00:04:55,134 --> 00:04:58,600
Time travel. How stupid
does that guy think we are?

72
00:05:01,767 --> 00:05:03,767
<i>Let's go, guys.</i>

73
00:05:31,800 --> 00:05:33,800
Why don't you close that up now, Jack?

74
00:05:33,900 --> 00:05:35,266
Come on.

75
00:05:35,366 --> 00:05:38,266
Let's get him in the van.
It's out back.

76
00:05:38,566 --> 00:05:39,933
Where are we taking him?

77
00:05:40,033 --> 00:05:42,300
We'll worry about that
once we pick up Hugo.

78
00:05:43,200 --> 00:05:45,733
Hurley...

79
00:05:45,833 --> 00:05:48,966
is locked away in a mental institution.

80
00:05:49,066 --> 00:05:50,933
Which should make recruiting him

81
00:05:51,033 --> 00:05:53,300
considerably easier than
the rest of your friends.

82
00:05:53,400 --> 00:05:55,933
They're not my friends anymore.

83
00:05:56,033 --> 00:05:58,467
Oh, that's the spirit.

84
00:06:00,900 --> 00:06:03,033
How did we get here?

85
00:06:05,900 --> 00:06:07,799
How did all this happen?

86
00:06:09,899 --> 00:06:12,699
It happened because you left, Jack.

87
00:06:15,933 --> 00:06:18,233
Now let's get started, shall we?

88
00:06:29,766 --> 00:06:32,533
So once we get Hurley... then what?

89
00:06:32,632 --> 00:06:36,200
Then we get Sun, Sayid...

90
00:06:36,300 --> 00:06:37,866
and Kate, of course.

91
00:06:37,966 --> 00:06:40,699
I don't see that happening.

92
00:06:47,566 --> 00:06:50,400
When was the last time you saw him?

93
00:06:50,500 --> 00:06:53,067
I mean, Locke.

94
00:06:55,366 --> 00:06:57,567
On the island...

95
00:06:59,367 --> 00:07:02,500
In the Orchid station
below the greenhouse.

96
00:07:02,700 --> 00:07:04,600
I told him I was sorry

97
00:07:04,700 --> 00:07:07,133
for making his life so miserable,

98
00:07:07,233 --> 00:07:08,833
and then he left.

99
00:07:14,267 --> 00:07:17,400
So obviously John's visit
to you made an impression.

100
00:07:17,500 --> 00:07:19,733
What did he say to make
you such a believer?

101
00:07:24,367 --> 00:07:26,100
Sawyer, Juliet,

102
00:07:27,233 --> 00:07:30,966
everyone from the boat...
and everyone we left behind--

103
00:07:31,066 --> 00:07:35,600
John said that they'd die,
too, if I didn't come back.

104
00:07:38,266 --> 00:07:41,434
Did he tell you what happened
to them after the island moved?

105
00:07:43,333 --> 00:07:45,566
No.

106
00:07:45,666 --> 00:07:47,266
No, he didn't.

107
00:07:49,299 --> 00:07:51,233
Then I guess we'll never know.

108
00:08:53,100 --> 00:08:54,267
What the...

109
00:08:55,933 --> 00:08:57,033
Richard...

110
00:08:58,733 --> 00:09:00,400
Richard?

111
00:09:02,466 --> 00:09:03,700
Anyone?

112
00:09:07,967 --> 00:09:10,433
Anyone?

113
00:09:14,100 --> 00:09:15,866
What happened?

114
00:09:16,966 --> 00:09:19,100
What was that light?

115
00:09:21,233 --> 00:09:23,767
We must've been inside the radius.

116
00:09:31,833 --> 00:09:33,797
What the hell was that?

117
00:09:33,900 --> 00:09:35,800
I don't know.

118
00:09:41,134 --> 00:09:43,434
Where's the freighter?

119
00:09:47,367 --> 00:09:49,200
- Maybe it went down.
- Unh-unh. No way.

120
00:09:49,900 --> 00:09:51,300
A minute ago, that boat
was coughing black smoke.

121
00:09:51,400 --> 00:09:53,467
Now there's just nothin'?

122
00:09:54,367 --> 00:09:56,233
What about the helicopter?

123
00:09:57,966 --> 00:10:00,133
It was heading for that boat.

124
00:10:02,667 --> 00:10:04,366
Rose!

125
00:10:04,466 --> 00:10:06,034
Rose!