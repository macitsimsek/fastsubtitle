<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Functions;
use Carbon\Carbon;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username'          => str_replace('.','',$faker->unique()->userName),
        'memberType'        => $faker->randomElement(['normalMember','translatorMember','controllerMember','admin']),
        'position'          => $faker->randomElement(['newTranslator','goodTranslator','bestTranslator','excellentTranslator']),
        'profilePicture'    => rand(1,4).'.jpg',
        'translateCount'    => rand(1,100),
        'password'          => md5('123456'),
        'email'             => $faker->unique()->email,
        'ipAdress'          => $faker->ipv4,
        'approved'          => 1
    ];
});

$factory->define(App\VideoProperty::class, function (Faker\Generator $faker) {
    return [
        'type'          => $faker->randomElement(['serie','movie','anime']),
        'name'          => $faker->unique()->name,
        'turkishName'   => $faker->unique()->text(20),
        'kind'          => $faker->firstName.', '.$faker->firstName.' ,'.$faker->firstName,
        'imdbPoint'     => rand(7,10),
        'duration'      => rand(80,150),
        'country'       => $faker->country,
        'dateVision'    => $faker->date(),
        'abstract'      => $faker->text(800),
        'trailer'       => $faker->randomElement(['https://www.youtube.com/embed/m5Jmh9JKnyQ','https://www.youtube.com/embed/RxK48ixsq-A']),
        'picture'       => Functions::downloadPicture($faker->imageUrl(175,250),$faker->unique()->name,'videoImages'),
        'vote'          => rand(5,10),
        'view'          => rand(1000,10000)
    ];
});

$factory->define(App\People::class, function (Faker\Generator $faker) {
    return [
        'personName'        => $faker->unique()->name,
        'personPicture'     => Functions::downloadPicture($faker->imageUrl(175,250),$faker->unique()->name,'peopleImages'),
        'content'           => $faker->text(500)
    ];
});

$factory->define(App\VideoPeople::class, function (Faker\Generator $faker) {
    $rol=$faker->randomElement(['actor','director']);
    return [
        'videoID'   => rand(1,10),
        'personID'  => rand(1,20),
        'job'       => $rol,
        'rol'       => ($rol)=='actor' ? $faker->name:''
    ];
});

$factory->define(App\Subtitle::class, function (Faker\Generator $faker) {
    $type = $faker->randomElement(['serie','movie']);
    return [
        'uploaderID'            =>  rand(1,10),
        'translatingSrtPath'    =>  'example.srt',
        'translatedSrtPath'     =>  '',
        'lastUpdaterID'         =>  rand(1,10),
        'requestCount'          =>  rand(10,200),
        'progress'              =>  rand(0,99),
        'fromLanguage'          =>  $faker->randomElement(['turkish','english','french','spanish','italian','german']),
        'toLanguage'            =>  $faker->randomElement(['turkish','english','french','spanish','italian','german']),
        'videoID'               =>  rand(1,10),
        'season'                =>  $type== 'movie'? rand(1,10) : null,
        'part'                  =>  $type== 'movie'? rand(1,20) : null,
        'partTrailer'           =>  null
    ];
});

$factory->define(App\SubtitleRequester::class, function (Faker\Generator $faker) {
    return [
        'subtitleID'            =>  rand(1,400),
        'userID'                =>  rand(1,10)
    ];
});

$factory->define(App\SubtitleSupport::class, function (Faker\Generator $faker) {
    return [
        'fromLanguage'  =>  $faker->randomElement(['turkish','english','german']),
        'toLanguage'    =>  $faker->randomElement(['turkish','english','german']),
        'sentence'      =>  $faker->unique()->sentence,
        'mean'          =>  $faker->unique()->sentence,
        'createTime'    =>  Carbon::now()
    ];
});

$factory->define(App\VideoComment::class, function (Faker\Generator $faker) {
    return [
        'videoID'       =>  rand(1,12),
        'commenterID'   =>  rand(1,10),
        'comment'       =>  $faker->unique()->sentence,
        'like'          =>  rand(1,20),
        'unlike'        =>  rand(1,4)
    ];
});