<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('messageID')->unsigned();
            $table->integer('senderID')->unsigned();
            $table->foreign('senderID')->references('userID')->on('users')->onDelete('cascade');
            $table->integer('receiverID')->unsigned();
            $table->foreign('receiverID')->references('userID')->on('users')->onDelete('cascade');
            $table->text('message');
            $table->enum('seen', [0, 1])->default(0);
            $table->timestamp('messageDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
