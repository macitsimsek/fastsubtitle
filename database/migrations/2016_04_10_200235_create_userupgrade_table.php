<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserupgradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userupgrade', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('upgradeID')->unsigned();
            $table->integer('userID')->unsigned();
            $table->foreign('userID')->references('userID')->on('users')->onDelete('cascade');
            $table->enum('memberType', ['translatorMember','controllerMember','controllerTranslatorMember'])->default('translatorMember');
            $table->string('subtitleName',255);
            $table->enum('success', [0,1])->default(0);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userupgrade');
    }
}
