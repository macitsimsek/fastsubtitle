<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('userID')->unsigned();
            $table->enum('memberType', ['normalMember', 'translatorMember','controllerMember','controllerTranslatorMember','admin'])->default('normalMember');
            $table->string('username',16)->unique();
            $table->string('password',255);
            $table->string('email',255)->unique();
            $table->string('ipAdress',25);
            $table->string('profilePicture')->default('2.jpg');
            $table->enum('position', ['member', 'newTranslator','goodTranslator','bestTranslator','professionnel Translator','expertTranslator'])->default('member');
            $table->integer('translateCount')->unsigned();
            $table->enum('ban', [0, 1])->default(0);
            $table->enum('approved', [0, 1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
