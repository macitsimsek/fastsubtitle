<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideopropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videoproperties', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('videoID')->unsigned();
            $table->enum('type', ['serie','movie','anime']);
            $table->string('name');
            $table->string('turkishName');
            $table->string('kind');
            $table->float('imdbPoint')->unsigned();
            $table->integer('duration')->unsigned();
            $table->string('country');
            $table->integer('dateVision');
            $table->mediumText('abstract');
            $table->text('trailer');
            $table->text('picture');
            $table->float('vote')->default('0')->unsigned();;
            $table->integer('view')->default('0')->unsigned();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videoproperties');
    }
}
