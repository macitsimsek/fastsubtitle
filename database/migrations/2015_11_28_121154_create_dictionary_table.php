<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDictionaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionary', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('wordID');
            $table->string('word',50);
            $table->string('mean',50);
            $table->enum('fromLanguage', ['turkish','english','french','spanish','italian','german']);
            $table->enum('toLanguage', ['turkish','english','french','spanish','italian','german']);
            $table->integer('adderID')->unsigned();
            $table->foreign('adderID')->references('userID')->on('users')->onDelete('cascade');;
            $table->timestamp('addDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary');
    }
}
