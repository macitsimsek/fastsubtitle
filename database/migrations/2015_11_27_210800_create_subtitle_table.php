<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitle', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('subtitleID')->unsigned();
            $table->integer('requesterID')->unsigned();
            $table->foreign('requesterID')->references('userID')->on('users')->onDelete('cascade');
            $table->text('translatedSrtPath');
            $table->integer('subtitleReadyID')->unsigned();
            $table->foreign('subtitleReadyID')->references('subtitleReadyID')->on('subtitleready')->onDelete('cascade');
            $table->integer('lastUpdaterID')->unsigned();
            $table->foreign('lastUpdaterID')->references('userID')->on('users')->onDelete('cascade');
            $table->integer('progress')->default(0);
            $table->integer('requestCount')->default(0);
            $table->enum('problem', [0,1])->default(0);
            $table->enum('isStarted', [0,1])->default(0);
            $table->integer('translatedDownloads')->unsigned()->default(0);
            $table->enum('toLanguage', ['turkish','english','french','spanish','italian','german']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitle');
    }
}
