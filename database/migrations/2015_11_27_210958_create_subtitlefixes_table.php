<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitlefixesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitlefixes', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('subtitleFixID')->unsigned();
            $table->integer('subtitleID')->unsigned();
            $table->foreign('subtitleID')->references('subtitleID')->on('subtitle')->onDelete('cascade');
            $table->integer('rowNumber')->unsigned();
            $table->string('newMean',255);
            $table->timestamp('createTime');
            $table->integer('adderID')->unsigned();
            $table->foreign('adderID')->references('userID')->on('users')->onDelete('cascade');
            $table->enum('bestTranslate', [0,1])->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitlefixes');
    }
}
