<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideocommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videocomments', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('commentID')->unsigned();
            $table->integer('videoID')->unsigned();
            $table->foreign('videoID')->references('videoID')->on('videoproperties')->onDelete('cascade');;
            $table->integer('commenterID')->unsigned();
            $table->foreign('commenterID')->references('userID')->on('users')->onDelete('cascade');;
            $table->text('comment');
            $table->integer('like')->unsigned()->default(0);
            $table->integer('unlike')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videocomments');
    }
}
