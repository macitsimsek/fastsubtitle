<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideofollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videofollowers', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('followID')->unsigned();
            $table->integer('videoID')->unsigned();
            $table->foreign('videoID')->references('videoID')->on('videoproperties')->onDelete('cascade');
            $table->integer('followerID')->unsigned();
            $table->foreign('followerID')->references('userID')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videofollowers');
    }
}
