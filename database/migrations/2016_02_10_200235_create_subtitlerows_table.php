<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitlerowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitlerows', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('subtitleRowID')->unsigned();
            $table->integer('subtitleID')->unsigned();
            $table->foreign('subtitleID')->references('subtitleID')->on('subtitle')->onDelete('cascade');
            $table->integer('rowNumber')->unsigned();
            $table->string('duration',50);
            $table->string('sentence',255);
            $table->enum('canUpdate', [0,1])->default(0);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitlerows');
    }
}
