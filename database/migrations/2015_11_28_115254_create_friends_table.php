<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('friendID');
            $table->integer('senderID')->unsigned();
            $table->foreign('senderID')->references('userID')->on('users')->onDelete('cascade');;
            $table->integer('receiverID')->unsigned();
            $table->foreign('receiverID')->references('userID')->on('users')->onDelete('cascade');;
            $table->enum('approve', [0,1])->default(0);
            $table->timestamp('approveDate')->default(null);
            $table->timestamp('sendDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}
