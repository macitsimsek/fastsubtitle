<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitlerequestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitlerequesters', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('requestID')->unsigned();
            $table->integer('subtitleID')->unsigned();
            $table->foreign('subtitleID')->references('subtitleID')->on('subtitle')->onDelete('cascade');
            $table->integer('userID')->unsigned();
            $table->foreign('userID')->references('userID')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitlerequesters');
    }
}
