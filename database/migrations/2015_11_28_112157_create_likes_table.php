<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('likeID')->unsigned();
            $table->integer('videoID')->unsigned();
            $table->foreign('videoID')->references('videoID')->on('videoproperties')->onDelete('cascade');;
            $table->integer('commentID')->unsigned();
            $table->foreign('commentID')->references('commentID')->on('videocomments')->onDelete('cascade');;
            $table->integer('likerID')->unsigned();
            $table->foreign('likerID')->references('userID')->on('users');
            $table->timestamp('likeDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
