<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitleReadyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitleready', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('subtitleReadyID');
            $table->integer('uploaderID')->unsigned();
            $table->foreign('uploaderID')->references('userID')->on('users')->onDelete('cascade');
            $table->text('srtPath');
            $table->integer('downloads')->unsigned()->default(0);
            $table->enum('language', ['turkish','english','french','spanish','italian','german']);
            $table->integer('videoID')->unsigned();
            $table->foreign('videoID')->references('videoID')->on('videoproperties')->onDelete('cascade');
            $table->integer('season')->length(2)->unsigned()->nullable();
            $table->integer('part')->length(2)->unsigned()->nullable();
            $table->text('partTrailer')->nullable();
            $table->timestamp('created_at');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitleready');
    }
}
