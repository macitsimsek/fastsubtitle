<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitlesupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitlesupports', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('subtitleSupportID')->unsigned();
            $table->enum('fromLanguage', ['turkish','english','french','spanish','italian','german']);
            $table->enum('toLanguage', ['turkish','english','french','spanish','italian','german']);
            $table->string('sentence',255)->index();
            $table->string('mean',255)->index();
            $table->timestamp('createTime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitlesupports');
    }
}
