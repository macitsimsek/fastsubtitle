<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideopeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videopeople', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('videoPeopleID')->unsigned();
            $table->integer('videoID')->unsigned();
            $table->foreign('videoID')->references('videoID')->on('videoproperties')->onDelete('cascade');
            $table->integer('personID')->unsigned();
            $table->foreign('personID')->references('personID')->on('people')->onDelete('cascade');
            $table->enum('job',['actor','director','scenarist'])->nullable();;
            $table->string('rol')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videopeople');
    }
}
