<?php

use App\People;
use App\Subtitle;
use App\SubtitleRequester;
use App\SubtitleRows;
use App\SubtitleSupport;
use App\User;
use App\VideoComment;
use App\VideoPeople;
use App\VideoProperty;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // $this->call(UserTableSeeder::class);

        //DB::table('users')->delete();

        //Storage::delete(Storage::allFiles(public_path().'/backend/peopleImages'));
        //Storage::delete(Storage::allFiles(public_path().'/backend/videoImages'));


        User::truncate();

        factory(User::class,10)->create();

        VideoProperty::truncate();

        factory(VideoProperty::class,10)->create();
        People::truncate();

        factory(People::class,20)->create();

        VideoPeople::truncate();

        factory(VideoPeople::class,20)->create();

        Subtitle::truncate();

        factory(Subtitle::class,400)->create();

        SubtitleRequester::truncate();

        factory(SubtitleRequester::class,1000)->create();

        Subtitle::truncate();

        factory(Subtitle::class,400)->create();

        SubtitleSupport::truncate();

        factory(SubtitleSupport::class,1000)->create();


        VideoComment::truncate();

        factory(VideoComment::class,500)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
