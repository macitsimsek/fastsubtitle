	var color 		= "bgm-red";
	var color2 		= "bgm-lightgreen";
    var title 		= "Uygun! ";
	var title2 		= "Uygun Değil! ";
	var message 	= "Kullanıcı Adı 6-16 Karakter Arasında Olmalı";
	var message2 	= "Kullanıcı Adınız Giriniz";
	var message3 	= "Şifre 6-16 Karakter Arasında Olmalı";
	var message4 	= "Şifrenizi Giriniz";
	var message5 	= "Geçersiz Kullanıcı Adı";
	var message6 	= "Geçersiz Şifre";
	var message7	= "Email Adresinizi Giriniz";
	var message8 	= "Geçersiz Email Adresi";
	var message9 	= "Email Adresi Doğru";
	var time 		= 3500;
	var hatasiz 	= 0;

    var pathname= location.protocol + '//' +window.location.host+'/';

    function changepass(){
        $("form a:has(i)").html("").addClass("disabled").append('<i class="md glyphicon glyphicon-refresh spinning"></i>');
        var frm = $("#form-changepass").serialize();
        $.ajax({
            url: pathname+"changepass", type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: frm,
            success:function(data){
                $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                if(data=="true")
                {
                    swal("Şifre Başarıyla Değiştirildi.", "Yönlendiriliyor...", "success");
                    $("#l-changepass").toggle().delay(1100).fadeOut(200,function(){
                        location.href='/';
                    });
                }
                else
                {
                    if($.isArray(data)){
                        $.each(data,function(i,donenveri){
                            notify('', donenveri, color, time);
                            time += 1000;
                        });
                    }
                    else{
                        notify('', data, color, time);
                    }
                }
            },error:function(data){
                $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                if($.isArray(data)){
                    $.each(data,function(i,donenveri){
                        notify('', donenveri, color, time);
                        time += 1000;
                    });
                }
                else{
                    notify('', data, color, time);
                }
            }
        }); //end of ajax
    }

    function login(){
        $("form a:has(i)").html("").addClass("disabled").append('<i class="md glyphicon glyphicon-refresh spinning"></i>');
        var frm = $("#form-login").serialize();
        $.ajax({
            url: pathname+"userlogin",
            type:"POST",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);

                }
            },
            data: frm,
            success:function(data){
                if(data=="true")
                {
                    swal("Giriş Başarılı.", "Yönlendiriliyor...", "success");
                    $("#l-login").toggle().delay(1100).fadeOut(200,function(){
                        location.href='/';
                    });
                }
                else
                {
                    $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                    if($.isArray(data)){
                        $.each(data,function(i,donenveri){
                            notify('', donenveri, color, time);
                            time += 1000;
                        });
                    }
                    else{
                        notify('', data, color, time);
                    }
                }
            },error:function(data){
                $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                if($.isArray(data)){
                    $.each(data,function(i,donenveri){
                        notify('', donenveri, color, time);
                        time += 1000;
                    });
                }
                else{
                    notify('', data, color, time);
                }
            }
        }); //end of ajax
    }

    function register(){
        $("form a:has(i)").html("").addClass("disabled").append('<i class="md glyphicon glyphicon-refresh spinning"></i>');
        var frm = $("#form-register").serialize();
        $.ajax({
            url: pathname+"createuser",
            type:"POST",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);

                }
            },
            data: frm,
            success:function(data){
                if(data=="true")
                {
                    swal("Üye Olundu!", "Üye olurken verdiğiniz email adresinize üyelik doğrulama linki gönderilmiştir. Email adresinizi kontrol ediniz.", "success");
                    $("#l-register").toggle().delay(300).fadeOut(200);
                    $( "button[class^='confirm']" ).on({
                        click: function() {
                            location.href='/user/login';
                        }
                    });
                }
                else
                {
                    $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                    if($.isArray(data)){
                        $.each(data,function(i,donenveri){
                            notify('', donenveri, color, time);
                            time += 1000;
                        });
                    }
                    else{
                        notify('', data, color, time);
                    }
                }
            },error:function(data){
                $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                if($.isArray(data)){
                    $.each(data,function(i,donenveri){
                        notify('', donenveri, color, time);
                        time += 1000;
                    });
                }
                else{
                    notify('', data, color, time);
                }
            }
        }); //end of ajax
    }

    function forgot(){
        $("form a:has(i)").html("").addClass("disabled").append('<i class="md glyphicon glyphicon-refresh spinning"></i>');
        var frm = $("#form-forgot-password").serialize();
        $.ajax({
            url: pathname+"forgotpass",
            type:"POST",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);

                }
            },
            data: frm,
            success:function(data){
                if(data=="true")
                {
                    swal("Şifre Sıfırlama Linki Email Adresine Gönderildi.!", "Emailinizi Kontrol Ediniz.", "success");
                    $("#l-forgot-password").toggle().delay(300).fadeOut(200);
                    $( "button[class^='confirm']" ).on({
                        click: function() {
                            location.href='/user/login';
                        }
                    });
                }
                else
                {
                    $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                    if($.isArray(data)){
                        $.each(data,function(i,donenveri){
                            notify('', donenveri, color, time);
                            time += 1000;
                        });
                    }
                    else{
                        notify('', data, color, time);
                    }
                }
            },error:function(data){
                $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                if($.isArray(data)){
                    $.each(data,function(i,donenveri){
                        notify('', donenveri, color, time);
                        time += 1000;
                    });
                }
                else{
                    notify('', data, color, time);
                }
            }
        }); //end of ajax
    }

    function newpass(){
        $("form a:has(i)").html("").addClass("disabled").append('<i class="md glyphicon glyphicon-refresh spinning"></i>');
        var frm = $("#form-newpass").serialize();
        $.ajax({
            url: pathname+"newpass",
            type:"POST",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);

                }
            },
            data: frm,
            success:function(data){
                if(data=="true")
                {
                    swal("Şifre Başarıyla Yenilendi!", "Yönlendiriliyor...", "success");
                    $("#l-newpass").toggle().delay(1100).fadeOut(200,function(){
                        location.href='/user/login';
                    });
                }
                else
                {
                    $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                    if($.isArray(data)){
                        $.each(data,function(i,donenveri){
                            notify('', donenveri, color, time);
                            time += 1000;
                        });
                    }
                    else{
                        notify('', data, color, time);
                    }
                }
            },error:function(data){
                $("form a:has(i)").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                if($.isArray(data)){
                    $.each(data,function(i,donenveri){
                        notify('', donenveri, color, time);
                        time += 1000;
                    });
                }
                else{
                    notify('', data, color, time);
                }
            }
        }); //end of ajax
    }
    $('body').on('click', '[data-fastsubtitle="userNotifications"]', function(e){
        e.preventDefault();
        var crypted =$(this);
        $.ajax({
            url: pathname+"approve-friend-request/"+crypted.val(), type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}
        }).done(function(donenVeri){
            if(donenVeri.code==200){
                notify('', donenVeri.message, color2, time);
                $('[data-fastsubtitle="notificationCount"]').remove();
                crypted.parent().parent().parent().addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                    crypted.parent().parent().parent().remove();
                });
            }else{
                notify('', donenVeri.message, color, time);
            }
        })
    });

    function notify(title, message, color, time){
				var from = $(this).attr('data-from');
                var align = $(this).attr('data-align');
                var icon = $(this).attr('data-icon');
                var type = $(this).attr('data-type');
                var animIn = $(this).attr('data-animation-in');
                var animOut = $(this).attr('data-animation-out');
				
                $.growl({
                    icon: icon,
                    title: title,
                    message: message,
                    url: ''
                },{
                        element: 'body',
                        type: type,
                        allow_dismiss: true,
                        placement: {
                                from: from,
                                align: align
                        },
                        offset: {
                            x: 20,
                            y: 85
                        },
                        spacing: 10,
                        z_index: 1031,
                        delay: time,
                        timer: 2000,
                        url_target: '_blank',
                        mouse_over: false,
                        animate: {
                                enter: animIn,
                                exit: animOut
                        },
                        icon_type: 'class',
                        template: '<div data-growl="container" class="alert col-xs-3 '+color +'" role="alert">' +
                                        '<button type="button" class="close" data-growl="dismiss">' +
                                            '<span aria-hidden="true">&times;</span>' +
                                            '<span class="sr-only">Close</span>' +
                                        '</button>' +
                                        '<span data-growl="icon"></span>' +
                                        '<span data-growl="title"></span>' +
                                        '<span data-growl="message"></span>' +
                                        '<a href="#" data-growl="url"></a>' +
                                    '</div>'
                });
            }

    function KullaniciKontrol(kullanici) {
    var desen = new RegExp(/^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){4,14}[a-zA-Z0-9]$/i);
    return desen.test(kullanici);
    }

    function EmailKontrol(email) {
    var desen = new RegExp(/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/i);
    return desen.test(email);
    }

    function register2(v1,v2,v3,v4,v5) {
        v1=v1.trim();
        v2=v2.trim();
        v3=v3.trim();
        hatasiz=0;
        if(v1=="")
        {
            notify('', message2, color, time);
        }
        else if(v1.length>=6 && v1.length<=16)
        {
            if(KullaniciKontrol(v1))
            {
                notify('', message, color2, time);
                hatasiz++;
            }
            else
            {
                notify('', message5, color, time);
            }
        }
        else
        {
            notify('', message, color, time);
        }
        time += 1000;
        if(v2=="")
        {
            notify('', message7, color, time);
        }
        else
        {
            if(EmailKontrol(v2))
            {
                notify('', message9, color2, time);
                hatasiz++;
            }
            else
            {
                notify('', message8, color, time);
            }
        }
        time += 1000;
        if(v3=="" || v4=="")
        {
            notify('', message4, color, time);
        }
        else if(v3.length>=6 && v3.length<=16 && v4.length>=6 && v4.length<=16)
        {
            if(v3==v4)
            {
                if(KullaniciKontrol(v4))
                {
                    notify('', message3, color2, time);
                    hatasiz++;
                }
                else
                {
                    notify('', message6, color, time);
                }
            }
            else
            {
                notify('', "Şifreler Uyuşmuyor", color, time);
            }
        }
        else
        {
            notify('', message3, color, time);
        }
        time += 1000;
        if(v5.checked==false)
        {
            notify('', "Kullanım Kurallarını Onaylamadınız", color, time);
        }
        else
        {
            hatasiz++;
        }

        if(hatasiz==4)
        {
            $.post( "memberPost.php", {
                v1: v1,
                v2: v2,
                v3: v3
            }, function (donenVeri) {
                if (donenVeri.indexOf("Üye Olundu.")!= -1)
                {
                    $("#l-register").remove();
                    swal("Üye Olundu!", "Üye olurken verdiğiniz email adresinize üyelik doğrulama linki gönderilmiştir. Email adresinizi kontrol ediniz.", "success");
                }
                else
                {
                    notify('', donenVeri, color, time);
                }
            });
        }
    }

    function sifreonay(s1,s2,k) {
        s1=s1.trim();
        s2=s2.trim();
        hatasiz =0;
        if(s1=="" || s2=="")
        {
            notify('', message4, color, time);
        }
        else if(s1.length>=6 && s1.length<=16 && s2.length>=6 && s2.length<=16)
        {
            if(s1==s2)
            {
                if(KullaniciKontrol(s2))
                {
                    notify('', message3, color2, time);
                    $.post( "forgetPassPost.php", {
                        s1: s1,
                        k: k
                    }, function (donenVeri) {
                        if (donenVeri.indexOf("Şifre Değiştirildi")!= -1)
                        {
                            $("#l-login").remove();
                            swal("Şifre Başarıyla Değiştirildi!", "Şimdi Üye Girişi Yapabilirsiniz.", "success");
                        }
                        else
                        {
                            notify("", donenVeri, color, time);
                        }
                    });
                }
                else
                {
                    notify('', message6, color, time);
                }
            }
            else
            {
                notify('', "Şifreler Uyuşmuyor", color, time);
            }

        }
        else
        {
            notify('', message3, color, time);
        }
    }

    function forgetpassword(v1) {
        v1=v1.trim();
        hatasiz =0;
        if(v1=="")
        {
            notify('', message7, color, time);
        }
        else
        {
            if(EmailKontrol(v1))
            {
                notify('', message9, color2, time);
                $.post( "forgetPass.php", {
                    v1: v1
                }, function (donenVeri) {
                    if (donenVeri.indexOf("Email Adresine Gönderildi")!= -1)
                    {
                        $("#l-forget-password").remove();
                        swal("Şifre Sıfırlama Linki Email Adresine Gönderildi.!", "Emailinizi Kontrol Ediniz.", "success");
                    }
                    else
                    {
                        notify("", donenVeri, color, time);
                    }
                });
            }
            else
            {
                notify('', message8, color, time);
            }
        }
    }

    function login1(v1,v2,v3) {
        v1=v1.trim();
        v2=v2.trim();
        hatasiz =0;
        if(v1=="")
        {
            notify('', message2, color, time);
        }
        else if(v1.length>=6 && v1.length<=16)
        {
            if(KullaniciKontrol(v1))
            {
                notify('', message, color2, time);
                hatasiz++;
            }
            else
            {
                notify('', message5, color, time);
            }
        }
        else
        {
            notify('', message, color, time);
        }

        time += 1000;
        if(v2=="")
        {
            notify('', message4, color, time);
        }
        else if(v2.length>=6 && v2.length<=16)
        {
            if(KullaniciKontrol(v2))
            {
                notify('', message3, color2, time);
                hatasiz++;
            }
            else
            {
                notify('', message6, color, time);
            }
        }
        else
        {
            notify('', message3, color, time);
        }
        time += 1000;

        if(hatasiz==2)
        {
            $.post( "memberLogin.php", {
                v1: v1,
                v2: v2
            }, function (donenVeri) {
                if (donenVeri.indexOf("Giriş başarılı")!= -1)
                {
                    $("#l-login").remove();
                    notify("", donenVeri, color2, time);
                }
                else
                {
                    notify("", donenVeri, color, time);
                }
            });

        }
        else
        {
            notify("", "Üye Girişi Başarısız!", color, time);
        }
    }
