var color 		= "bgm-red";
var color2 		= "bgm-lightgreen";
var title 		= "Uygun! ";
var title2 		= "Uygun Değil! ";
var message 	= "Kullanıcı Adı 6-16 Karakter Arasında Olmalı";
var message2 	= "Kullanıcı Adınız Giriniz";
var message3 	= "Şifre 6-16 Karakter Arasında Olmalı";
var message4 	= "Şifrenizi Giriniz";
var message5 	= "Geçersiz Kullanıcı Adı";
var message6 	= "Geçersiz Şifre";
var message7	= "Email Adresinizi Giriniz";
var message8 	= "Geçersiz Email Adresi";
var message9 	= "Email Adresi Doğru";
var time 		= 3500;
var hatasiz 	= 0;
var pathname= location.protocol + '//' +window.location.host+'/';
//ilk işlem video html parsing
function getVideo(){
    var imdblink = $("input[name=radios1]:checked").val();
    if (imdblink!=null)
    {
        $("#btnImdb").html("").addClass("disabled").append('Hazırlanıyor... <span class="glyphicon glyphicon-refresh spinning"></span>');
        var link="http://www.imdb.com"+imdblink;
        var IMDBID=imdblink.replace("/title/","").replace("/","");
        var tip="",kind="",country="",normalName=$("#imdbtext").val();
        var informations=new videoInformations();
        $.ajax({
            url: pathname+ "getpagehtml", type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:link}
        }).done(function(donenVeri){
            informations.Name=$(donenVeri).find('div[class^="title_wrapper"] h1').text().trim();
            if(informations.Name.indexOf('(')!=-1)informations.Name=informations.Name.substr(0,informations.Name.indexOf("(")).trim();
            var durat=$(donenVeri).find('.article div:contains("Runtime:")').text().trim().replace("Runtime:","").replace("min","").split('|')[0].trim();
            while(durat.indexOf("  ")!=-1)durat=durat.replace("  "," ");
            informations.Duration=durat;
            $(donenVeri).find('h1+div a span[class="itemprop"][itemprop="genre"]').each(function(){
                kind+=$(this).text().trim()+", ";
            });
            kind=kind.slice(0,-2);
            informations.Kind=kind;
            if($(donenVeri).find('a[title^="See more release dates"]').text().indexOf("TV Series")!=-1 && $(donenVeri).find('a[title^="See more release dates"]').text().indexOf("Animation")!=-1)
                tip="anime";
            else if($(donenVeri).find('a[title^="See more release dates"]').text().indexOf("TV Series")!=-1 && $(donenVeri).find('a[title^="See more release dates"]').text().indexOf("Animation")==-1)
                tip="serie";
            else
                tip="movie";
            informations.Type=tip;
            var visiondate =$(donenVeri).find('a[title^="See more release dates"]').text().replace("(","").toLowerCase().replace("tv series","").replace("tv movies","").replace(")","").split("-")[0].split("–")[0].trim().split(' ');
            informations.VisionDate=(visiondate.length>2)?visiondate[2]:visiondate[0];
            informations.ImdbPoint=$(donenVeri).find('span[itemprop^="ratingValue"]').text().trim();
            $(donenVeri).find('div span[itemprop="creator"][itemtype="http://schema.org/Person"] a span').each(function(){
                informations.People.push({'name':$(this).text().trim(),'rol':"","job":"director"});
            });
            $(donenVeri).find('.cast_list tr:not(tr:first)').each(function(){
                var rolFix="";
                rolFix=$(this).find('td:eq(3)').text().trim().replace("episodes","Bölüm");
                rolFix=rolFix.replace("...","");
                while(rolFix.indexOf("  ")!=-1)rolFix=rolFix.replace("  "," ");
                informations.People.push({'name':$(this).find('td:eq(1)').text().trim(),'rol':rolFix,"job":"actor"});
            });
            $(donenVeri).find('.article h2+div+div h4~a').each(function(){
                country+=$(this).text().trim()+", ";
            });
            if(informations.Duration=="" || informations.VisionDate=="" || informations.ImdbPoint=="")searchIMDBAPI(IMDBID);
            country=country.slice(0,-2);
            informations.Country=country;
            informations.Picture=$(donenVeri).find('div[class^="poster"] a img').attr('src');
            informations.TurkishName=$(donenVeri).find('.article div:contains("Also Known As")').text().trim().replace("Also Known As:","").replace("See more","").replace('»',"").trim();
            abstract(informations.Name,tip);
            $("#properties").html(form(informations.Name));
            getTrailer(informations.Name);
            $('html, body').animate({ scrollTop: $('#ilerleme').offset().top }, 800);
            $('#ilerleme').width('66%');
            $('.selectpicker').val(informations.Type).prop('selected', true);
            $('.selectpicker').selectpicker('refresh');
            $("#name").val(informations.Name);
            $("#imdbPoint").val(informations.ImdbPoint);
            $("#duration").val(informations.Duration);
            $("#country").val(informations.Country);
            $("#kind").val(informations.Kind);
            $("#dateVision").val(informations.VisionDate);
            $("#picture1").attr({'src':informations.Picture,'alt':informations.Name});
            $.each(informations.People, function (i, item) {
                people2(item);
            });
            window.history.pushState(informations.Name, "Title", pathname+'addvideo?search='+informations.Name);
        });//ajax bitiş
    }
    else
    {
        notify("", "Bir Film Yada Dizi İsmi Seçiniz.", "bgm-red", 3500);
    }

};
//video nesnesi buradan üretiliyor.
function videoInformations(){
    this.Name='';
    this.TurkishName='';
    this.Duration='';
    this.Type='';
    this.Kind='';
    this.VisionDate='';
    this.ImdbPoint='';
    this.People=new Array();
    this.Country='';
    this.Picture='';
    this.Trailer='';
    this.Abstract='';
}

//video konusu fonksiyonu
function abstract(nameOfAbstract,type){
    while(nameOfAbstract.indexOf('  ')!=-1)nameOfAbstract=nameOfAbstract.replace('  '," ");
    while(nameOfAbstract.indexOf(' ')!=-1)nameOfAbstract=nameOfAbstract.replace(' ',"+");
    if(type=="serie") type="dizi";
    else type="film";
    var ara=(nameOfAbstract);
    var token = $('meta[name="csrf_token"]').attr('content');
    $.ajax({
        url: pathname+ "api-wiki", type:"POST", beforeSend: function (xhr) {
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:ara}
    }).done(function(donenVeri){
        $("#abstract").text(donenVeri);
    }); //end of ajax
}

function people(item){
    var dizi = new Array();
    for(var i =0;i<50;i++)dizi.push("["+i+"]");
    var ara=(item['name']).replace(" ","_");
    var link="https://tr.wikipedia.org/wiki/"+ara;
    var pic="";
    var content="";
    $.ajax({
        url: pathname+ "getpagehtml", type:"POST", beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:link}
    }).done(function(donenVeri){
        pic = $(donenVeri).find('.infobox td a img').attr('src');
        if(!pic) pic ="backend/img/empty.jpg";
        else pic ="http:"+pic;
        $(donenVeri).find('p').each(function(i,data){
            if(content.length<500)content+="\n\t"+$(data).text().trim();
        });
        content="\t"+content.trim();
        $.each(dizi,function(i,data){
            content=content.replace(data,"");
        });
        content=content.replace("[kaynak belirtilmeli]","");
        var people='<div class="col-sm-3 m-t-10 m-b-10">'+
            '<div class="card">'+
            '<div class="card-header ch-alt m-b-20">'+
            '<h2>'+item['name']+'</h2><span>'+item['job']+'</span>'+
            '</div>'+
            '<a href="#" class="thumbnail">'+
            '<img src="'+pic+'" alt="'+item['name']+'" style="height:200px;">'+
            '</a>';
        if(item['job']=="actor")
            people+='<input type="text" value="'+item['rol']+'" class="form-control" placeholder="Oynadığı Rol" '+'/>';
        people+='<textarea class="form-control" rows="6" placeholder="Kişi Bilgisi">'+content+'</textarea>'+
            '</div></div>';
        if(item['job']=="actor")
            $(".col-xs-12 .row:contains('Oyuncular:') .form-group").append(people);
        else{
            $(".col-xs-12 .row:contains('Direktör:') .form-group").append(people);
        }
        var dir =   $("html").find(".col-xs-12 .row:contains('Oyuncular:') .form-group .col-sm-3").length;
        var act =   $("html").find(".col-xs-12 .row:contains('Direktör:') .form-group .col-sm-3").length;
        if(dir%4==0 && dir!=0){
            $(".col-xs-12 .row:contains('Oyuncular:') .form-group").append("<div class='row'></div>");
        }
        if(act%4==0 && act!=0){
            $(".col-xs-12 .row:contains('Direktör:') .form-group").append("<div class='row'></div>");
        }
    }).fail( function(xhr, textStatus, errorThrown) {
    }); //end of ajax
}

function people2(item){
    var dizi = new Array();
    for(var i =0;i<50;i++)dizi.push("["+i+"]");
    var ara=(item['name']),content,pic;
    $.ajax({
        url: pathname+ "api-wiki-pic", type:"POST", beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:ara}
    }).done(function(donenVeri){
        if(donenVeri!="false"){
            content=donenVeri['abstract'].trim();
            $.each(dizi,function(i,data){
                content=content.replace(data,"");
            });
            pic = donenVeri['picture'];
            content=content.replace("[kaynak belirtilmeli]","");
            var people='<div class="col-sm-3 m-t-10 m-b-10">'+
                '<div class="card">'+
                '<div class="card-header ch-alt m-b-20">'+
                '<h2>'+item['name']+'</h2><span>'+item['job']+'</span>'+
                '</div>'+
                '<a href="#" class="thumbnail">'+
                '<img src="'+pic+'" alt="'+item['name']+'" style="height:200px;">'+
                '</a>';
            if(item['job']=="actor")
                people+='<input type="text" value="'+item['rol']+'" class="form-control" placeholder="Oynadığı Rol" '+'/>';
            people+='<textarea class="form-control" rows="6" placeholder="Kişi Bilgisi">'+content+'</textarea>'+
                '</div></div>';
            if(item['job']=="actor")
                $(".col-xs-12 .row:contains('Oyuncular:') .form-group").append(people);
            else{
                $(".col-xs-12 .row:contains('Direktör:') .form-group").append(people);
            }
            var dir =   $("html").find(".col-xs-12 .row:contains('Oyuncular:') .form-group .col-sm-3").length;
            var act =   $("html").find(".col-xs-12 .row:contains('Direktör:') .form-group .col-sm-3").length;
            if(dir%4==0 && dir!=0){
                $(".col-xs-12 .row:contains('Oyuncular:') .form-group").append("<div class='row'></div>");
            }
            if(act%4==0 && act!=0){
                $(".col-xs-12 .row:contains('Direktör:') .form-group").append("<div class='row'></div>");
            }
        }
    }).fail( function(xhr, textStatus, errorThrown) {
    }); //end of ajax
}

//son işlem bilgileri veritabanına gönderen fonksiyon
function finish(){
    $("#videoEkleBtn").html("").addClass("disabled").prop("disabled", true).append('Ekleniyor... <span class="glyphicon glyphicon-refresh spinning"></span>');
    var thetrailer = $('#trailer').val();
    if(thetrailer.indexOf('watch?v=')!=-1 && thetrailer.indexOf('youtube')!=-1){
        var trailerurl="https://www.youtube.com/embed/"+thetrailer.split('watch?v=')[1];
        $('#trailer').val(trailerurl);
    }
        var finishVideo =   new videoInformations();
        $(".col-xs-12 .row:contains('Direktör:') .form-group .card").each(function(){
            finishVideo.People.push({
                "videoId":"",
                "name":$(this).find('h2:first').text().trim(),
                "job":$(this).find('span:first').text().trim(),
                "rol":"",
                "content":$(this).find('textarea').text().trim(),
                "personPicture":$(this).find('a img').attr('src').trim()
            });
        });
        $(".col-xs-12 .row:contains('Oyuncular:') .form-group .card").each(function(){
            finishVideo.People.push({
                "videoId":"",
                "name":$(this).find('h2:first').text().trim(),
                "job":$(this).find('span:first').text().trim(),
                "rol":$(this).find('input:text').val().trim(),
                "content":$(this).find('textarea').text().trim(),
                "personPicture":$(this).find('a img').attr('src').trim()
            });
        });
        finishVideo.Name=$("#name").val().trim();
        finishVideo.TurkishName=$("#trname").val().trim();
        finishVideo.ImdbPoint=$("#imdbPoint").val().trim();
        finishVideo.Duration=$("#duration").val().trim();
        finishVideo.Country=$("#country").val().trim();
        finishVideo.Kind=$("#kind").val().trim();
        finishVideo.VisionDate=$("#dateVision").val().trim();
        finishVideo.Picture=$("#picture1").attr('src').trim();
        finishVideo.Trailer=$('#trailer').val().trim();
        finishVideo.Abstract=$("#abstract").val().trim();
        finishVideo.Type=$('.selectpicker').val().trim();
        $.ajax({
            url: pathname+ "addvideo", type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {video:finishVideo}
        }).done(function(donenVeri){
            if(donenVeri.indexOf("true")!=-1)
            {
                var ID = donenVeri.split('-')[1];
                $('#ilerleme').width('100%');
                swal("Video Bilgileri Başarıyla Eklendi.!", "Şimdi video İçin Altyazı Dosyası Ekleyebilirsiniz.", "success");
                $("#videoEkleBtn").html("").append('Video Eklendi <span class="md md-check"></span>');
                var url="/video/"+beGoodSeo(finishVideo.Name)+"/"+ID;
                $('body').click(function(){
                    location.href=url;
                });
            }
            else
            {
                $("#videoEkleBtn").html("").removeClass("disabled").prop("disabled", false).append('Videoyu Ekle <span class="md md-check"></span>');
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }
                else{
                    notify('', donenVeri, color, time);
                }
            }
        }).fail(function(donenVeri){
            $("#videoEkleBtn").html("").removeClass("disabled").append('Videoyu Ekle <span class="md md-check"></span>');
            if($.isArray(donenVeri)){
                $.each(donenVeri,function(i,donenveri2){
                    notify('', donenveri2, color, time);
                    time += 1000;
                });
            }
            else{
                notify('', donenVeri, color, time);
            }
        }); //end of ajax

}
//ver,tabanına people Ekleme ve filmle bağlama fonksiyonu
function addPeople(Info){
    $.ajax({
        url: pathname+ "addpeople", type:"POST", beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: Info
    }); //end of ajax
}
//yönlendirme için link yapan fonskiyon
function beGoodSeo(url){
    url = url.trim();
    url = url.replace("ö","o");
    url = url.replace("ç","c");
    url = url.replace("ş","s");
    url = url.replace("ı","i");
    url = url.replace("ğ","g");
    url = url.replace("ü","u");
    url = url.replace("Ö","O");
    url = url.replace("Ç","C");
    url = url.replace("Ş","S");
    url = url.replace("İ","I");
    url = url.replace("Ğ","G");
    url = url.replace("Ü","U");
    url = url.replace(/[^a-zA-Z0-9]+/g, "_");
    return url;
}

function goToVideo(){
    var link = $('input[name=radios2]:checked').val();
    if(link){
        location.href=link;
    }else{
        notify('','Seçim Yapmadınız.',color,time);
    }
}
//video özelliklerinde değişiklik yapılması için yapılan form
function form(nameOfAbstract){
    return '<div class="card-header">'+
        '<h2>Video Özellikleri</h2>'+
        '</div> '+
        '<div class="card-body card-padding">'+
        '<div class="row">'+
        '<form id="gform" method="POST" action="javascript:void(0);">'+
        '<div class="col-sm-8">'+
        '<div class="form-group">'+
        '<label for="name">Video Adı:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="name" id="name" placeholder="Video Adı Giriniz" disabled>'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="trname">Türkçe Video Adı:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="trname" id="trname" value="" placeholder="Videonun Türkçe Adını Giriniz">'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="type">Tip:</label>'+
        '<div class="fg-line">'+
        '<select name="type" class="selectpicker">'+
        '<option value="serie">Dizi</option>'+
        '<option value="movie">Film</option>'+
        '<option value="anime">Anime</option>'+
        '</select>'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="kind">Tür:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="kind" id="kind" value="" placeholder="Çeşidini Giriniz" >'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="imdbPoint">İMDB Puanı:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="imdbPoint" id="imdbPoint" value="" placeholder="İMDB Puanı Giriniz">'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="duration">Süre:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="duration" id="duration" value="" placeholder="Süre Giriniz" >'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="country">Ülke:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="country" id="country" value="" placeholder="Ülke Giriniz" >'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="dateVision">Vizyon Zamanı:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="dateVision" id="dateVision" value="" placeholder="Vizyon Zamanı Giriniz" >'+
        '</div>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="abstract">Konu:</label>'+
        '<div class="fg-line">'+
        '<textarea class="form-control" name="abstract" id="abstract" rows="6" placeholder="Konu Giriniz"></textarea>'+
        '</div>'+
        '<a href="https://www.google.com/search?q='+nameOfAbstract+'&oq=google&sourceid=chrome&ie=UTF-8&gws_rd=ssl" target="_blank" class="btn btn-large btn-primary btn-success pull-right waves-effect">Search '+nameOfAbstract+'</a>'+
        '</div>'+
        '<div class="form-group">'+
        '<label for="trailer">Fragman:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="trailer" id="trailer" placeholder="Fragman Linki Giriniz">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-xs-4">'+
        '<input type="hidden" value="" name="picture" id="picture"/>'+
        '<label for="picture1">Video Görseli:</label>'+
        '<img src="" alt="" id="picture1" class="img-thumbnail center-block">'+
        '<div class="form-group">'+
        '<label for="newPic1">Yeni Resim Linki:</label>'+
        '<div class="fg-line">'+
        '<input type="text" class="form-control" name="newPic1" id="newPic1" value="" placeholder="Eklemek İstediğiniz Resim Linkini Giriniz">'+
        '</div>'+
        '</div>'+
        '<button type="button" onclick="newPic(newPic1.value)" class="btn btn-primary btn-success pull-right">'+
        'Yeni Resmi Getir'+
        '</button>'+
        '</div>'+
        '<div class="col-xs-12">'+
        '<div id="trailerPlace">'+
        '<a href="https://www.youtube.com/results?search_query='+nameOfAbstract+'+trailer" target="_blank" class="btn btn-large btn-primary bgm-orange pull-left waves-effect">Find Trailer</a>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-xs-12">'+
        '<div class="row">'+
        '<label for="scenerio">Direktör:</label>'+
        '<div class="form-group">'+
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<label for="scenerio">Oyuncular:</label>'+
        '<div class="form-group">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<button type="button" onclick="finish()" id="videoEkleBtn" class="btn btn-lg btn-success pull-right">'+
        'Videoyu Ekle <span class="md md-check"></span>'+
        '</button>'+
        '</div>'+
        '</div>'+
        '</form>'+
        '</div>'+
        '</div>';
}
//video resmini yenileyip yeni resmi güncelleyen fonksiyon
function newPic(picture){
    $('#picture').val(picture);
    $('#picture1').attr("src", picture);
}
//video arayıp sonuçları seçim için gösteren fonksiyon
function imdbAra(aranacak) {
    if (aranacak!="") {
        var count =0;
        $("#videoSearch").html("").addClass("disabled").append('<i class="glyphicon glyphicon-refresh spinning"></i>');
        aranacak    = aranacak.toLowerCase().trim();
        $.ajax({
            url: pathname+ "getfromimdb",
            type:"POST",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) { return xhr.setRequestHeader('X-CSRF-TOKEN', token);}
            },
            data: {'sorgu':aranacak},
            success:function(data){
                count++;
                if(count>=2){
                    $('#ilerleme').width('33%');
                    $("#videoSearch").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                }
                if(data) {
                    $("#aramacekimdb").html(data);
                    $('body').animate({ scrollTop: $('#aramacekimdb').offset().top-100 }, 800);
                } else {
                    notify('', data, color, time);
                }
            },error:function(data){
                count++;
                if(count>=2){
                    $('#ilerleme').width('33%');
                    $("#videoSearch").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                }
                if($.isArray(data)){
                    $.each(data,function(i,donenveri){
                        notify('', donenveri, color, time);
                        time += 1000;
                    });
                } else {
                    notify('', data, color, time);
                }
            }
        }); //end of ajax
        $.ajax({
            url: pathname+ "getfromha",
            type:"POST",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) { return xhr.setRequestHeader('X-CSRF-TOKEN', token);}
            },
            data: {'sorgu':aranacak},
            success:function(data){
                count++;
                if(count>=2){
                    $('#ilerleme').width('33%');
                    $("#videoSearch").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                }
                if(data) {
                    $("#aramacekha").html(data);
                    $('html, body').animate({ scrollTop: $('#aramacekha').offset().top }, 800);
                } else if(data==""){
                    $("#aramacekha").html('Bulunamadı.');
                }
            },error:function(data){
                count++;
                if(count>=2){
                    $('#ilerleme').width('33%');
                    $("#videoSearch").html("").removeClass("disabled").append('<i class="md md-arrow-forward"></i>');
                }
                $("#aramacekha").html('Bulunamadı.');
            }
        }); //end of ajax
    } else {
        $( "#imdbtext" ).focus();
        notify("", "Film Yada Dizi Adı Giriniz.", "bgm-red", 3500);
    }
}

//youtubeden videonun trailerini alan fonksiyon
function getTrailer(name){
    name= name.toLowerCase();
    $.ajax({
        url: pathname+ "gettrailer", type:"POST", beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {sorgu:name},
        success:function(donenVeri){
            $("#trailerPlace").html(donenVeri);
        },error:function(donenVeri){
        }
    }); //end of ajax
}
function searchIMDBAPI(id){
    $.ajax({
        url: pathname+ "api-imdb-search-with-id", type:"POST", beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:id},
        success:function(donenVeri){
            var duration =donenVeri['duration'].split(" ");
            duration = ((parseInt(duration[0].replace("h","").trim())*60)+parseInt(duration[1].replace("min","").trim()));
            if($("#imdbPoint").val().trim()=="")$("#imdbPoint").val(10);
            if($("#duration").val().trim()=="")$("#duration").val(duration);
            if($("#dateVision").val().trim()=="")$("#dateVision").val(donenVeri['released']);
        }
    }); //end of ajax
}

//video varsa kontrolünün olduğu fonksiyon işe yarayabilir kalsın
/*
 function filmiSec()
 {
 var filmismi = $("input[name=radios1]:checked").val();
 if (filmismi!=null)
 {
 $("#btnImdb").html("").addClass("disabled").append('Hazırlanıyor... <span class="glyphicon glyphicon-refresh spinning"></span>');
 $.post( "imdbInformation.php", {
 link: filmismi
 }, function (donenVeri) {
 $("#btnImdb").html("").append('Hazırlandı! <span class="md md-check"></span>');
 if (donenVeri.indexOf("zaten var")!= -1)
 {
 $('#ilerleme').width('100%');
 $("#btnImdb").html("").removeClass("disabled").append('Seç ve Devam Et <span class="md md-arrow-forward"></span>');
 notify("", "Bu Video Zaten Sistemde Bulunuyor.<br>Yönlendiriliyorsunuz...", color, time);
 }
 else
 {
 $("#properties").html(donenVeri);
 $(document).ready(function () {
 $('html, body').animate({ scrollTop: $('#ilerleme').offset().top }, 800);
 });
 $('#ilerleme').width('66%');
 }
 });
 }
 else
 {
 notify("", "Bir Film Yada Dizi İsmi Seçiniz.", "bgm-red", 3500);
 }
 }*/