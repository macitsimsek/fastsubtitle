@extends('backend.master')

@section('meta')
    <meta name="keywords" content="Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com" />
@stop

@section('title')
    {{trans('pageTranslations.home')}} - Türkçe Altyazı - {{trans('pageTranslations.domain')}}
@stop

@section('style')
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }

        .item{background:#999;text-align: center;}

    </style>
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="row">
                @if($errors->any())
                    <div class="col-sm-12">
                    <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {!!$errors->first('message')!!}
                    </div>
                    </div>
                @endif
                <div class="col-sm-8">
                    <div class="card" style="background-color: transparent">
                        <div class="card-header bgm-white">
                            <h2 class="c-black">{{trans('pageTranslations.last_added_series_movies')}}</h2>
                        </div>
                        <div class="card-body">
                            <div id="slider" class="carousel slide" data-interval="3500">
                                <ol class="carousel-indicators" style="z-index: 3;">
                                    @foreach($lastvideos as $index=>$key)
                                        <li data-target="#myCarousel" data-slide-to="{{$index}}" @if($index==0)class="active"@endif></li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    @foreach($lastvideos as $index=>$videos)
                                        <div class="item @if($index==0) active @endif">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="col-sm-5"><a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}"><img src="{{url($videoImagePath.$videos->picture)}}" alt="{{$videos->name}}" style="height: 250px;width: 200px;"> </a></div>
                                                    <div class="col-sm-7">
                                                        <div class="card-header ">
                                                            <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}"><h2 style="">{{stripslashes($videos->name)}} ({{stripslashes($videos->turkishName)}}) </h2></a>
                                                        </div>
                                                        <div class="card-body">
                                                            {!!str_limit(stripslashes($videos->abstract), $limit = 500, $end = '...')!!}
                                                            <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}">{{trans('pageTranslations.more')}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <a class="carousel-control left" href="#slider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="carousel-control right" href="#slider" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="background-color: transparent">
                        <div class="card-header bgm-white">
                            <h2 class="c-black">{{trans('pageTranslations.last_added_casts')}}</h2>
                        </div>
                        <div class="card-body">
                            <div id="sliderPeople" class="carousel slide" data-interval="4500" >
                                <ol class="carousel-indicators" style="z-index:  3;">
                                    @foreach($lastpeople as $index=>$key)
                                        <li data-target="#myCarousel" data-slide-to="{{$index}}" @if($index==0)class="active"@endif></li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    @foreach($lastpeople as $index=>$person)
                                        <div class="item @if($index==0) active @endif">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="col-sm-7">
                                                        <div class="card-header ">
                                                            <h2 style="">{{$person->personName}}</h2>
                                                        </div>
                                                        <div class="card-body">
                                                            {!!str_limit(stripslashes($person->content), $limit = 500, $end = '...')!!}
                                                            <a href="{{\App\Functions::url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}">{{trans('pageTranslations.more')}}</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5"><a href="{{\App\Functions::url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}"><img src="{{url($peopleImagePath.$person->personPicture)}}" alt="{{$person->personName}}" style="height: 250px;width: 200px;"> </a></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <a class="carousel-control left" href="#sliderPeople" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="carousel-control right" href="#sliderPeople" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bgm-orange">
                            <h2>{{trans('pageTranslations.last_added_movie_subtitles')}}</h2>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr class="text-center">
                                    <th class="text-center"></th>
                                    <th class="text-center">{{trans('pageTranslations.to')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.sender')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.movie')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.subtitles')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.check')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.sent_time')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lastMovies as $key=>$subtitle)
                                    <tr class="text-center">
                                        <td >{{$key+1}}</td>
                                        <td >{{trans('pageTranslations.'.$subtitle->language)}}</td>
                                        <td >{{$subtitle->username}}</td>
                                        <td >
                                            <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($subtitle->name)).'/'.$subtitle->videoID)}}" title="{{trans('pageTranslations.go_to_video')}}">
                                                {{stripslashes($subtitle->name)}}<i class="md-arrow-forward"></i>
                                            </a>
                                        </td>
                                        <td >
                                            <a href="{{\App\Functions::url('request/'.$subtitle->subtitleReadyID)}}">
                                                {{trans('pageTranslations.request_subtitle')}}<i class="md-plus-one"></i>
                                            </a>
                                        </td>
                                        <td >
                                            <a href="{{\App\Functions::url('/subtitle/check/'.$subtitle->subtitleReadyID)}}">
                                                {{trans('pageTranslations.subtitle_check')}}<i class="md-check"></i>
                                            </a>
                                        </td>
                                        <td >{{Date::parse($subtitle->created_at)->diffForHumans()}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="card-body text-center">
                           .
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bgm-white">
                            <h2 class="c-black">{{trans('pageTranslations.last_added_serie_subtitles')}}</h2>
                        </div>
                            <div class="table-responsive">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr class="text-center">
                                    <th class="text-center"></th>
                                    <th class="text-center">{{trans('pageTranslations.from')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.to')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.sender')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.movie')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.subtitles')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.check')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.sent_time')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lastSeries as $key=>$subtitle)
                                    <tr class="text-center">
                                        <td >{{$key+1}}</td>
                                        <td >{{trans('pageTranslations.'.$subtitle->fromLanguage)}}</td>
                                        <td >{{trans('pageTranslations.'.$subtitle->toLanguage)}}</td>
                                        <td >{{$subtitle->username}}</td>
                                        <td >
                                            <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($subtitle->name)).'/'.$subtitle->videoID)}}" title="{{trans('pageTranslations.go_to_video')}}">
                                                 {{stripslashes($subtitle->name)}}<i class="md-arrow-forward"></i>
                                            </a>
                                        </td>
                                        <td >
                                            <a href="{{\App\Functions::url('request/'.$subtitle->subtitleID)}}">
                                                {{trans('pageTranslations.request_subtitle')}}<i class="md-plus-one"></i>
                                            </a>
                                        </td>
                                        <td >
                                            <a href="{{\App\Functions::url('/subtitle/check/'.$subtitle->subtitleID)}}">
                                                {{trans('pageTranslations.subtitle_check')}}<i class="md-check"></i>
                                            </a>
                                        </td>
                                        <td >{{Date::parse($subtitle->created_at)->diffForHumans()}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                @include('backend.right')
                @include('backend.bottom')
            </div>
        </div>
    </section>
@stop
