@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{trans('pageTranslations.last_added_casts')}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{trans('pageTranslations.last_added_casts')}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/people/last"/>
@stop

@section('title')
    {{trans('pageTranslations.last_added_casts')}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h2>{{trans('pageTranslations.last_added_casts')}}</h2>
                </div>
            </div>
            @foreach($lastPeople as $person)
                <div class="card col-sm-12 card-padding">
                    <div class="col-sm-3">
                        <div class="image text-center">
                            <a href="{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}"><img src="{{url($peopleImagePath.$person->personPicture)}}" style="height:200px;width:160px;" alt="{{$person->personName}}" class="img-thumbnail"></a>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="card-header bgm-bluegray">
                            <a href="{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}"><h2>{{stripslashes($person->personName)}}</h2></a>
                            <ul class="actions actions-alt">
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                                        <i class="md md-settings"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID.'/edit')}}">{{trans('pageTranslations.edit')}}</a>
                                        </li>
                                        <li>
                                            <a href="#">{{trans('pageTranslations.report')}}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body card-padding">
                            <div class="pull-right bottom-left">
                                <button onclick="location.href='{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}'" class="btn bgm-blue btn-float waves-effect"><i class="md-arrow-forward"></i></button>
                            </div>
                            {!!str_limit(stripslashes($person->content), $limit = 300, $end = '...')!!}
                            <a href="{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}">{{trans('pageTranslations.more')}}</a>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="card col-sm-12 card-padding">
                <div class="card-body text-center">
                    {!! $lastPeople->render() !!}
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop