@extends('backend.master')

@if(Session::has('user'))

@section('meta')
    <meta name="keywords" content="{{stripslashes($title)}} - {{trans('pageTranslations.people_edit')}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{!!str_limit(stripslashes($person->content), $limit = 150, $end = '...')!!}">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makeEditPersonUrl($person->personName,$person->personID)}}"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.people_edit')}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">

            {!!Form::model($person,['method'=>'PATCH','route'=>['person.update',$person->personID]])!!}

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header bgm-cyan"><h2>{{$person->personName}} {{trans('pageTranslations.edit_page')}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                @if($errors->any())
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                {!!Form::label('personName',trans('pageTranslations.person_name'))!!}
                                <div class="fg-line">
                                    {!!Form::text('personName', null, array('class' => 'form-control', 'placeholder'=>trans('pageTranslations.enter_a_name'))) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('contents',trans('pageTranslations.content'))!!}
                                <div class="fg-line">
                                    {!!Form::textarea('content', stripslashes($person->content), array('class' => 'form-control', 'placeholder'=>trans('pageTranslations.enter_a_content')))!!}
                                </div>
                            </div>

                            <div class="btn-colors btn-demo">
                                {!!Form::button(trans('pageTranslations.update_person').' <span></span>', ['type'=>'submit','id'=>'updateBtn','class' => 'btn btn-large btn-primary bgm-indigo pull-right'])!!}
                            </div>
                            <a href="https://www.google.com/search?q={{$person->personName}}&oq=google&sourceid=chrome&ie=UTF-8&gws_rd=ssl" target="_blank" class="btn btn-large btn-primary bgm-indigo pull-left waves-effect">{{trans('pageTranslations.search',['name'=>$person->personName])}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header bgm-cyan"><h2>{{trans('pageTranslations.picture',['name'=>$person->personName])}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                {!!Form::label('personPicture',trans('pageTranslations.person_picture'))!!}
                                <div class="fg-line">
                                    {!!Form::hidden('personPicture') !!}
                                    {!!Form::image(url($peopleImagePath.$person->personPicture),'personPicture1', array('id'=>'personPicture1','class' => 'img-thumbnail center-block', 'alt'=>$person->personName,'title'=>$person->personName, 'style'=>'height: 250px;width: 175px;'))!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="newPic1">{{trans('pageTranslations.new_picture_link')}}</label>
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="newPic1" id="newPic1" placeholder="{{trans('pageTranslations.enter_a_new_picture_link')}}">
                                </div>
                            </div>

                            <button type="button" id="getPicture" class="btn btn-primary bgm-indigo pull-right waves-effect">
                                {{trans('pageTranslations.get_new_picture')}}
                            </button>
                            <a href="https://www.google.com.tr/search?q={{$person->personName}}+picture&source=lnms&tbm=isch" target="_blank" class="btn btn-large btn-primary bgm-indigo pull-left waves-effect">{{'Google '.trans('pageTranslations.find_picture')}}</a>
                        </div>
                    </div>
                </div>
            </div>

            {!!Form::close()!!}

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header bgm-cyan"><h2>{{$person->personName}} {{trans('pageTranslations.find_picture')}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div id="images"></div>
                            <div class="btn-colors btn-demo">
                                {!!Form::button(trans('pageTranslations.find_picture'), ['type'=>'button','id'=>'chooseImage','class' => 'btn btn-block btn-primary bgm-indigo pull-right'])!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bgm-cyan"><h2>{{$person->personName}} {{trans('pageTranslations.find_videos')}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div id="videos"></div>
                            <div class="btn-colors btn-demo">
                                {!!Form::button(trans('pageTranslations.find_videos'), ['type'=>'button','id'=>'findVideos','class' => 'btn btn-block btn-primary bgm-indigo pull-right'])!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')
    <script>
        $(function(){

            $('#chooseImage').click(function(){
                var personName =$('#personName').val();
                $("#images").html("");
                $.ajax({
                    url: pathname+ "api-wiki-only-pic", type:"POST", beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:personName}
                }).done(function(donenVeri){
                    if(donenVeri!="false"){
                        var people='<div class="col-sm-6 m-t-10 m-b-10">'+
                                        '<div class="card">'+
                                        '<a href="#" class="thumbnail">'+
                                        '<img src="'+donenVeri['picture']+'" alt="'+personName+'" style="height:200px;"></a>'+
                                        '<h2><input type="button" value="Seç" class="btn btn-block btn-primary bgm-indigo"/></h2>'
                                ;
                        $("#images").append(people);
                    }
                }); //end of ajax
                $.ajax({
                    url: pathname+ "api-imdb-pic", type:"POST", beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:personName}
                }).done(function(donenVeri){
                    if(donenVeri!="false"){
                        var people='<div class="col-sm-6 m-t-10 m-b-10">'+
                                '<div class="card">'+
                                '<a href="#" class="thumbnail">'+
                                '<img src="'+donenVeri['picture']+'" alt="'+personName+'" style="height:200px;"></a>'+
                                '<h2><input type="button" value="Seç" class="btn btn-block btn-primary bgm-indigo"/></h2>'
                                ;
                        $("#images").append(people);
                    }
                }); //end of ajax
            });
            $('#findVideos').click(function(){
                var personName =$('#personName').val().trim();
                $("#videos").html("");
                $.ajax({
                    url: pathname+ "api-imdb-search-with-query", type:"POST", beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {search:personName,where:'names'}
                }).done(function(donenVeri){
                    $("#videos").append(donenVeri);
                    if ($.isArray(donenVeri)){
                        $.each(donenVeri,function (i,val) {
                            var videos='<div><a target="_blank" href="https://www.fastsubtitle.com/addvideo?search='+val["title"]+'" class="btn bgm-gray">'+val["title"]+val["year"]+'<a><input type="text" value="'+val["info"]+'" class="form-control"/></div>';
                            $("#videos").append(videos);
                        });
                    }
                }); //end of ajax
            });

            $('body').on("click","input:button",function(){
                var picture=$(this).parent().parent().parent().find('img').attr('src');
                $('#personPicture').val(picture);
                $('#personPicture1').attr("src", picture).val(picture);
            });

            $('#getPicture').click(function(){
                var picture = $('#newPic1').val();
                if(picture!=""){
                    $('#personPicture').val(picture);
                    $('#personPicture1').attr("src", picture).val(picture);
                }else{
                    notify('Link can not empty','','bgm-red',1000);
                }
            });

            $('#updateBtn').click(function(){
                $('#updateBtn span').addClass("glyphicon glyphicon-refresh spinning");
                $('#updateBtn').addClass("disabled");
            });


        });
    </script>
@stop

@endif