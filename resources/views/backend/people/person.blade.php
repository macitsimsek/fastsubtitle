@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{stripslashes($title)}},Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{!!str_limit(stripslashes($person->content), $limit = 150, $end = '...')!!}">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makePersonUrl($person->personName,$person->personID)}}"/>
@stop

@section('title')
    {{stripslashes($title)}} - {{trans('pageTranslations.cast')}} - Çeviri, Türkçe Altyazı - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="col-sm-8">
                <div class="card p-10">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4 text-center" >
                                <img src="{{url(stripslashes($peopleImagePath.$person->personPicture))}}" alt="{{stripslashes($person->personName)}}" class="img-thumbnail"/>
                            </div>

                            <div class="col-sm-8">
                                <div class="card-header bgm-green">
                                    <h2>{{stripslashes($person->personName)}}</h2>
                                </div>

                            </div>

                            <div class="col-sm-8">
                                <div class="card-body card-padding">
                                    <div class="row">
                                        {{stripslashes($person->content)}}
                                        @if(Session::has('user'))
                                            <a href="{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID.'/edit')}}" class="btn btn-default bgm-green waves-effect pull-right m-t-20">{{trans('pageTranslations.edit')}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 p-b-10 p-t-10 text-right">
                                <i class="md-remove-red-eye"></i> {{$person->view}} {{trans('pageTranslations.view')}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" >
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="card-header"><h2>{{trans('pageTranslations.series_movies')}}</h2></div>
                            @foreach(array_chunk($personVideos->getCollection()->all(),3) as $row)
                            <div class="row">
                                @foreach($row as $video)
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header bgm-gray ch-alt m-b-20 text-center"><h2>{{trans('pageTranslations.'.$video->job)}} - ({{trans('pageTranslations.'.$video->type)}})</h2></div>
                                            <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID)}}" class="thumbnail">
                                                <img src="{{url(stripslashes($videoImagePath.$video->picture))}}" alt="{{stripslashes($video->name)}}">
                                            </a>
                                            <div class="card-header bgm-gray ch-alt m-b-20 text-center"><h2>{{stripslashes($video->name)}}</h2><span>{{stripslashes($video->turkishName)}}</span></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @endforeach
                            <div class="card-body text-center">
                                {!! $personVideos->render() !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @include('backend.right')
            @include('backend.bottom')
        </div>
    </section>
@stop
