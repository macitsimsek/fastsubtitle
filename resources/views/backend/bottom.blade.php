<footer>
    <div class="card col-sm-12">
        <div class="card-header">
            <h2>{{\Carbon\Carbon::now()->year}} -  {{trans('pageTranslations.domain')}}
                <small>Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir. Tüm Hakları Saklıdır. Copyright ©</small>
            </h2>
        </div>
    </div>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-50970003-2', 'auto');
        ga('send', 'pageview');

    </script>
</footer>