<div class="col-sm-4">


    @if(!empty($lastVideos))
        <div class="card">
            <div class="card-header bgm-bluegray">
                <h2>{{trans('pageTranslations.last_added_series_movies')}}</h2>

                <ul class="actions actions-alt">
                    <li>
                        <button onclick="location.href='{{\App\Functions::url('video/last')}}'" class="btn bgm-gray btn-float waves-effect"><i class="md md-send"></i></button>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="table-responsive" tabindex="3" style="overflow: hidden; outline: none;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('pageTranslations.series_movies')}}</th>
                            <th class="text-center">{{trans('pageTranslations.view')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lastVideos as $index => $video)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td><a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID)}}"> {{stripslashes($video->name)}}</a></td>
                                <td class="text-center">{{$video->view}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

    @if(!empty($popularSeries))
        <div class="card">
            <div class="card-header bgm-green">
                <h2>{{trans('pageTranslations.popular_series')}}</h2>

                <ul class="actions actions-alt">
                    <li>
                        <button onclick="location.href='{{\App\Functions::url('video/last')}}'" class="btn bgm-lightgreen btn-float waves-effect"><i class="md md-send"></i></button>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="table-responsive" tabindex="3" style="overflow: hidden; outline: none;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('pageTranslations.serie')}}</th>
                            <th class="text-center">{{trans('pageTranslations.view')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($popularSeries as $index => $video)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td><a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID)}}"> {{stripslashes($video->name)}}</a></td>
                                <td class="text-center">{{$video->view}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

    @if(!empty($popularMovies))
        <div class="card">
            <div class="card-header bgm-orange">
                <h2>{{trans('pageTranslations.popular_movies')}}</h2>

                <ul class="actions actions-alt">
                    <li>
                        <button onclick="location.href='{{\App\Functions::url('video/last')}}'" class="btn bgm-amber btn-float waves-effect"><i class="md md-send"></i></button>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="table-responsive" tabindex="3" style="overflow: hidden; outline: none;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('pageTranslations.movie')}}</th>
                            <th class="text-center">{{trans('pageTranslations.view')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($popularMovies as $index => $video)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td><a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID)}}"> {{stripslashes($video->name)}}</a></td>
                                <td class="text-center">{{$video->view}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif


    @if(!empty($popularAnimes))
        <div class="card">
            <div class="card-header bgm-blue">
                <h2>{{trans('pageTranslations.popular_animes')}}</h2>

                <ul class="actions actions-alt">
                    <li>
                        <button onclick="location.href='{{\App\Functions::url('video/last')}}'" class="btn bgm-cyan btn-float waves-effect"><i class="md md-send"></i></button>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="table-responsive" tabindex="3" style="overflow: hidden; outline: none;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('pageTranslations.anime')}}</th>
                            <th class="text-center">{{trans('pageTranslations.view')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($popularAnimes as $index => $video)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td><a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID)}}"> {{stripslashes($video->name)}}</a></td>
                                <td class="text-center">{{$video->view}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>
