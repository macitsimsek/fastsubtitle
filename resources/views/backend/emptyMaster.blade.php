<!DOCTYPE html>
<html class="ie9">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>

    <!-- Vendor CSS -->
    <link href="{{url('backend/vendors/fullcalendar/fullcalendar.css')}}" rel="stylesheet">
    <link href="{{url('backend/vendors/animate-css/animate.min.css')}}" rel="stylesheet">
    <link href="{{url('backend/vendors/sweet-alert/sweet-alert.min.css')}}" rel="stylesheet">
    <link href="{{url('backend/vendors/noUiSlider/jquery.nouislider.min.css')}}" rel="stylesheet">
    <link href="{{url('backend/vendors/farbtastic/farbtastic.css')}}" rel="stylesheet">
    <link href="{{url('backend/vendors/summernote/summernote.css')}}" rel="stylesheet">

    @yield('style')
            <!-- CSS -->
    <link href="{{url('backend/css/app.min.css')}}" rel="stylesheet">
    <style>
        .toggle-switch .ts-label {
            min-width: 130px;
        }
        .glyphicon.spinning {
            animation: spin 1s infinite linear;
            -webkit-animation: spin2 1s infinite linear;
        }

        @keyframes spin {
            from { transform: scale(1) rotate(0deg); }
            to { transform: scale(1) rotate(360deg); }
        }

        @-webkit-keyframes spin2 {
            from { -webkit-transform: rotate(0deg); }
            to { -webkit-transform: rotate(360deg); }
        }
        .progress{
            height: 20px;
        }
    </style>
</head>

@yield('loginContainer')

        <!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">IE SUCKS!</h1>
    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser <br/>in order to access the maximum functionality of this website. </p>
    <ul class="iew-download">
        <li>
            <a href="http://www.google.com/chrome/">
                <img src="{{url('img/browsers/chrome.png')}}" alt="">
                <div>Chrome</div>
            </a>
        </li>
        <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
                <img src="{{url('img/browsers/firefox.png')}}" alt="">
                <div>Firefox</div>
            </a>
        </li>
        <li>
            <a href="http://www.opera.com">
                <img src="{{url('img/browsers/opera.png')}}" alt="">
                <div>Opera</div>
            </a>
        </li>
        <li>
            <a href="https://www.apple.com/safari/">
                <img src="{{url('img/browsers/safari.png')}}" alt="">
                <div>Safari</div>
            </a>
        </li>
        <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                <img src="{{url('img/browsers/ie.png')}}" alt="">
                <div>IE (New)</div>
            </a>
        </li>
    </ul>
    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
</div>
<![endif]-->

<!-- Javascript Libraries -->
<script src="{{url('backend/js/jquery-2.1.1.min.js')}}"></script>
<script src="{{url('backend/js/bootstrap.min.js')}}"></script>
<script src="{{url('backend/js/functions.js')}}"></script>
<script src="{{url('backend/js/functionsha.js')}}"></script>
<script src="{{url('backend/js/member.js')}}"></script>

<script src="{{url('backend/vendors/fullcalendar/lib/moment.min.js')}}"></script>
<script src="{{url('backend/vendors/fullcalendar/fullcalendar.min.js')}}"></script>
<script src="{{url('backend/vendors/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{url('backend/vendors/easypiechart/jquery.easypiechart.min.js')}}"></script>
<script src="{{url('backend/vendors/auto-size/jquery.autosize.min.js')}}"></script>
<script src="{{url('backend/vendors/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{url('backend/vendors/mediaelement/mediaelement-and-player.min.js')}}"></script>
<script src="{{url('backend/vendors/waves/waves.min.js')}}"></script>
<script src="{{url('backend/vendors/bootstrap-growl/bootstrap-growl.min.js')}}"></script>
<script src="{{url('backend/vendors/sweet-alert/sweet-alert.min.js')}}"></script>
<script src="{{url('backend/vendors/bootstrap-select/bootstrap-select.min.js')}}"></script>

@yield('script')
</body>
</html>