<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    @yield('meta')
    <title>@yield('title')</title>

    <!-- Vendor CSS -->
    <link href="{{\App\Functions::url('backend/vendors/mediaelement/mediaelementplayer.css')}}" rel="stylesheet">
    <link href="{{\App\Functions::url('backend/vendors/fullcalendar/fullcalendar.css')}}" rel="stylesheet">
    <link href="{{\App\Functions::url('backend/vendors/animate-css/animate.min.css')}}" rel="stylesheet">
    <link href="{{\App\Functions::url('backend/vendors/sweet-alert/sweet-alert.min.css')}}" rel="stylesheet">
    @yield('style')
    <!-- CSS -->
    <link href="/backend/css/app.min.css" rel="stylesheet">
    <link href="{{\App\Functions::url('backend/css/fast-sub-styles.css')}}" rel="stylesheet">

</head>
<body>
<header id="header">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="/">{{trans('pageTranslations.title')}}</a>
        </li>

        <li class="pull-right">
            <ul class="top-menu">
                <li id="toggle-width">
                    <div class="toggle-switch">
                        <input id="tw-switch" type="checkbox" hidden="hidden">
                        <label for="tw-switch" class="ts-helper"></label>
                    </div>
                </li>
                <li id="top-search">
                    <a class="tm-search" href="#"></a>
                </li>

                <?php
                $usernameSigned=session('user');
                if($usernameSigned){
                    $userSigned = DB::table('users')->where(['username' => $usernameSigned])->first();
                    $allMessages= DB::table('messages')->where(['receiverID'=>$userSigned->userID])
                            ->join('users as u1','messages.senderID','=','u1.userID')->orderBy('messageDate','DESC')->get();
                }
                ?>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-message" href="#">@if(isset($allMessages) && count($allMessages)!=0)<i data-fastsubtitle="messageCount" class="tmn-counts">{{count($allMessages)}}</i>@endif</a></a>
                    <div class="dropdown-menu dropdown-menu-lg pull-right">
                        <div class="listview">
                            <div class="lv-header">
                                Messages
                            </div>
                            <div class="lv-body c-overflow">
                                @if(isset($allMessages))
                                    @foreach($allMessages as $per)
                                        <a class="lv-item active" href="{{\App\Functions::url('users/messages/'.Crypt::encrypt($per->username.'-'.$per->userID))}}">
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img class="lv-img-sm" src="{{\App\Functions::url("backend/img/profile-pics/".$per->profilePicture)}}" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="lv-title">{{$per->username}}</div>
                                                    <small class="lv-small">{{$per->message}}</small>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                @endif
                            </div>
                            <a class="lv-footer" href="#">View All</a>
                        </div>
                    </div>
                </li>
                <?php
                $usernameSigned=session('user');
                if($usernameSigned){
                    $userSigned = DB::table('users')->where(['username' => $usernameSigned])->first();
                    $userNotifications = DB::table('friends')->where(['friends.receiverID'=>$userSigned->userID])
                            ->join('users','friends.receiverID','=','users.userID')
                            ->join('activities','friends.friendID','=','activities.committedID')
                            ->where(['activities.activityName'=>'friend_request_has_been_send'])
                            ->where(['friends.approve'=>"0"])
                            ->get();
                }
                ?>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-notification" href="#">@if(isset($userNotifications) && count($userNotifications)!=0)<i data-fastsubtitle="notificationCount" class="tmn-counts">{{count($userNotifications)}}</i>@endif</a></a>
                    <div class="dropdown-menu dropdown-menu-lg pull-right">
                        <div class="listview" id="notifications">
                            <div class="lv-header">
                                Notification
                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="#" data-clear="notification">
                                            <i class="md md-done-all"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="lv-body c-overflow">
                                @if(isset($userNotifications))
                                    @foreach($userNotifications as $per)
                                    <a class="lv-item" href="#">
                                        <div class="media">
                                            <div class="pull-left">
                                                <img class="lv-img-sm" src="{{\App\Functions::url("backend/img/profile-pics/".$per->profilePicture)}}" alt="">
                                            </div>
                                            <div class="pull-right">
                                                    <button class="btn btn-icon bgm-lightgreen command-edit" data-fastsubtitle="userNotifications" value="{{Crypt::encrypt($per->receiverID."-".$per->senderID)}}"><span class="md md-check"></span></button>
                                            </div>

                                            <div class="media-body">
                                                <div class="lv-title">{{$per->username}}</div>
                                                <small class="lv-small">{{trans('pageTranslations.'.$per->activityName)}}</small>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                @endif
                            </div>

                            <a class="lv-footer" href="#">View Previous</a>
                        </div>

                    </div>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-settings" href="#"></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li>
                            <a href="{{\App\Functions::url('lang/tr')}}"><i class="md md-flag"></i>{{trans('pageTranslations.turkish')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('lang/en')}}"><i class="md md-flag"></i>{{trans('pageTranslations.english')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('lang/es')}}"><i class="md md-flag"></i>{{trans('pageTranslations.spanish')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('lang/de')}}"><i class="md md-flag"></i>{{trans('pageTranslations.german')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('lang/it')}}"><i class="md md-flag"></i>{{trans('pageTranslations.italian')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('lang/fr')}}"><i class="md md-flag"></i>{{trans('pageTranslations.french')}}</a>
                        </li>
                    </ul>
                </li>

                @if(Session::has('user'))
                    <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                        <a class="tm-chat" href="#"></a>
                    </li>
                @endif
            </ul>
        </li>
    </ul>

    <!-- Top Search Content -->
    <div id="top-search-wrap">
        <form action="{{\App\Functions::url('search/video')}}"><input placeholder="{{trans('pageTranslations.serie_movie_name_search')}}" name="name" type="text"></form>
        <i id="top-search-close">&times;</i>
    </div>
</header>

<section id="main">
    <aside id="sidebar">
        <div class="sidebar-inner">
            <div class="si-inner">
                <div class="profile-menu">
                    <a href="#">
                        <div style="margin-top:0;" class="profile-info">
                            @if(Session::has('user'))
                                {{trans('pageTranslations.welcome', ['username' => Session::get('user')])}}
                            @else
                                {{trans('pageTranslations.user_settings')}}
                            @endif
                            <i class="md md-settings"></i>
                        </div>
                    </a>
                    @if(Session::has('user'))
                        <ul class="main-menu" style="display: block">
                            <li>
                                <a href="{{\App\Functions::url('users/profile/')}}"><i class="md md-person"></i> {{trans('pageTranslations.view_profile')}}</a>
                            </li>
                            <li>
                                <a href="{{\App\Functions::url('user/changepass')}}"><i class="md md-settings"></i> {{trans('pageTranslations.change_password')}}</a>
                            </li>
                            <li>
                                <a href="{{\App\Functions::url('exit')}}"><i class="md md-history"></i> {{trans('pageTranslations.logout')}}</a>
                            </li>
                        </ul>
                    @else
                        <ul class="main-menu" style="display: block">
                            <li>
                                <a href="{{\App\Functions::url('user/login')}}"><i class="md md-person"></i> {{trans('pageTranslations.user_login')}}</a>
                            </li>
                            <li>
                                <a href="{{\App\Functions::url('user/createuser')}}"><i class="md md-settings-input-antenna"></i> {{trans('pageTranslations.sign_up')}}</a>
                            </li>
                            <li>
                                <a href="{{\App\Functions::url('user/forgot')}}"><i class="md md-settings"></i> {{trans('pageTranslations.forgot_password')}}</a>
                            </li>
                        </ul>
                    @endif
                </div>
                <ul class="main-menu">
                    @if(Session::has('user'))
                        <li>
                            <a href="{{\App\Functions::url('users/profile/')}}"><i class="md md-person"></i> {{trans('pageTranslations.view_profile')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('user/changepass')}}"><i class="md md-settings"></i> {{trans('pageTranslations.change_password')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('exit')}}"><i class="md md-history"></i> {{trans('pageTranslations.logout')}}</a>
                        </li>
                    @else
                        <li>
                            <a href="{{\App\Functions::url('user/login')}}"><i class="md md-person"></i> {{trans('pageTranslations.user_login')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('user/createuser')}}"><i class="md md-settings-input-antenna"></i> {{trans('pageTranslations.sign_up')}}</a>
                        </li>
                        <li>
                            <a href="{{\App\Functions::url('user/forgot')}}"><i class="md md-settings"></i> {{trans('pageTranslations.forgot_password')}}</a>
                        </li>
                    @endif
                    <li class="active"><a href="{{\App\Functions::url('')}}"><i class="md md-home"></i> {{trans('pageTranslations.home')}}</a></li>
                    <li><a href="{{\App\Functions::url('addvideo')}}"><i class="md md-add-box"></i> {{trans('pageTranslations.add_serie_movie')}}</a></li>
                    <li><a href="{{\App\Functions::url('video/last')}}"><i class="md md-video-collection"></i> {{trans('pageTranslations.last_added_series_movies')}}</a></li>
                    <li><a href="{{\App\Functions::url('people/last')}}"><i class="md md-people"></i> {{trans('pageTranslations.last_added_casts')}}</a></li>
                    <li><a href="{{\App\Functions::url('requests')}}"><i class="md md-subtitles"></i> {{trans('pageTranslations.last_added_subtitles')}}</a></li>
                    <li><a href="{{\App\Functions::url('support/subtitle')}}"><i class="md md-wrap-text"></i> {{trans('pageTranslations.support_subtitle')}}</a></li>
                    <li><a href="{{\App\Functions::url('upgradeaccount')}}"><i class="md md-trending-up"></i> {{trans('pageTranslations.upgrade_account')}}</a></li>
                    <li><a href="{{\App\Functions::url('users/showupgraderequests')}}"><i class="md md-trending-up"></i> {{trans('pageTranslations.last_upgrade_requests')}}</a></li>

                </ul>
            </div>
        </div>
    </aside>
    @if(Session::has('user'))
        <aside id="chat">
            <ul class="tab-nav tn-justified" role="tablist">
                <li role="presentation" class="active"><a href="#friends" aria-controls="friends" role="tab" data-toggle="tab">Friends</a></li>
                <li role="presentation"><a href="#online" aria-controls="online" role="tab" data-toggle="tab">Online Now</a></li>
            </ul>

            <div class="chat-search">
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Search People">
                </div>
            </div>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="friends">
                    <div class="listview">
                        <?php
                        $usernameSigned=session('user');
                            if($usernameSigned){
                                $userSigned = DB::table('users')->where(['username' => $usernameSigned])->first();
                                $query1=DB::table('friends')->select(DB::raw('senderID as userID, users.username, users.profilePicture, friends.approveDate'))->where('receiverID',$userSigned->userID)->join('users','friends.senderID','=','users.userID')->where(['friends.approve'=>"1"]);
                                $myFriends=DB::table('friends')->select(DB::raw('receiverID as userID, users.username, users.profilePicture, friends.approveDate'))->where('senderID',$userSigned->userID)->union($query1)
                                        ->orderBy('approveDate','DESC')
                                        ->where(['friends.approve'=>"1"])
                                        ->join('users','friends.receiverID','=','users.userID')->get();
                            }
                        ?>
                        @if(isset($myFriends))
                            @foreach($myFriends as $item)
                                <a class="lv-item" href="{{\App\Functions::url('users/profile/'.$item->username)}}">
                                    <div class="media">
                                        <div class="pull-left p-relative">
                                            <img class="lv-img-sm" src="{{\App\Functions::url(\App\Functions::$profileImagePath.$item->profilePicture)}}" alt="">
                                            <i class="chat-status-offline"></i>
                                        </div>
                                        <div class="media-body">
                                            <div class="lv-title">{{$item->username}}</div>
                                            <small class="lv-small">Available</small>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="online">
                    <div class="listview">

                    </div>
                </div>
            </div>
        </aside>
    @endif
    @yield('container')
</section>

<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">IE SUCKS!</h1>
    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser <br/>in order to access the maximum functionality of this website. </p>
    <ul class="iew-download">
        <li>
            <a href="http://www.google.com/chrome/">
                <img src="{{url('img/browsers/chrome.png')}}" alt="">
                <div>Chrome</div>
            </a>
        </li>
        <li>
            <a href="https://www.mozilla.org/en-US/firefox/new/">
                <img src="{{url('img/browsers/firefox.png')}}" alt="">
                <div>Firefox</div>
            </a>
        </li>
        <li>
            <a href="http://www.opera.com">
                <img src="{{url('img/browsers/opera.png')}}" alt="">
                <div>Opera</div>
            </a>
        </li>
        <li>
            <a href="https://www.apple.com/safari/">
                <img src="{{url('img/browsers/safari.png')}}" alt="">
                <div>Safari</div>
            </a>
        </li>
        <li>
            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                <img src="{{url('img/browsers/ie.png')}}" alt="">
                <div>IE (New)</div>
            </a>
        </li>
    </ul>
    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
</div>
<![endif]-->

<!-- Javascript Libraries -->
<script src="{{\App\Functions::url('backend/js/jquery-2.1.1.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/js/bootstrap.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/js/functions.js')}}"></script>
<script src="{{\App\Functions::url('backend/js/functionsha.js')}}"></script>
<script src="{{\App\Functions::url('backend/js/member.js')}}"></script>

<script src="{{\App\Functions::url('backend/vendors/fullcalendar/lib/moment.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/fullcalendar/fullcalendar.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/easypiechart/jquery.easypiechart.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/auto-size/jquery.autosize.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/mediaelement/mediaelement-and-player.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/waves/waves.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/bootstrap-growl/bootstrap-growl.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/sweet-alert/sweet-alert.min.js')}}"></script>
<script src="{{\App\Functions::url('backend/vendors/bootstrap-select/bootstrap-select.min.js')}}"></script>

@yield('script')
</body>

</html>