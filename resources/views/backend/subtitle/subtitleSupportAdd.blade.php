@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/support/subtitle"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header bgm-lime"><h2>  {{$title}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                <div class="alert alert-warning alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    {{trans('pageTranslations.please_make_sure_file_encoding')}}
                                </div>
                            </div>
                            <div class="form-group">
                                @if($errors->any())
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                            </div>
                            {!!Form::open(['route'=>['subsupport.check'],'files' => true])!!}


                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!!Form::label('fromLanguage',trans('pageTranslations.from_language'))!!}
                                    <div class="fg-line">
                                        {!!Form::select('fromLanguage', array('turkish'=>trans('pageTranslations.turkish'),'english'=>trans('pageTranslations.english'),'french'=>trans('pageTranslations.french'),'spanish'=>trans('pageTranslations.spanish'),'italian'=>trans('pageTranslations.italian'),'german'=>trans('pageTranslations.german')), 'english' , array('class' => 'selectpicker')) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file m-r-10">
                                            <span class="fileinput-new">{{trans('pageTranslations.select_file')}}</span>
                                            <span class="fileinput-exists">Change</span>
                                            {!! Form::file('fromSrt',['accept'=>'.srt','required'=>'required']) !!}
                                        </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!!Form::label('toLanguage',trans('pageTranslations.to_language'))!!}
                                    <div class="fg-line">
                                        {!!Form::select('toLanguage', array('turkish'=>trans('pageTranslations.turkish'),'english'=>trans('pageTranslations.english'),'french'=>trans('pageTranslations.french'),'spanish'=>trans('pageTranslations.spanish'),'italian'=>trans('pageTranslations.italian'),'german'=>trans('pageTranslations.german')), 'turkish' , array('class' => 'selectpicker')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file m-r-10">
                                            <span class="fileinput-new">{{trans('pageTranslations.select_file')}}</span>
                                            <span class="fileinput-exists">Change</span>
                                            {!! Form::file('toSrt',['accept'=>'.srt','required'=>'required']) !!}
                                        </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                </div>
                            </div>
                            <div class="btn-colors btn-demo">
                                {!!Form::button(trans('pageTranslations.add_support_this_files'), ['id'=>'supportSubAdd','type'=>'submit','class' => 'btn btn-lg bgm-lime pull-right waves-effect'])!!}
                            </div>
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>


@stop

@section('script')

@stop