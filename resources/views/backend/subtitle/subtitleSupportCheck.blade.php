@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/support/subtitle"/>
@stop

@section('title')
    {{trans('pageTranslations.support_subtitle_check')}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card">
                <div class="card-header">

                    <h2>{{trans('pageTranslations.support_subtitle_check')}}</h2>

                </div>
            </div>
            <div class="card col-sm-12 ">
                <div class="card-header">

                    <h2>{{$subtitleSupport['fromSrt']}} -> {{$subtitleSupport['toSrt']}}</h2>

                </div>
                <div class="row">
                    <div class="card-body card-padding col-sm-12">
                        <div class="card col-sm-6 ">
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" id="timeForFrom" placeholder="{{trans('pageTranslations.change_to_time_with_millisecond')}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('fromSrt',trans('pageTranslations.'.$subtitleSupport['fromLanguage']))!!}
                                <div class="fg-line">
                          <textarea id="fromSrt" class="form-control bgm-bluegray c-white" rows="20">
                              <?php

                              foreach(file(storage_path('backend/srtDatas/'.$subtitleSupport['fromSrt'])) as $line) {
                                  echo $line;
                              }
                              ?>
                          </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="card col-sm-6 ">
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" id="timeForTo" placeholder="{{trans('pageTranslations.change_to_time_with_millisecond')}}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('toSrt',trans('pageTranslations.'.$subtitleSupport['toLanguage']))!!}
                                <div class="fg-line">
                          <textarea id="toSrt" class="form-control bgm-bluegray c-white" rows="20">
                              <?php foreach(file(storage_path('backend/srtDatas/'.$subtitleSupport['toSrt'])) as $line) {
                                  echo $line;
                              }
                              ?>
                          </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card-padding col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="p-10" id="resultFrom"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="p-10" id="resultTo"></div>
                            </div>
                        </div>
                    </div>


                    <div class="card-body card-padding col-sm-12">
                    <div class="btn-colors btn-demo">
                        {!!Form::button(trans('pageTranslations.check').'<span></span>', ['id'=>'checkSub','type'=>'button','class' => 'btn btn-lg bgm-orange pull-left waves-effect'])!!}
                        <button id="next" type="button" class="btn btn-lg btn-success pull-right waves-effect">{{trans('pageTranslations.next')}}<span></span></button>
                    </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')
    <script>

        var pathname= location.protocol + '//' +window.location.host+'/';
        var arrayFrom   = new Array();
        var arrayTo     = new Array();
        var arrayMatch  = new Array();
        var result      = true;
        $( "#timeForFrom" ).keydown(function( event ) {
            if ( event.which == 13 ) {
                var value =parseInt($('#timeForFrom').val());
                if(value.length!=0){
                    check($('#fromSrt').val(),$('#resultFrom'),'from');
                    fixSubTime(arrayFrom,$('#fromSrt'),value);
                }else{
                    notify('','Please Enter A Value',color2,time);
                }
            }
        });

        $( "#timeForTo" ).keydown(function( event ) {
            if ( event.which == 13 ) {
                var value =parseInt($('#timeForTo').val());
                if(value.length!=0){
                    check($('#toSrt').val(),$('#resultTo'),'to');
                    fixSubTime(arrayTo,$('#toSrt'),value);
                }else{
                    notify('','Please Enter A Value',color2,time);
                }
            }
        });

        function fixSubTime(fixArr,object,value){
            $(object).val('');

            return $.map(fixArr,function(val,i){
                var temperArr = val['duration'].split('-->');
                var durationSpt = changeTime(temperArr[0],value)+ ' --> '+ changeTime(temperArr[1],value);
                $(object).val($(object).val()+(i+1)+'\n');
                $(object).val($(object).val()+durationSpt+'\n');
                $(object).val($(object).val()+val['sentence']+'\n');
                return new Array({ 'duration' : durationSpt, 'sentence' : val['sentence']});
            });
        }


        function changeTime(hmsms,millisecond) {
            var chars = hmsms.trim().split('').map(function (val, i) {
                return val == ',' ? ':' : val;
            }).join('');
            var d = new Date("1995-05-06 "+chars);
            d.setMilliseconds(d.getMilliseconds() + millisecond);
            var str = d.getHours()+ ':'+d.getMinutes()+':'+ d.getSeconds()+','+d.getMilliseconds();
            return str.split(':').map(function(val,i){
                if(i!=2){
                    return val.length==1? '0'+val+':':val+':'
                }else{
                    var spl=val.split(',');
                    var temp = spl[0].length==1? '0'+spl[0]+',':spl[0]+',';
                    temp+= spl[1].length==1? '00'+spl[1]:'';
                    temp+= spl[1].length==2? '0'+spl[1]:'';
                    temp+= spl[1].length==3? spl[1]:'';
                    return temp;
                }
            }).join('');
        }

        $('#checkSub').click(function(){
            $('#checkSub').addClass("disabled").prop("disabled", true);
            $("#checkSub span").addClass("glyphicon glyphicon-refresh spinning");
            check($('#fromSrt').val(),$('#resultFrom'),'from');
            check($('#toSrt').val(),$('#resultTo'),'to');
            arrayMatching(arrayFrom,arrayTo,$('#resultFrom'));
            fixSubTime(arrayTo,$('#toSrt'),0);
            fixSubTime(arrayFrom,$('#fromSrt'),0);
        });

        function messageBtn(val,message,bool){
            if(bool==true) return '<div class="alert alert-success alert-dismissible" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+val+ "</strong>"+ message + '<br></div>';
            else if(bool==false) return '<div class="alert alert-danger alert-dismissible" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+val+ "</strong>"+ message + '<br></div>';
            else return '<div class="alert alert-warning alert-dismissible" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+val+ "</strong>"+ message + '<br></div>';
        }


        function check(txtVal,object,where){
            result = true;
            var tempAr = new Array(),
            count = 1,
            divideSrt= txtVal.split('\n'),
            checkCount= 1,
            temp ="",
            duration ="";
            $(divideSrt).each(function(i,val){
                var insideVal=val.trim();
                if(insideVal!=''){
                    if(parseInt(insideVal)==count){
                        if(duration!='') tempAr.push({ 'duration' : duration, 'sentence' : temp.trim().toLowerCase()});
                        count++;
                        checkCount=1;
                        temp='';
                    }else{
                        if(checkCount==1){
                            duration=insideVal;
                        }else if(checkCount==2){
                            temp+=' '+insideVal;
                        }else{
                            temp+=' '+insideVal;
                        }
                        if (checkCount > 10) {
                            $(object).html('').prepend(messageBtn(count,'. Cümle Bulunamadı!',false));
                            result = false;
                            return false;
                        }
                        checkCount++;
                    }
                }
            });
            $(divideSrt).promise().done(function(){
                tempAr.push({ 'duration' : duration, 'sentence' : temp.trim().toLowerCase()});
                if(result==true) {
                    $(object).html('').prepend(messageBtn(tempAr.length,' Cümle Bulundu! Doğruluğunu Kontrol Edip Devam Ediniz.',true));
                }
                if(where=='from')
                arrayFrom=tempAr;
                else
                arrayTo=tempAr;
            });
        }

        function arrayMatching(arrayF,arrayT,object){
            var arr = jQuery.map( arrayF, function( a1 ) {
                return jQuery.map( arrayT, function( a2 ) {
                    if(a1.duration==a2.duration){
                        var a1sentence = a1.sentence;
                        var a2sentence = a2.sentence;
                        if(a1sentence.indexOf('çeviri') !=-1 || a1sentence.indexOf('çevirmen') !=-1 || a2sentence.indexOf('çeviri') !=-1 ||
                                a2sentence.indexOf('çevirmen') !=-1) return null;
                        while(a2sentence.indexOf('\\')!=-1)a2sentence=a2sentence.replace('\\','');
                        while(a2sentence.indexOf('-')!=-1)a2sentence=a2sentence.replace('-',' ');
                        while(a2sentence.indexOf("  ")!=-1)a2sentence=a2sentence.replace("  "," ");

                        while(a1sentence.indexOf('\\')!=-1)a1sentence=a1sentence.replace('\\','');
                        while(a1sentence.indexOf('-')!=-1)a1sentence=a1sentence.replace('-',' ');
                        while(a1sentence.indexOf("  ")!=-1)a1sentence=a1sentence.replace("  "," ");
                        return new Array({'sentence':a1sentence.trim(),'mean':a2sentence.trim()});
                    }
                    else{
                        return null;
                    }
                });
            });
            arrayMatch= arr;
            $('#checkSub').removeClass("disabled").prop("disabled", false);
            $("#checkSub span").removeClass("glyphicon glyphicon-refresh spinning");
            $(object).append(messageBtn(arr.length,' Adet Eşleşme Sağlandı.','notr'));
        }

        $("#next").on("click",function(){
            check($('#fromSrt').val(),$('#resultFrom'),'from');
            check($('#toSrt').val(),$('#resultTo'),'to');
            arrayMatching(arrayFrom,arrayTo,$('#resultFrom'));
            if(result==true && arrayMatch.length!=0){
                $("#next").addClass("disabled").prop("disabled", true);
                $("#next span").addClass("glyphicon glyphicon-refresh spinning");
                $.ajax({ url: pathname+"post/support/subtitle/add", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {values:arrayMatch,from:'{{$subtitleSupport['fromLanguage']}}',to:'{{$subtitleSupport['toLanguage']}}'}
                }).done(function(donenVeri){
                    $("#next span").removeClass("glyphicon glyphicon-refresh spinning").addClass("md md-check");
                    if(donenVeri=='true'){
                        swal("Yarcımcı Alt Yazı Çevirisi Başarıyla Eklendi!", "Yönlendirilmek İçin Tıklayın.", "success");
                        var url="/support/subtitle";
                        $('body').click(function(){
                            location.href=url;
                        });
                    }else if(donenVeri=='false'){
                        notify('', 'Subtitle Translating Already Started', color, time);
                    }
                }).fail(function(donenVeri){
                    $('#resultFrom').html(JSON.stringify(donenVeri,null,'<br>'));
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }); //end of ajax
            }else{
                notify('', 'Looks Like Something Wrong', color, time);
            }
        });
    </script>
@stop