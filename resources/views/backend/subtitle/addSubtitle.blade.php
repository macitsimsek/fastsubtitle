@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}} - {{trans('pageTranslations.request_subtitle')}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}} - {{trans('pageTranslations.request_subtitle')}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makeRequestSubtitleUrl($title,$videoID)}}"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.request_subtitle')}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header bgm-lime"><h2>{{$title}} - {{trans('pageTranslations.request_subtitle_page')}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                <div class="alert alert-warning alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    {{trans('pageTranslations.please_make_sure_file_encoding')}}
                                </div>
                            </div>
                            <div class="form-group">
                                @if($errors->any())
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                            </div>
                            {!!Form::open(['route'=>['request.add',$videoID],'files' => true])!!}
                            {!!Form::hidden('type',$type)!!}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!!Form::label('language',trans('pageTranslations.language'))!!}
                                    <div class="fg-line">
                                        {!!Form::select('language', array('turkish'=>trans('pageTranslations.turkish'),'english'=>trans('pageTranslations.english'),'french'=>trans('pageTranslations.french'),'spanish'=>trans('pageTranslations.spanish'),'italian'=>trans('pageTranslations.italian'),'german'=>trans('pageTranslations.german')), 'english' , array('class' => 'selectpicker')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                @if($type!='movie')
                                    <div class="form-group">
                                        {!!Form::label('season',trans('pageTranslations.season'))!!}
                                        <div class="fg-line">
                                            {!!Form::selectRange('season', 1, 20, 1, ['class' => 'selectpicker']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!!Form::label('part',trans('pageTranslations.part'))!!}
                                        <div class="fg-line">
                                            {!!Form::selectRange('part', 1, 40, 1, ['class' => 'selectpicker']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!!Form::label('partTrailer',trans('pageTranslations.part_trailer'))!!}
                                        <div class="fg-line">
                                            {!!Form::text('partTrailer', null, array('placeholder'=>trans('pageTranslations.you_can_enter_a_trailer'),'class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="col-sm-12">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file m-r-10">
                                            <span class="fileinput-new">{{trans('pageTranslations.select_file')}}</span>
                                            <span class="fileinput-exists">Change</span>
                                            {!! Form::file('srt',['accept'=>'.srt','required'=>'required']) !!}
                                        </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                </div>
                            </div>
                            <div class="btn-colors btn-demo">
                                {!!Form::button(trans('pageTranslations.send_request'), ['id'=>'requestBtn','type'=>'submit','class' => 'btn btn-lg bgm-lime pull-right waves-effect'])!!}
                            </div>
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')

@stop