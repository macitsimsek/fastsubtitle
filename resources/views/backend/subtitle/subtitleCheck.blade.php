@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makeSubtitleCheckUrl($subtitle->subtitleID)}}"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card">
                <div class="card-header">

                    <h2>{{$title}}</h2>

                </div>
            </div>
            @if($errors->any())
                <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {{$errors->first('message')}}
                </div>
            @endif
            <div class="card col-sm-12 ">
                <div class="row">
                    <div class="card-body card-padding col-sm-12">
                        <div class="form-group">
                            {!!Form::label('srtFile',trans('pageTranslations.subtitle_contents'))!!}
                            <div class="fg-line">
                          <textarea id="srtFile" class="form-control bgm-bluegray c-white" rows="20">
                              <?php foreach(file(storage_path('backend/translatingSrt/'.$subtitle->translatingSrtPath)) as $line) {
                                  echo $line;
                              }
                              ?>
                          </textarea>
                            </div>
                        </div>

                        <div class="btn-colors btn-demo">
                            {!!Form::button(trans('pageTranslations.check').' <span></span>', ['id'=>'checkSub','type'=>'button','class' => 'btn btn-lg bgm-orange pull-left waves-effect'])!!}
                            <button id="next2" type="button" class="btn btn-lg btn-success pull-right waves-effect">{{trans('pageTranslations.next')}} <span></span></button>
                        </div>


                    </div>
                    <div class="card-body card-padding col-sm-12">
                        <div class="form-group">
                            <div class="p-10" id="result"></div>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')
    <script>
        var result = true;
        var array = new Array();
        var pathname= location.protocol + '//' +window.location.host+'/';

        function check(){
            result = true;
            var insideArray = new Array();
            array = new Array();
            var count =1;
            var srt = $('#srtFile').val();
            var divideSrt= srt.split('\n');
            var checkCount=1;
            var temp ="";
            var duration ="";
            $(divideSrt).each(function(i,val){
                var insideVal=val.trim();
                if(insideVal!=''){
                    if(parseInt(insideVal)==count){
                        if(duration!=''){
                            array.push({ 'duration' : duration, 'sentence' : temp});
                        }
                        count++;
                        checkCount=1;
                        temp='';
                    }else{
                        if(checkCount==1){
                            duration=insideVal;
                        }else if(checkCount==2){
                            temp+=' '+insideVal;
                        }else{
                            temp+=' '+insideVal;
                        }
                        if (checkCount > 10) {
                            var message ='<div class="alert alert-danger alert-dismissible" role="alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+count+ "</strong>. Cümle Bulunamadı!" + '<br></div>';
                            //var correctBtn = '<button id="next" type="button" class="btn btn-lg btn-success pull-right waves-effect">Next</button>';
                            $("#result").html('').prepend(message);
                            result = false;
                            return false;
                        }
                        checkCount++;
                    }
                }
            });

            $(divideSrt).promise().done(function(){
                array.push({ 'duration' : duration, 'sentence' : temp});
                if(result==true){
                    var message ='<div class="alert alert-success alert-dismissible" role="alert">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'+array.length+ "</strong> Cümle Bulundu! Doğruluğunu Kontrol Edip Devam Ediniz." + '<br></div>';
                    //var correctBtn = '<button id="next" type="button" class="btn btn-lg btn-success pull-right waves-effect">Next</button>';
                    $("#result").html('').prepend(message);

                    $('#checkSub').removeClass("disabled").prop("disabled", false);
                    $("#checkSub span").removeClass("glyphicon glyphicon-refresh spinning");

                }
                /*
                 * $(donenVeri).each(function(i,val){
                 $('#showSrt2').append('<input class="form-control" type="text" value="'+val['duration']+'" >');
                 $('#showSrt2').append('<input class="form-control" type="text" value="'+val['sentence']+'" >');
                 });
                 * */
                /*$(array).each(function(i,val){
                 $('#showSrt2').append('<input class="form-control" type="text" value="'+val['duration']+'" >');
                 $('#showSrt2').append('<input class="form-control" type="text" value="'+val['subtitleRow']+'" >');
                 });*/
            });
        }
        $('#checkSub').click(function(){
            $('#checkSub').addClass("disabled").prop("disabled", true);
            $("#checkSub span").addClass("glyphicon glyphicon-refresh spinning");
            check();
        });
        $("#next2").on("click",function(){
            check();
            if(result==true){
                $('#next2').addClass("disabled").prop("disabled", true);
                $("#next2 span").addClass("glyphicon glyphicon-refresh spinning");
                $.ajax({ url: pathname+"subtitleadd", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {subtitleID:{{$subtitle->subtitleID}},subtitle:array}
                }).done(function(donenVeri){
                    $('#next2').removeClass("disabled").prop("disabled", false);
                    $("#next2 span").removeClass("glyphicon glyphicon-refresh spinning");
                    if(donenVeri=='true'){
                        swal("Alt Yazı Çevirme Süreci Başarıyla Başlatıldı.!", "Yönlendirilmek İçin Tıklayın.", "success");
                        var url="/subtitle/processing/{{$subtitle->subtitleID}}";
                        $('body').click(function(){
                            location.href=url;
                        });
                    }else if(donenVeri=='false'){
                        notify(title2, 'Subtitle Translating Already Started', color, time);
                    }
                }).fail(function(donenVeri){
                    $.each(donenVeri,function(i,donenveri2){
                        notify(title2, donenveri2, color, time);
                        time += 1000;
                    });
                }); //end of ajax
            }
        });

    </script>
@stop