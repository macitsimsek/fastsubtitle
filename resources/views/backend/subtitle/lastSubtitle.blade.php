@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/post/support/subtitle"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card col-sm-12">
                <div class="card-header">
                    <div class="btn-group  pull-right">
                        <div class="dropdown" data-animation="fadeInDown,fadeOutUp,600">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">
                                {{trans('pageTranslations.order_by')}}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right fadeToggle animated" role="menu">
                                <li><a href="{{url('requests/finishing')}}" >
                                        {{trans('pageTranslations.finishing_closer')}}
                                    </a></li>
                                <li><a href="{{url('requests/mostrequested')}}" >
                                        {{trans('pageTranslations.most_requested')}}
                                    </a></li>
                                <li><a href="{{url('requests')}}">
                                        {{trans('pageTranslations.last_added')}}
                                    </a></li>
                            </ul>
                        </div>
                    </div>

                    <h2>{{$title}}</h2>

                </div>
            </div>
            @if($errors->any())
                <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {!!$errors->first('message')!!}
                </div>
            @endif
            @foreach($lastSubtitles as $subtitle)
                <div class="card col-sm-12">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="col-xs-6">
                                <div class="p-15 c-white bgm-lightgreen text-center">
                                    <h4 class="c-white">{{trans('pageTranslations.'.$subtitle->type)}}</h4>
                                    {{trans('pageTranslations.sender')}}: <strong class="c-indigo"><a class="c-white" href="{{url('/users/profile/'.$subtitle->username)}}">{{$subtitle->username}}</a> </strong>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="card-body p-10 image text-center">
                                    <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($subtitle->name)).'/'.$subtitle->videoID)}}">
                                        <img src="{{url($videoImagePath.$subtitle->picture)}}" style="height:50px;width:40px;" alt="{{$subtitle->name}}" class="img-thumbnail">
                                    </a>
                                    <div><strong>{{$subtitle->name}}</strong></div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="p-10 text-center">
                                {{trans('pageTranslations.subtitle_name')}}: <strong>{{$subtitle->srtPath}}</strong>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="col-xs-6">
                                <div class="p-10 text-center">
                                    @if($subtitle->season)
                                        {{trans('pageTranslations.season')}}: <strong>{{$subtitle->season}}</strong><br>
                                        {{trans('pageTranslations.part')}}: <strong>{{$subtitle->part}}</strong>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="p-10 text-center">
                                    {{trans('pageTranslations.to')}}: <strong>{{$subtitle->language}}</strong>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="col-xs-6">
                                {{--<div class="p-10 text-center">
                                    <strong>{{$subtitle->requestCount}}</strong> {{trans('pageTranslations.users_waiting')}}
                                </div>
                                <div class="p-10 text-center">
                                    <strong class="c-green">{{$subtitle->progress}}% {{trans('pageTranslations.complate')}}</strong>
                                </div>--}}
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($subtitle->name)).'/'.$subtitle->videoID)}}" class="btn bgm-green waves-effect">
                                        {{trans('pageTranslations.go_to_serie_movie')}} <i class="md-arrow-forward"></i>
                                    </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <a href="{{url('request/'.$subtitle->subtitleReadyID)}}" class="btn bgm-lightgreen waves-effect">
                                        {{trans('pageTranslations.request_subtitle')}}<i class="md-plus-one"></i>
                                    </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <a href="{{url('/subtitle/check/'.$subtitle->subtitleReadyID)}}" class="btn bgm-lime waves-effect">
                                        {{trans('pageTranslations.subtitle_check')}}<i class="md-check"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row p-t-5">
                        <div class="progressBigBlue">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" id="progress" aria-valuemin="0" aria-valuemax="100" style="width:1%"></div>
                            {{--<div class="progress-bar progress-bar-striped active" role="progressbar" id="progress" aria-valuemin="0" aria-valuemax="100" style="width:{{$subtitle->progress}}%"></div>--}}
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="card col-sm-12">
                <div class="card-body text-center">
                    {!! $lastSubtitles->render() !!}
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')

@stop