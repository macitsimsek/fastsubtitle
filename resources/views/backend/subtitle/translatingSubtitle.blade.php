@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$subtitleAndVideo->name}}, {{trans('pageTranslations.processing_subtitles')}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$subtitleAndVideo->name}}, {{trans('pageTranslations.processing_subtitles')}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makeProcessingSubtitleUrl($subtitleAndVideo->subtitleID)}}"/>
@stop

@section('title')
    {{$subtitleAndVideo->name}} - {{trans('pageTranslations.processing_subtitles')}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    @if($errors->any())
                        <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {!!$errors->first('message')!!}
                        </div>
                    @endif
                </div>
            </div>

            <div class="card">
                <div class="card-header bgm-teal">

                    <h2>{{$subtitleAndVideo->name}} - {{trans('pageTranslations.processing_subtitles')}}</h2>

                </div>
                <div class="card-body card-padding">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-3 text-center">
                                    <div class="epc-item bgm-blue">
                                        <div class="easy-pie main-pie" data-percent="{{$subtitleAndVideo->progress}}">
                                            <div class="percent">{{$subtitleAndVideo->progress}}</div>
                                            <div class="pie-title">{{trans('pageTranslations.complate')}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 text-center">
                                    <table class="col-sm-12 col-xs-12 col-md-12 p-5 c-white bgm-blue text-center">
                                        <tr>
                                            <td class="p-5 bgm-indigo b-2">{{trans('pageTranslations.translators')}}</td>
                                            <td class="p-5 bgm-indigo b-2">{{trans('pageTranslations.rows')}}</td>
                                        </tr>
                                        @foreach($subUsers as $users)
                                            <tr>
                                                <td class="p-5 b-2"><strong><a class="c-white" href="{{url('/users/profile/'.$users->username)}}">{{$users->username}}</a></strong></td>
                                                <td class="p-5 b-2">{{$users->total}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="p-5 bgm-indigo b-2">{{trans('pageTranslations.last_translate_time')}}</td>
                                            <td class="p-5 bgm-indigo b-2">{{Date::parse($subtitleAndVideo->updated_at)->diffForHumans()}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <table class="col-sm-12 col-xs-12 col-md-12 p-5 c-white bgm-blue text-center">
                                        <tr>
                                            <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.subtitle_name')}}</td>
                                            <td class="p-5 b-2">{{$subtitleAndVideo->translatingSrtPath}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.from_language')}}</td>
                                            <td class="p-5 b-2">{{trans('pageTranslations.'.$subtitleAndVideo->fromLanguage)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.to_language')}}</td>
                                            <td class="p-5 b-2">{{trans('pageTranslations.'.$subtitleAndVideo->toLanguage)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.type')}}</td>
                                            <td class="p-5 b-2">{{trans('pageTranslations.'.$subtitleAndVideo->type)}}</td>
                                        </tr>
                                        @if($subtitleAndVideo->type!='movie')
                                            <tr>
                                                <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.season')}}</td>
                                                <td class="p-5 b-2">{{$subtitleAndVideo->season}}</td>
                                            </tr>
                                            <tr>
                                                <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.part')}}</td>
                                                <td class="p-5 b-2">{{$subtitleAndVideo->part}}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td class="bgm-indigo p-5 b-2">{{trans('pageTranslations.complate')}}</td>
                                            <td class="p-5 b-2">{{$subtitleAndVideo->progress}} %</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <div class="row progressBigBlue">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" id="progress" aria-valuemin="0" aria-valuemax="100" style="width:{{$subtitleAndVideo->progress}}%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header bgm-teal">
                    <h2>{{trans('pageTranslations.not_translated_rows')}} <small>{{trans('pageTranslations.you_can_lock_the_rows')}}</small></h2>
                </div>
                <ul class="list-group" id="list">
                    <li class="list-group-item col-xs-12" >
                        <div class="col-xs-1"><strong>{{trans('pageTranslations.rows')}}</strong></div>
                        <div class="col-xs-1"><strong>{{trans('pageTranslations.can_update')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.sentence')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.mean')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.support_mean')}}</strong></div>
                        <div class="col-xs-1"></div>
                    </li>
                    @foreach($subtitleRows as $row)
                        <li class="list-group-item col-sm-12" >
                            <div class="col-sm-2">
                                <div class="col-xs-6 p-10 c-white bgm-lightgreen text-center">{{$row->rowNumber}}</div>
                                <div class="col-xs-6 p-10 c-white bgm-green text-center">{{trans('pageTranslations.yes')}}</div>
                            </div>
                            <div class="col-sm-3">
                                <input type="hidden" id="sentences{{$row->rowNumber}}" value="{{trim(stripcslashes($row->sentence))}}">
                                <?php
                                $split=explode(' ',trim(stripcslashes($row->sentence)));
                                foreach($split as $spl){
                                    echo '<span class="word" data-toggle="popover" data-html="true" data-placement="top" data-trigger="hover" data-content="'.trans('pageTranslations.click_for_search').'" title="" data-original-title="'.$spl.'">'.$spl.'</span>';
                                }
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <input type="text" placeholder="{{trans('pageTranslations.enter_a_translate')}}" class="form-control" id="mean{{$row->rowNumber}}">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <select id="support{{$row->rowNumber}}" class="selectpicker show-tick pull-right">
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-1 text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Split button dropdowns</span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li class="text-center bgm-green"><a onclick="checkExist({{$row->rowNumber}})"><span class="md md-check"></span> {{trans('pageTranslations.confirm')}}</a></li>
                                        <li class="divider"></li>
                                        <li class="text-center bgm-lightgreen"><a onclick="finder({{$row->rowNumber}})"><span class="md md-search"></span> {{trans('pageTranslations.approximate_subtitles')}}</a></li>
                                        <li class="divider"></li>
                                        <li class="text-center bgm-lime"><a onclick="lock({{$row->rowNumber}})"><span class="md md-lock"></span> {{trans('pageTranslations.lock')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>

                <div class="card-body text-center">
                    .{!! $subtitleRows->appends(\Request::only('page_translated'))->render() !!}
                </div>
            </div>

            <div class="card">
                <div class="card-header bgm-teal">
                    <h2>{{trans('pageTranslations.translated_rows')}} <small>{{trans('pageTranslations.you_can_send_back_rows_to_translate')}}</small></h2>
                </div>

                <ul class="list-group" id="list2">
                    <li class="list-group-item col-xs-12" >
                        <div class="col-xs-1"><strong>{{trans('pageTranslations.rows')}}</strong></div>
                        <div class="col-xs-1"><strong>{{trans('pageTranslations.translate')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.sentence')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.mean')}}</strong></div>
                        <div class="col-xs-2"><strong>{{trans('pageTranslations.adder')}}</strong></div>
                        <div class="col-xs-2"><strong>{{trans('pageTranslations.commands')}}</strong></div>
                    </li>
                    @foreach($subtitleTranslates as $translate)
                        <li class="list-group-item col-sm-12">
                            <div class="col-sm-2">
                                <div class="fg-line">
                                    <div class="col-xs-6 p-10 c-white bgm-lightgreen text-center">{{$translate->rowNumber}}</div>
                                    @if($translate->bestTranslate==1)
                                        <div name="translate-{{$translate->rowNumber}}" id="row{{$translate->subtitleFixID}}" class="col-xs-6 p-10 c-white bgm-green text-center">{{trans('pageTranslations.best')}}</div>
                                    @else
                                        <div name="translate-{{$translate->rowNumber}}" id="row{{$translate->subtitleFixID}}" class="col-xs-6 p-10 c-white bgm-lime text-center">{{trans('pageTranslations.good')}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="fg-line">
                                    <div class="col-xs-6 p-10">{{stripcslashes($translate->sentence)}}</div>
                                    <div class="col-xs-6 p-10">{{$translate->newMean}}</div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <div class="col-xs-6 p-10 c-white bgm-lightgreen text-center"><strong><a class="c-white" href="{{url('/users/profile/'.$translate->username)}}">{{$translate->username}}</a></strong></div>
                                    <div class="col-xs-6 text-center">
                                        <a class="btn btn-icon command-edit" title="{{trans('pageTranslations.get_back_row')}}" onclick="getBack({{$translate->rowNumber}})"><span class="md md-arrow-back p-5"></span></a>
                                        <a class="btn btn-icon bgm-lightgreen command-edit" title="{{trans('pageTranslations.better_subtitle_row')}}" onclick="upToBest({{$translate->rowNumber.','.$translate->subtitleFixID}})"><span class="md md-check p-5"></span></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="card-body text-center">
                    .{!!  $subtitleTranslates->appends(\Request::only('page_translating'))->render() !!}
                </div>
            </div>

            <div class="card">
                <div class="card-header @if($subtitleAndVideo->process>=90) bgm-red @else bgm-green @endif">
                    <h2 class="text-right">@if($subtitleAndVideo->process>=90){{trans('pageTranslations.subtitle_translate_not_finished_yet')}}@else {{trans('pageTranslations.subtitle_translate_finished')}} @endif</h2>
                </div>
                <div class="card-body card-padding">
                    <div class="row">
                        <button onclick="location.href='{{url('subtitle/finish/'.$subtitleAndVideo->subtitleID)}}'" class="btn btn-lg pull-right bgm-green" @if($subtitleAndVideo->process>=90) disabled @endif>{{trans('pageTranslations.finish')}}</button>
                </div>
            </div>
        </div>
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')
    <script src="{{url('backend/js/fuse.min.js')}}"></script>
    <script src="{{url('backend/js/charts.js')}}"></script>
    <script type="text/javascript">

        var pathname= location.protocol + '//' +window.location.host+'/';

        $(function() {
            $('.selectpicker').on('change', function(){
                var mean = '#mean'+(this.id).replace('support','');
                var selected = $(this).find("option:selected").val();
                $(mean).val(selected);
            });
        });

        function searchOnGoogle(from,to,search){
            var bodyContent = $.ajax({
                        url:  pathname+"gtranslate",
                        global: false,
                        type: "POST",
                        beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}},
                        data: {to:to,from:from,text:search},
                        dataType: "html",
                        async:false
                }
            ).responseText;
            return bodyContent;
        }

        function searchOnYandex(from,to,search){
            var bodyContent = $.ajax({
                        url:  pathname+"ytranslate",
                        global: false,
                        type: "POST",
                        beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}},
                        data: {to:to,from:from,text:search},
                        dataType: "html",
                        async:false
                }
            ).responseText;
            return bodyContent;
        }

        $('span[class="word"]').click(function(){
            var word = $(this).text().replace('.','').replace(',','').toLowerCase().trim();
            var objWord = $(this);

            var resultGoogle = searchOnGoogle('{{$subtitleAndVideo->fromLanguage}}','{{$subtitleAndVideo->toLanguage}}',word);
            var resultYandex = searchOnYandex('{{$subtitleAndVideo->fromLanguage}}','{{$subtitleAndVideo->toLanguage}}',word);
            resultGoogle = '<div><span class="badge bgm-indigo">GT</span><span class="badge">'+resultGoogle+'</span></div>';
            resultYandex = '<div><span class="badge bgm-indigo">YT</span><span class="badge"">'+resultYandex+'</div></div>';
            $('#'+$(objWord).attr('aria-describedby')).find('h3+div').text(resultGoogle+resultYandex);
            $(objWord).attr('data-content',resultGoogle+resultYandex);
        });

        function lock(rowID){
            $.ajax({
                url: pathname+"lockrow", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {row:rowID,subtitleID:{{$subtitleID}}}
            }).done(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    if(donenVeri=='true'){
                        $('#mean'+rowID).parent().parent().parent().addClass('bgm-lime').slideToggle(800,function(){
                            this.remove();
                        });
                        notify('', 'Kilitlendi.', color2, time)
                    }else{
                        notify('', "asdasd", color, time);
                    }
                }
            }).fail(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    notify('', donenVeri, color, time);
                }
            }); //end of ajax
        }
        function finder(id) {
            if(searchArray.length === 0){
                notify('','Çeviri veri tabanı yükleniyor. Lütfen Bekleyin.',color,time);
            }else{
            //$('#list li:eq('+id+') div div:first-of-type:not(:only-of-type)').each(function(i,val){
            var sentencesID = '#sentences'+id;
            var supportID = '#support'+id;
            $(supportID)
                    .find('option')
                    .remove();
            var searchVal =$(sentencesID).val().toLowerCase();

            var searchWord = $(sentencesID).val().toLowerCase().replace('\'','').replace('\"','').replace('-','').trim();
            var result = fuse.search(searchWord);
            if(result.length>0){
                var yandex = (searchOnYandex('{{$subtitleAndVideo->fromLanguage}}','{{$subtitleAndVideo->toLanguage}}',searchVal)).toLowerCase();
                $(supportID).append('<option value="'+yandex+'">'+searchVal+' ('+yandex+') (Yandex Translate API)</option>');

                var google = (searchOnGoogle('{{$subtitleAndVideo->fromLanguage}}','{{$subtitleAndVideo->toLanguage}}',searchVal)).toLowerCase();
                $(supportID).append('<option value="'+google+'">'+searchVal+' ('+google+') (Google Translate API)</option>');

                result=result.slice(0,15);
                $(result).each(function(i,val){
                    $(supportID).append('<option value="'+val['mean']+'">'+val['sentence']+' ('+val['mean']+')</option>');
                });
                notify('','Bulundu',color2,time);
            }
            $(supportID).selectpicker('refresh');
            //$('textarea').each(function(k,data){});

            }
        }

        function meanList(val,array,id){
            $('.combobox-container').remove();
            $('#meanBox').parent().remove();

            var meanBox = '<div class="list-group-item col-sm-12" ><select class="combobox input-large form-control" id="meanBox"></select></div>';
            $('#'+id).parent().parent().parent().after(meanBox);
            var google = (searchOnGoogle('{{$subtitleAndVideo->fromLanguage}}','{{$subtitleAndVideo->toLanguage}}',val)).toLowerCase();
            $('#meanBox').append('<option value="'+google+'">'+val+' ('+google+') (Google Translate API)</option>');

            var yandex = (searchOnYandex('{{$subtitleAndVideo->fromLanguage}}','{{$subtitleAndVideo->toLanguage}}',val)).toLowerCase();
            $('#meanBox').append('<option value="'+yandex+'">'+val+' ('+yandex+') (Yandex Translate API)</option>');

            $(array).each(function(i,val){
                $('#meanBox').append('<option value="'+val['mean']+'">'+val['sentence']+'<br>'+val['mean']+' - Fast Subtitle</option>');
            });
            $('.combobox').combobox();
        }

        var options = {
            caseSensitive: false,
            includeScore: false,
            shouldSort: true,
            threshold: 0.5,
            location: 0,
            distance: 20000,
            maxPatternLength: 100,
            keys: ["sentence"] //nerelerde aranacağı
        };
        var fuse;
        var searchArray=new Array();
        $( document ).ready(function() {
            //var time1 = new Date();
            //var time1ms= time1.getTime(time1);
            $.ajax({ url: pathname+"getmean", type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {from:"{{$subtitleAndVideo->fromLanguage}}",to:"{{$subtitleAndVideo->toLanguage}}"}
            }).done(function(donenVeri){
                searchArray= jQuery.map( donenVeri, function( data, i ) {
                    while(data['sentence'].indexOf('\\')!=-1 || data['sentence'].indexOf('/')!=-1 )data['sentence']=data['sentence'].replace('\\','').replace('/','');
                    while(data['mean'].indexOf('\\')!=-1 || data['mean'].indexOf('/')!=-1 )data['mean']=data['mean'].replace('\\','').replace('/','');
                    return new Array({sentence:data['sentence'],
                        mean:data['mean']});
                });
                //var time2 = new Date();
                //var time2ms= time2.getTime(time2);
                //alert((time2ms-time1ms));
                options['distance']=searchArray.length;
                fuse = new Fuse(searchArray, options); // "list" is the item array
            }).fail(function(donenVeri){
                $( "#resultTextArea").html(JSON.stringify(donenVeri,null,'<br>'));
            }); //end of ajax
        });

        $( "div div input:text" ).keydown(function( event ) {
            if ( event.which == 13 ) {
                //alert($('#'+this.id).val());
                var val =$('#'+this.id).val();
                var result = fuse.search(val);
                meanList(val,result,this.id);

                //alert(JSON.stringify(meanObj.parent().css({background:'red'}),null,'<br>'))
            }
        });

        function getBack(rowID){
            $.ajax({
                url: pathname+"getbackrow", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {row:rowID,subtitleID:{{$subtitleID}}}
            }).done(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    if(donenVeri=='true'){
                        var row = '<li class="list-group-item col-sm-12">'+
                                '<div class="col-sm-2">'+
                                '<div class="col-xs-6 p-10 c-white bgm-lightgreen text-center">73</div>'+
                                '<div class="col-xs-6 p-10 c-white bgm-green text-center">Evet</div>'+
                                '</div>'+
                                '<div class="col-sm-3">'+
                                '<input type="hidden" id="sentences" value="Come live with us.">'+
                                '<span class="word" data-toggle="popover" data-html="true" data-placement="top" data-trigger="hover" data-content="Aramak İçin Tıklayın" title="" data-original-title="Come">Come</span><span class="word" data-toggle="popover" data-html="true" data-placement="top" data-trigger="hover" data-content="Aramak İçin Tıklayın" title="" data-original-title="live">live</span><span class="word" data-toggle="popover" data-html="true" data-placement="top" data-trigger="hover" data-content="Aramak İçin Tıklayın" title="" data-original-title="with">with</span><span class="word" data-toggle="popover" data-html="true" data-placement="top" data-trigger="hover" data-content="Aramak İçin Tıklayın" title="" data-original-title="us.">us.</span>                            </div>'+
                                '<div class="col-sm-3">'+
                                '<div class="fg-line">'+
                                '<input type="text" placeholder="Bir Çeviri Giriniz" class="form-control" id="mean73" tabindex="28" style="overflow: hidden; outline: none;">'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-sm-3">'+
                                '<div class="fg-line">'+
                                '<select id="support73" class="selectpicker show-tick pull-right" style="display: none;">'+
                                '</select><div class="btn-group bootstrap-select show-tick pull-right"><button type="button" class="btn dropdown-toggle selectpicker btn-default" data-toggle="dropdown" data-id="support73" title="Nothing selected"><span class="filter-option pull-left">Nothing selected</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner selectpicker" role="menu"></ul></div></div>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-sm-1 text-right">'+
                                '<div class="btn-group">'+
                                '<button type="button" class="btn btn-info dropdown-toggle waves-effect waves-button" data-toggle="dropdown" aria-expanded="false">'+
                                '<span class="caret"></span>'+
                                '<span class="sr-only">Split button dropdowns</span>'+
                                '</button>'+
                                '<ul class="dropdown-menu pull-right" role="menu">'+
                                '<li class="text-center bgm-green"><a onclick="checkExist(73)"><span class="md md-check"></span> Onayla</a></li>'+
                                '<li class="divider"></li>'+
                                '<li class="text-center bgm-lightgreen"><a onclick="finder(73)"><span class="md md-search"></span> Yakın Alt Yazılar</a></li>'+
                                '<li class="divider"></li>'+
                                '<li class="text-center bgm-lime"><a onclick="lock(73)"><span class="md md-lock"></span> Kilitle</a></li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</li>';
                    }
                    donenVeri=='true'?notify('', 'Geri Alındı.', color2, time):notify(title2, donenVeri, color, time);
                }
            }).fail(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    notify('', donenVeri, color, time);
                }
            }); //end of ajax
        }



        function upToBest(rowNo,subtitleFixID){
            var textGood ="";
            var textBest ="";
            if(!$('#row'+subtitleFixID).hasClass('bgm-green')){
                $.ajax({
                    url: pathname+"uptobest", type:"POST", beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {FixID:subtitleFixID}
                }).done(function(donenVeri){
                    if($.isArray(donenVeri)){
                        $.each(donenVeri,function(i,donenveri2){
                            notify('', donenveri2, color, time);
                            time += 1000;
                        });
                    }else{
                        if(donenVeri=='true'){
                            $('div[name="translate-'+rowNo+'"]').each(function(){
                                if($(this).hasClass("bgm-green")) textBest=$(this).text();
                                else textGood=$(this).text();
                            });
                            $('div[name="translate-'+rowNo+'"]').each(function(){
                                if($(this).attr('id')=='row'+subtitleFixID){
                                    $(this).fadeToggle(400,function(){
                                        $(this).fadeToggle(50).removeClass("bgm-green").removeClass("bgm-lime").addClass("bgm-green").text(textBest);
                                    })
                                }
                                else {
                                    $(this).fadeToggle(400,function(){
                                        $(this).fadeToggle(50).removeClass("bgm-green").removeClass("bgm-lime").addClass("bgm-lime").text(textGood);
                                    });
                                }
                            });
                        }else{
                            notify('', donenVeri, color, time);
                        }
                    }
                }).fail(function(donenVeri){
                    if($.isArray(donenVeri)){
                        $.each(donenVeri,function(i,donenveri2){
                            notify('', donenveri2, color, time);
                            time += 1000;
                        });
                    }else{
                        notify('', donenVeri, color, time);
                    }
                }); //end of ajax
            }
        }

        function addMean(rowID){
            $('.combobox-container').remove();
            $('#meanBox').parent().remove();
            var meanRow =$('#mean'+rowID).val();
            if(meanRow!=""){
                $('#mean'+rowID).parent().parent().parent().addClass('bgm-lightgreen').slideToggle(800,function(){
                    this.remove();
                });
                $.ajax({
                    url: pathname+"addsubrow", type:"POST", beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {mean:meanRow,row:rowID,subtitleID:{{$subtitleID}}}
                }).done(function(donenVeri){
                    if($.isArray(donenVeri)){
                        $.each(donenVeri,function(i,donenveri2){
                            notify('', donenveri2, color, time);
                            time += 1000;
                        });
                    }else{
                        donenVeri=='true'?notify('', 'Eklendi.', color2, time):notify(title2, donenVeri, color, time);
                    }
                }).fail(function(donenVeri){
                    if($.isArray(donenVeri)){
                        $.each(donenVeri,function(i,donenveri2){
                            notify('', donenveri2, color, time);
                            time += 1000;
                        });
                    }else{
                        notify('', donenVeri, color, time);
                    }
                }); //end of ajax
            }else{
                notify('', 'Value Didn\'t Fill', color, time);
                $('#mean'+rowID).focus();
            }
        }
        function checkExist(rowID)
        {
            $.ajax({
                url: pathname+"checkexistsrow", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {row:rowID,subtitleID:{{$subtitleID}}}
            }).done(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    if(donenVeri=='true'){
                        makeSure(rowID);
                    }else{
                        addMean(rowID);
                    }
                }
            }).fail(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    notify('', donenVeri, color, time);
                }
            }); //end of ajax
        }
        //Parameter
        function makeSure(rowID){
            swal({
                title: "Görünüşe göre başkası anlam eklemiş.",
                text: "Yinede eklemek istiyormusunuz?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Evet Ekle!",
                cancelButtonText: "Hayır, Kontrol Edeceğim",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    addMean(rowID);
                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                }else{
                    $('.combobox-container').remove();
                    $('#meanBox').parent().remove();
                    $('#mean'+rowID).parent().parent().parent().addClass('bgm-amber').slideToggle(800,function(){
                        this.remove();
                    });
                }
            });
        };


    </script>
@stop