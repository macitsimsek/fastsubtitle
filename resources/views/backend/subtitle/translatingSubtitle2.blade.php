@extends('backend.master')

@section('title')
    {{$title}} - {{trans('pageTranslations.processing_subtitles')}} - {{trans('pageTranslations.domain')}}
@stop

@section('style')
<link href="{{url('backend/vendors/bootstrap-combobox-master/css/bootstrap-combobox.css')}}" rel="stylesheet">
    <style>
        .word{
            display:inline-block;
            padding:0px 3px;
            margin-bottom:0;
            font-size:12px;
            font-weight:400;
            line-height:1.42857143;
            text-align:center;
            white-space:nowrap;
            vertical-align:middle;
            -ms-touch-action:manipulation;
            touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;
            -ms-user-select:none;user-select:none;background-image:none;
            border:1px solid transparent;
            border-radius:4px}
        .word:hover{
            font-weight:600;
            color: #0d8aee;
            box-shadow: 0 2px 5px 0 rgba(12, 84, 255, 0.16),0 2px 10px 0 rgba(60, 103, 255, 0.59);
        }
    </style>
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card">
                <div class="card-header">

                    <h2>{{$title}} - {{trans('pageTranslations.processing_subtitles')}}</h2>

                </div>
            </div>

<div id="deneme"></div>
            <div class="card">
                <div class="card-header">
                    <h2>{{trans('pageTranslations.not_translated_rows')}} <small>{{trans('pageTranslations.you_can_lock_the_rows')}}</small></h2>
                </div>
                <ul class="list-group" id="list">
                    <li class="list-group-item col-xs-12" >
                        <div class="col-xs-1"><strong>{{trans('pageTranslations.rows')}}</strong></div>
                        <div class="col-xs-1"><strong>{{trans('pageTranslations.can_update')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.sentence')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.mean')}}</strong></div>
                        <div class="col-xs-3"><strong>{{trans('pageTranslations.support_mean')}}</strong></div>
                        <div class="col-xs-1"></div>
                    </li>
                    @foreach($subtitleRows as $row)
                        <li class="list-group-item col-sm-12" >


                            <div class="col-sm-2">
                                <div class="col-xs-6">{{$row->rowNumber}}</div>
                                <div class="col-xs-6">{{trans('pageTranslations.yes')}}</div>
                            </div>
                            <div class="col-sm-3">
                                <input type="hidden" id="sentences{{$row->rowNumber}}" value="{{trim(stripcslashes($row->sentence))}}">
                                <?php
                                    $split=explode(' ',trim(stripcslashes($row->sentence)));
                                    foreach($split as $spl){
                                        echo '<span class="word" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="'.trans('pageTranslations.click_for_search').'" title="" data-original-title="'.$spl.'">'.$spl.'</span>';
                                    }
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <input type="text" placeholder="{{trans('pageTranslations.enter_a_translate')}}" class="form-control" id="mean{{$row->rowNumber}}">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <select id="support{{$row->rowNumber}}" class="selectpicker show-tick pull-right">
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-1 text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Split button dropdowns</span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li class="text-center bgm-green"><a onclick="checkExist({{$row->rowNumber}})"><span class="md md-check"></span> {{trans('pageTranslations.confirm')}}</a></li>
                                        <li class="divider"></li>
                                        <li class="text-center bgm-lightgreen"><a onclick="finder({{$row->rowNumber}})"><span class="md md-search"></span> {{trans('pageTranslations.approximate_subtitles')}}</a></li>
                                        <li class="divider"></li>
                                        <li class="text-center bgm-lime"><a onclick="lock({{$row->rowNumber}})"><span class="md md-lock"></span> {{trans('pageTranslations.lock')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>

                <div class="card-body text-center">
                    {!! $subtitleRows->render() !!}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h2>{{trans('pageTranslations.translated_rows')}} <small>{{trans('pageTranslations.you_can_send_back_rows_to_translate')}}</small></h2>
                </div>

                <div class="table-responsive">
                    <table id="data-table-command" class="table table-striped">
                        <thead>
                        <tr>
                            <th data-column-id="fixid" data-visible="false"></th>
                            <th data-column-id="id" data-type="numeric" data-order="asc">{{trans('pageTranslations.rows')}}</th>
                            <th data-column-id="translate" data-order="asc">{{trans('pageTranslations.translate')}}</th>
                            <th data-column-id="sentence" data-sortable="false">{{trans('pageTranslations.sentence')}}</th>
                            <th data-column-id="text" data-sortable="false">{{trans('pageTranslations.mean')}}</th>
                            <th data-column-id="username" data-sortable="false">{{trans('pageTranslations.adder')}}</th>
                            <th data-column-id="commands" data-formatter="commands" data-sortable="false">{{trans('pageTranslations.commands')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subtitleTranslates as $translate)
                            <tr>
                                <td>{{$translate->subtitleFixID}}</td>
                                <td>{{$translate->rowNumber}}</td>
                                <td>@if($translate->bestTranslate==1)
                                        {{trans('pageTranslations.best')}}
                                    @else
                                        {{trans('pageTranslations.good')}}
                                    @endif
                                </td>
                                <td>{{stripcslashes($translate->sentence)}}</td>
                                <td>{{$translate->newMean}}</td>
                                <td>{{$translate->username}}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section>
@stop



@section('script')
<script src="{{url('backend/vendors/bootgrid/jquery.bootgrid.min.js')}}"></script>
<script src="{{url('backend/js/fuse.min.js')}}"></script>
<script src="{{url('backend/vendors/bootstrap-combobox-master/js/bootstrap-combobox.js')}}"></script>

    <!-- Data Table -->

<script type="text/javascript">

    var pathname= location.protocol + '//' +window.location.host+'/';
    
    $(function() {
        $('.selectpicker').on('change', function(){
            var mean = '#mean'+(this.id).replace('support','');
            var selected = $(this).find("option:selected").text();
            $(mean).val(selected);
        });
    });

    $('span[class="word"]').click(function(){
        var word = $(this).text().replace('.','').replace(',','').toLowerCase().trim();
        var token = $('meta[name="csrf_token"]').attr('content');
        var deneme = $(this);
        $.ajax({
            url: pathname+"translate", type:"POST", beforeSend: function (xhr) {
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {to:'turkish',from:'english',text:word}
        }).done(function(donenVeri){
            $('#'+$(deneme).attr('aria-describedby')).find('h3+div').text(donenVeri);
            $(deneme).attr('data-content',donenVeri);
        });
    });

        function lock(rowID){
            $.ajax({
                url: pathname+"lockrow", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {row:rowID,subtitleID:{{$subtitleID}}}
            }).done(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    if(donenVeri=='true'){
                        $('#mean'+rowID).parent().parent().parent().addClass('bgm-lime').slideToggle(800,function(){
                            this.remove();
                        });
                        notify('', 'Kilitlendi.', color2, time)
                    }else{
                        notify('', donenVeri, color, time);
                    }
                }
            }).fail(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    notify('', donenVeri, color, time);
                }
            }); //end of ajax
        }
        function finder(id) {
            //$('#list li:eq('+id+') div div:first-of-type:not(:only-of-type)').each(function(i,val){
                var sentencesID = '#sentences'+id;
                var supportID = '#support'+id;
                $(supportID)
                    .find('option')
                    .remove();
                var searchWord = $(sentencesID).val().toLowerCase().replace('\'','').replace('\"','').replace('-','').trim();
                var result = fuse.search(searchWord);
                if(result.length>0){
                    result=result.slice(0,15);
                    $(result).each(function(i,val){
                        $(supportID).append('<option value="'+i+'">'+val['sentence']+' = '+val['mean']+'</option>');
                    });
                    notify('','Bulundu',color2,time);
                }
                $(supportID).selectpicker('refresh');
            //$('textarea').each(function(k,data){});
        }

        function meanList(array,id){
            $('.combobox-container').remove();
            $('#meanBox').parent().remove();
            var meanBox = '<div class="list-group-item col-sm-12" ><select class="combobox input-large form-control" id="meanBox"></select></div>';
            $('#'+id).parent().parent().parent().after(meanBox);
            $(array).each(function(i,val){
                $('#meanBox').append('<option value="'+i+'">'+val['mean']+' = '+val['sentence']+'</option>');
            });
            $('.combobox').combobox();
        }

        var options = {
            caseSensitive: false,
            includeScore: false,
            shouldSort: true,
            threshold: 0.5,
            location: 0,
            distance: 18267,
            maxPatternLength: 100,
            keys: ["sentence"] //nerelerde aranacağı
        };
        var fuse;
        var searchArray=new Array();
        $( document ).ready(function() {
            //var time1 = new Date();
            //var time1ms= time1.getTime(time1);
            $.ajax({ url: pathname+"getmean", type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}
            }).done(function(donenVeri){
                searchArray= donenVeri;
                //var time2 = new Date();
                //var time2ms= time2.getTime(time2);
                //alert((time2ms-time1ms));
                fuse = new Fuse(searchArray, options); // "list" is the item array
            }).fail(function(donenVeri){

                $( "#resultTextArea").html(JSON.stringify(donenVeri,null,'<br>'));
            }); //end of ajax
        });
        $( "#searchq" ).keydown(function( event ) {
            if ( event.which == 13 ) {
                var result = fuse.search($('#searchq').val());
                meanList(result);
            }
        });
        $( "div div input:text" ).keydown(function( event ) {
            if ( event.which == 13 ) {
                //alert($('#'+this.id).val());
                var result = fuse.search($('#'+this.id).val());
                meanList(result,this.id);

                //alert(JSON.stringify(meanObj.parent().css({background:'red'}),null,'<br>'))
            }
        });



        function getBack(rowID){
            $.ajax({
                url: pathname+"getbackrow", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {row:rowID,subtitleID:{{$subtitleID}}}
            }).done(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    donenVeri=='true'?notify('', 'Geri Alındı.', color2, time):notify(title2, donenVeri, color, time);
                }
            }).fail(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    notify('', donenVeri, color, time);
                }
            }); //end of ajax
        }



        function upToBest(rowNo,subtitleFixID){
            alert(rowNo+''+subtitleFixID+''+$('tr td:eq(1)').text());
            $('tr td:eq(0)').each(function(){
                alert($(this).text());
            })

            /*
            $.ajax({
                url: pathname+"uptobest", type:"POST", beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {FixID:subtitleFixID}
            }).done(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    if(donenVeri=='true'){
                        notify('', 'Yükseltildi.', color2, time);
                    }else{
                        notify('', donenVeri, color, time);
                    }
                }
            }).fail(function(donenVeri){
                if($.isArray(donenVeri)){
                    $.each(donenVeri,function(i,donenveri2){
                        notify('', donenveri2, color, time);
                        time += 1000;
                    });
                }else{
                    notify('', donenVeri, color, time);
                }
            }); //end of ajax*/
        }

        function addMean(rowID){
            var meanRow =$('#mean'+rowID).val();
            if(meanRow!=""){
                $('#mean'+rowID).parent().parent().parent().addClass('bgm-lightgreen').slideToggle(800,function(){
                    this.remove();
                });
                $.ajax({
                    url: pathname+"addsubrow", type:"POST", beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {mean:meanRow,row:rowID,subtitleID:{{$subtitleID}}}
                }).done(function(donenVeri){
                    if($.isArray(donenVeri)){
                        $.each(donenVeri,function(i,donenveri2){
                            notify('', donenveri2, color, time);
                            time += 1000;
                        });
                    }else{
                        donenVeri=='true'?notify('', 'Eklendi.', color2, time):notify(title2, donenVeri, color, time);
                    }
                }).fail(function(donenVeri){
                    if($.isArray(donenVeri)){
                        $.each(donenVeri,function(i,donenveri2){
                            notify('', donenveri2, color, time);
                            time += 1000;
                        });
                    }else{
                        notify('', donenVeri, color, time);
                    }
                }); //end of ajax
            }else{
                notify('', 'Value Didn\'t Fill', color, time);
                $('#mean'+rowID).focus();
            }
        }

        $(document).ready(function(){
            //Command Buttons
            $("#data-table-command").bootgrid({
                css: {
                    icon: 'md icon',
                    iconColumns: 'md-view-module',
                    iconDown: 'md-expand-more',
                    iconRefresh: 'md-refresh',
                    iconUp: 'md-expand-less'
                },
                selection: true,
                rowSelect: true,
                formatters: {
                    "text": function(column, row) {
                        return "<div class=\"fg-line\"><input type=\"text\" class=\"form-control\" placeholder=\"" + row.id + ". Row Mean\" id=\"mean" +row.id  + "\"></div>";
                    },
                    "commands": function(column, row) {
                        return "<a class=\"btn btn-icon command-edit\" onclick=\"getBack(" + row.id + ")\"><span class=\"md md-arrow-back p-5\"></span></a>" +
                                "<a class=\"btn btn-icon bgm-lightgreen command-edit\" onclick=\"upToBest(" +row.id+','+ row.fixid + ")\"><span class=\"md md-check p-5\"></span></a>";
                    }
                }
            });
        });
    function checkExist(rowID)
    {
        $.ajax({
            url: pathname+"checkexistsrow", type:"POST", beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
                if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}, data: {row:rowID,subtitleID:{{$subtitleID}}}
        }).done(function(donenVeri){
            if($.isArray(donenVeri)){
                $.each(donenVeri,function(i,donenveri2){
                    notify('', donenveri2, color, time);
                    time += 1000;
                });
            }else{
                if(donenVeri=='true'){
                    makeSure(rowID);
                }else{
                    addMean(rowID);
                }
            }
        }).fail(function(donenVeri){
            if($.isArray(donenVeri)){
                $.each(donenVeri,function(i,donenveri2){
                    notify('', donenveri2, color, time);
                    time += 1000;
                });
            }else{
                notify('', donenVeri, color, time);
            }
        }); //end of ajax
    }
    //Parameter
    function makeSure(rowID){
        swal({
            title: "Görünüşe göre başkası başkası anlam eklemiş.",
            text: "Yinede eklemek istiyormusunuz?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Evet Ekle!",
            cancelButtonText: "Hayır, Kontrol Edeceğim",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                addMean(rowID);
                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            }else{

            }
        });
    };

    </script>
@stop