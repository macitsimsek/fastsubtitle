@extends('backend.master')

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <button onclick="deneme('asd','asdasd')">deneme</button>
            @if(!empty($users))
                @foreach($users as $user)
                    {{$user->username}}<br>
                    {{$user->memberType}}<br>
                    {{$user->profilePicture}}<br>
                    {{$user->position}}<br>
                    {{$user->translateCount}}<br><br>
                @endforeach
            @endif
        </div>
        <div id="deneme"></div>
    </section>
@stop

@section('script')

<script>
    var searchArray=new Array();
    $( document ).ready(function() {
        $.ajax({ url: pathname+"getmean", type:"POST", beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');
            if (token) {return xhr.setRequestHeader('X-CSRF-TOKEN', token);}}
        }).done(function(donenVeri){
            searchArray= jQuery.map( donenVeri, function( data, i ) {
                while(data['sentence'].indexOf('\\')!=-1 || data['sentence'].indexOf('/')!=-1 )data['sentence']=data['sentence'].replace('\\','').replace('/','');
                while(data['mean'].indexOf('\\')!=-1 || data['mean'].indexOf('/')!=-1 )data['mean']=data['mean'].replace('\\','').replace('/','');
                return new Array({sentence:data['sentence'],
                    mean:data['mean']});
            });
            var time1 = new Date();
            var time1ms= time1.getTime(time1);
            searchArray= jQuery.map( donenVeri, function( data, i ) {
                var string ="four months after the events of";
                var dene =deneme(string, data['mean']);
                if(dene<string.length*(1/2))
                return new Array(data['sentence'], dene);
                else
                        return null;
            });

            searchArray.sort(
                    function(a,b) {
                        return b[1]-a[1] || a[0].localeCompare(b[0]);
                    }
            );

            var time2 = new Date();
            var time2ms= time2.getTime(time2);

            $( "#deneme").html(JSON.stringify(searchArray,null,'<br>'));


        }).fail(function(donenVeri){
            $( "#deneme").html(JSON.stringify(donenVeri,null,'<br>'));
        }); //end of ajax
    });


    function deneme(a, b){
        var start = +new Date();
        if(a.length == 0) return b.length;
        if(b.length == 0) return a.length;

        var matrix = [];

        // increment along the first column of each row
        var i;
        for(i = 0; i <= b.length; i++){
            matrix[i] = [i];
        }

        // increment each column in the first row
        var j;
        for(j = 0; j <= a.length; j++){
            matrix[0][j] = j;
        }


        // Fill in the rest of the matrix
        for(i = 1; i <= b.length; i++){
            for(j = 1; j <= a.length; j++){
                if(b.charAt(i-1) == a.charAt(j-1)){
                    matrix[i][j] = matrix[i-1][j-1];
                } else {
                    matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                            Math.min(matrix[i][j-1] + 1, // insertion
                                    matrix[i-1][j] + 1)); // deletion
                }
            }
        }

        return matrix[b.length][a.length]
    };
</script>
@stop