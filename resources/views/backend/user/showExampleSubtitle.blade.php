@extends('backend.master')

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card col-sm-12">
                <div class="card-header bgm-orange"><h2>{{$title}}</h2></div>
                <div class="card-body card-padding">
                    <div class="row">
                        <div class="col-sm-12">
                                <textarea id="fromSrt" class="form-control bgm-bluegray c-white" rows="20">
                                    <?php
                                    foreach(file(storage_path('backend/upgradeSrt/'.$upgrade->subtitleName)) as $line) {
                                        echo $line;
                                    }
                                    ?>
                          </textarea>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop
