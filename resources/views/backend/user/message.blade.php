@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/users/messages/" />
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.messages')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card col-sm-12">
                <div class="card-header"><h4>{{trans('pageTranslations.messages')}}</h4></div>
            </div>
            @if($errors->any())
                <div class="col-sm-12">
                <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {!!$errors->first('message')!!}
                </div>
                </div>
            @endif
            <div class="card col-sm-12" id="messages-main" >
                <div class="row">
                    <div class="ms-menu">
                        <div class="listview lv-user m-t-20">
                            @foreach($friends as $per)
                                <a class="lv-item" href="{{url('users/messages/'.Crypt::encrypt($per->username.'-'.$per->userID))}}">
                                    <div class="lv-item media">
                                        <div class="lv-avatar pull-left">
                                            <img src="{{url(\App\Functions::$profileImagePath.$per->profilePicture)}}" title="{{$per->username}}" alt="{{$per->username}}">
                                        </div>
                                        <div class="media-body">
                                            <div class="lv-title">{{$per->username}}</div>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>

                    <div class="ms-body" style="min-height: 250px;">
                        <div class="listview lv-message">
                            <div class="lv-header-alt bgm-white">
                                <div id="ms-menu-trigger">
                                    <div class="line-wrap">
                                        <div class="line top"></div>
                                        <div class="line center"></div>
                                        <div class="line bottom"></div>
                                    </div>
                                </div>
                                @if(isset($messageReceiver))
                                    <div class="lvh-label hidden-xs">
                                        <div class="lv-avatar pull-left">
                                            <img src="{{url(\App\Functions::$profileImagePath.$messageReceiver->profilePicture)}}" alt="">
                                        </div>
                                        <span class="c-black">{{$messageReceiver->username}}</span>
                                    </div>
                                @endif
                            </div>

                            <div class="lv-body">
                                @if(isset($allMessages))
                                    @foreach($allMessages as $item)
                                        <div class="lv-item media @if(session('user') ==$item->username) right @endif p-20">
                                            <div class="lv-avatar pull-@if(session('user') ==$item->username){{'right'}} @else{{'left'}} @endif">
                                                <img src="{{url(\App\Functions::$profileImagePath.$item->profilePicture)}}" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="ms-item">
                                                    {{$item->message}}
                                                </div>
                                                <small class="ms-date"><i class="md md-access-time"></i> {{Date::parse($item->messageDate)->diffForHumans()}} @if($item->seen=="1") - {{trans('pageTranslations.seen')}}@endif</small>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <div class="lv-footer ms-reply">
                                @if(isset($token))
                                    {{Form::open(array('route' => array('send.message', $token)))}}
                                    {{Form::textarea('message', null, ['class' => 'field','placeholder'=>trans('pageTranslations.whats_on_your_mind')]) }}
                                    {{Form::button('<i class="md md-send"></i>', ['name'=>'sentMessage','type'=>'submit'])}}
                                    {{Form::close()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop