@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/upgradeaccount" />
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card col-sm-8">
                    <div class="card-header bgm-lime"><h2>{{$title}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="form-group">
                                @if($errors->any() && !$errors->first('result'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                                    @if($errors->any() && $errors->first('result'))
                                        <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            {!!$errors->first('message')!!}
                                        </div>
                                    @endif
                            </div>
                                <div class="form-group">
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        {{trans('pageTranslations.please_download_the_example_subtitle_file_translate_and_upload_for_apply')}}
                                    </div>
                                </div>
                                {!!Form::open(['route'=>['upgrade.account'],'files' => true])!!}

                                <div class="form-group">
                                    {!!Form::label('apply',trans('pageTranslations.apply'))!!}
                                    <div class="fg-line">
                                        {!!Form::select('apply', array('translatorMember'=>trans('pageTranslations.translatorMember'),'controllerMember'=>trans('pageTranslations.controllerMember'),'controllerTranslatorMember'=>trans('pageTranslations.controllerTranslatorMember')), 'turkish' , array('class' => 'selectpicker')) !!}
                                    </div>
                                </div>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file m-r-10">
                                            <span class="fileinput-new">{{trans('pageTranslations.select_file')}}</span>
                                            <span class="fileinput-exists">Change</span>
                                            {!! Form::file('srt',['accept'=>'.srt','required'=>'required']) !!}
                                        </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                </div>
                                <div class="form-group">
                                    <div class="fg-line">
                                        <a class="btn btn-primary btn-file m-r-10" href="{{url('example.srt')}}">{{trans('pageTranslations.download_the_example_subtitle')}}</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="fg-line">
                                        {!!Form::button(trans('pageTranslations.send_request'), ['id'=>'requestBtn','type'=>'submit','class' => 'btn btn-lg bgm-lime pull-right waves-effect'])!!}
                                    </div>
                                </div>
                                {!!Form::close()!!}
                            </div>
                        </div>
                        </div>
                </div>
            @include('backend.bottom')
            </div>
    </section>


@stop

@section('script')

@stop