@extends('backend.master')

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="card col-sm-12">
                <div class="card-header bgm-orange"><h2>{{$title}}</h2></div>
                <div class="card-body card-padding">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                @if($errors->any() && !$errors->first('result'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        {{$errors->first()}}
                                    </div>
                                @endif
                                @if($errors->any() && $errors->first('result'))
                                    <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        {!!$errors->first('message')!!}
                                    </div>
                                @endif
                            </div>

                            <table class="table table-hover text-center">
                                <thead>
                                <tr class="text-center">
                                    <th class="text-center"></th>
                                    <th class="text-center">{{trans('pageTranslations.username')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.member_type')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.subtitle_name')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.status')}}</th>
                                    <th class="text-center">{{trans('pageTranslations.approve')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($userUpgrades as $key=>$upgrade)
                                    <tr class="text-center">
                                        <td >{{$key+1}}</td>
                                        <td >{{$upgrade->username}}</td>
                                        <td >{{trans('pageTranslations.'.$upgrade->memberType)}}</td>
                                        <td ><a href="{{url('/show/subtitle/'.$upgrade->userID)}}">{{$upgrade->subtitleName}}</a></td>
                                        <td >@if($upgrade->success==1){{trans('pageTranslations.approved')}}@else {{trans('pageTranslations.not_approved')}} @endif</td>
                                        <td >
                                            <a href="{{url('/users/approve/'.$upgrade->userID)}}">
                                                {{trans('pageTranslations.upgrade')}}<i class="md-check"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop
