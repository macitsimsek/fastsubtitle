<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#f5f3f0;">
    <tbody><tr>
        <td>
            <table class="body" width="620" align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                    <td align="center">
                        <div class="" style="height:20px;">&nbsp;</div>
                        <table class="ecxmodule" width="600" border="0" cellpadding="0" cellspacing="0" style="background:#FFFFFF;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-left-radius:3px;border-bottom-right-radius:3px;border-top:1px solid #e3e3e3;border-bottom:1px solid #e3e3e3;border-left:1px solid #e3e3e3;border-right:1px solid #e3e3e3;overflow:hidden;">
                            <tbody><tr>
                                <td align="center" style="padding:0px;">
                                    <table class="ecxmodule ecxeducation" width="600" border="0" cellpadding="0" cellspacing="0" style="background:#FFFFFF;border-top:0;">
                                        <tbody><tr>
                                            <td class="spacer ecxmobile-h30" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td class="ecxeducation-text" align="left" style="padding:3px 30px 3px 30px;font-family:helvetica neue,helvetica,sans-serif;font-size:18px;line-height:18px;font-weight:bold;color:#454545;">
                                                {{trans('pageTranslations.hi', ['username' => $username])}},
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ecxeducation-text" align="left" style="font-family:helvetica neue,helvetica,sans-serif;font-size:16px;color:#666666;line-height:24px;padding:0 30px 20px 30px;">
                                                <b>{{trans('pageTranslations.welcome', ['username' => ''])}}</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ecxeducation-text" align="left" style="font-family:helvetica neue,helvetica,sans-serif;font-size:16px;color:#666666;line-height:24px;padding:0 30px 0px 30px;">
                                                <table>
                                                    <tr>
                                                        <td  style="width:120px;">{{trans('pageTranslations.email_address')}} </td>
                                                        <td>:</td>
                                                        <td>{{ $email }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:120px;">{{trans('pageTranslations.username',['username'=>''])}} </td>
                                                        <td>:</td>
                                                        <td>{{ $username }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:120px;">Link </td>
                                                        <td>:</td>
                                                        <td><a href="{{Request::root()}}/vl{{ $validation }}"target="_blank">{{trans('pageTranslations.click_for_link')}}</a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="ecxeducation-text" style="padding:20px 30px 0 30px;">
                                                <a href="{{Request::root()}}/vl{{ $validation }}" style="text-decoration:none;display:block;" target="_blank">
                                                    <table height="40" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody><tr>
                                                            <td height="38" style="background-color:#3983d0;padding:2px 15px 0px 15px;border-bottom:2px solid #2868a4;border-radius:3px;">
                                                                <a href="{{Request::root()}}/vl{{ $validation }}" style="font-family:helvetica,sans-serif;font-size:16px;color:#ffffff;text-decoration:none;display:block;" target="_blank">
                                                                    {{trans('pageTranslations.enter_to_fast_subtitle_com')}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacer ecxmobile-h30" height="30"></td>
                                        </tr>
                                        </tbody></table>
                                    <table class="ecxmodule ecxeducation" width="600" border="0" cellpadding="0" cellspacing="0" style="background:#FFFFFF;border-top:0;">
                                        <tbody><tr>
                                            <td class="ecxeducation-text" align="left" style="font-family:helvetica neue,helvetica,sans-serif;font-size:16px;color:#666666;line-height:24px;padding:0 30px 0px 30px;">
                                                {!!$comment!!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ecxmobile-h30" height="30"></td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <div class="" style="height:30px;">&nbsp;</div>
                        <table class="ecxmodule" width="600" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
                            <tbody><tr>
                                <td align="center">
                                    <table class="ecxsocial-footer" width="500" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
                                        <tbody><tr>
                                            <td class="ecxsocial-type" width="30">
                                                <a href="https://www.facebook.com/" title="Facebook" style="text-decoration:none;" target="_blank">
                                                    <img width="30" height="30" border="0" src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/F_icon.svg/2000px-F_icon.svg.png" style="-ms-interpolation-mode:bicubic;text-decoration:none;display:block;">
                                                </a>
                                            </td>
                                            <td class="ecxsocial-label" style="padding-left:6px;padding-right:20px;font-family:helvetica neue,helvetica,sans-serif;font-weight:bold;font-size:11px;">
                                                <a href="https://www.facebook.com/" title="Facebook" style="text-decoration:none;color:#979797;" target="_blank">
                                                    Facebook
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <div class="" style="height:30px;">&nbsp;</div>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>
    </tbody>
</table>