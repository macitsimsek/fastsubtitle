@extends('backend.emptyMaster')

@section('title')
    @if($title){{$title}} @endif
@stop

@section('loginContainer')
    <div class="lc-block @if($page=='changepass')toggled @endif" id="l-changepass">
        <form id="form-changepass">
            <input type="hidden" value="@if($page=='changepass' && $response){{$response}} @endif">
        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
            <div class="fg-line">
                <input type="password" class="form-control" id="rpassword" name="rpassword" placeholder="{{trans('pageTranslations.password')}}">
            </div>
        </div>

        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
            <div class="fg-line">
                <input type="password" class="form-control" id="rrepassword" name="rrepassword" placeholder="{{trans('pageTranslations.again_password')}}">
            </div>
        </div>
        <div class="clearfix"></div>
        </form>
    </div>

    {!! Html::script('backend/js/member.js') !!}
@stop
@section('script')
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-50970003-2', 'auto');
        ga('send', 'pageview');

    </script>
@stop