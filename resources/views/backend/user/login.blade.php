@extends('backend.emptyMaster')

@section('title')
    @if($title){{$title}} @endif
@stop

@section('loginContainer')
    <body class="login-content">

    @if($page=='login' && !Session::has('user'))
    <!-- Login -->
    <div class="lc-block toggled" id="l-login">
        @if($page=='login' && $response){{$response}} @endif
        <form id="form-login">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-person"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" id="lusername" name="lusername" placeholder="{{trans('pageTranslations.username')}}">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" id="lpassword" name="lpassword" placeholder="{{trans('pageTranslations.password')}}">
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="lcheckbox">
                    <i class="input-helper"></i>
                    Oturumu Açık Bırak
                </label>
            </div>

            <a href="#" class="btn btn-login btn-danger btn-float" onclick="login()"><i class="md md-arrow-forward"></i></a>

        </form>
            <div class="row p-t-10">
                <div class="btn-group" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="{{url('/')}}" class="btn bgm-lightblue waves-effect" >{{trans('pageTranslations.home')}}</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{url('/user/createuser')}}" class="btn bgm-blue waves-effect" >{{trans('pageTranslations.sign_up')}}</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{url('/user/forgot')}}" class="btn bgm-indigo waves-effect" >{{trans('pageTranslations.forgot_password')}}</a>
                    </div>
                </div>
            </div>
    </div>

    @elseif($page=='createuser' && !Session::has('user'))

    <!-- Register -->
    <div class="lc-block toggled" id="l-register">
        @if($page=='createuser' && $response){{$response}} @endif
        <form id="form-register">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-person"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" id="rusername" name="rusername" placeholder="{{trans('pageTranslations.username')}}">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-mail"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" id="rmail" name="rmail" placeholder="{{trans('pageTranslations.email_address')}}">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" id="rpassword" name="rpassword" placeholder="{{trans('pageTranslations.password')}}">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" id="rrepassword" name="rrepassword" placeholder="{{trans('pageTranslations.again_password')}}">
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="rcheckbox">
                    <i class="input-helper"></i>
                    {{trans('pageTranslations.agree_to_terms')}}
                </label>
            </div>

            <a href="#" class="btn btn-login btn-danger btn-float" onclick="register()"><i class="md md-arrow-forward"></i></a>

        </form>

            <div class="row p-t-10">
                <div class="btn-group" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="{{url('/')}}" class="btn bgm-lightblue waves-effect" >{{trans('pageTranslations.home')}}</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{url('/user/login')}}" class="btn bgm-blue waves-effect" >{{trans('pageTranslations.user_login')}}</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{url('/user/forgot')}}" class="btn bgm-indigo waves-effect" >{{trans('pageTranslations.forgot_password')}}</a>
                    </div>
                </div>
            </div>
    </div>

    @elseif($page=='forgot' && !Session::has('user'))
        <!-- Forgot Password -->
        <div class="lc-block toggled" id="l-forgot-password">
            @if($page=='forgot' && $response){{$response}} @endif
            <form id="form-forgot-password">
                <p class="text-left">{{trans('pageTranslations.forgot_email_message')}}</p>

                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="md md-email"></i></span>
                    <div class="fg-line">
                        <input type="text" class="form-control" name="femail" placeholder="{{trans('pageTranslations.email_address')}}">
                    </div>
                </div>

                <a href="#" class="btn btn-login btn-danger btn-float" onclick="forgot()"><i class="md md-arrow-forward"></i></a>

            </form>

                <div class="row p-t-10">
                    <div class="btn-group" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <a href="{{url('/')}}" class="btn bgm-lightblue waves-effect" >{{trans('pageTranslations.home')}}</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="{{url('/user/login')}}" class="btn bgm-blue waves-effect" >{{trans('pageTranslations.user_login')}}</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="{{url('/user/createuser')}}" class="btn bgm-indigo waves-effect" >{{trans('pageTranslations.sign_up')}}</a>
                        </div>
                    </div>
                </div>
        </div>

    @elseif($page=='changepass' && Session::has('user'))

    <div class="lc-block toggled" id="l-changepass">
        <strong>{{ Session::get('user')}}</strong> {{trans('pageTranslations.please_enter_your_password_informations')}}
        <form id="form-changepass">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" name="coldpassword" placeholder="{{trans('pageTranslations.last_password')}}">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" name="cpassword" placeholder="{{trans('pageTranslations.new_password')}}">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" name="crepassword" placeholder="{{trans('pageTranslations.again_password')}}">
                </div>
            </div>
            <div class="clearfix"></div>

            <a href="#" class="btn btn-login btn-danger btn-float" onclick="changepass()"><i class="md md-arrow-forward"></i></a>

        </form>

        <div class="row p-t-10">
            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <a href="{{url('/')}}" class="btn bgm-green waves-effect" >{{trans('pageTranslations.home')}}</a>
                </div>
            </div>
        </div>
    </div>

    @elseif($page=='newpass' && !Session::has('user'))

        <div class="lc-block toggled" id="l-newpass">
            <form id="form-newpass">
                <input type="hidden" value="@if($page=='newpass' && $response){{$response}}@endif" name="token">
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                    <div class="fg-line">
                        <input type="password" class="form-control" name="newpassword" placeholder="{{trans('pageTranslations.new_password')}}">
                    </div>
                </div>

                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="md md-vpn-key"></i></span>
                    <div class="fg-line">
                        <input type="password" class="form-control" name="renewpassword" placeholder="{{trans('pageTranslations.again_password')}}">
                    </div>
                </div>
                <div class="clearfix"></div>

                <a href="#" class="btn btn-login btn-danger btn-float" onclick="newpass()"><i class="md md-arrow-forward"></i></a>

            </form>
        </div>

    @endif

@stop

@section('script')
    <script>
        $(function(){
            $( 'body' ).keydown(function( event ) {
                if ( event.which == 13 ) {
                    $('form input:text:first').focus();
                    $('a[href="#"]').click();
                }
            });
        });
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-50970003-2', 'auto');
        ga('send', 'pageview');

    </script>
@stop