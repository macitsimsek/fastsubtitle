@extends('backend.master')

@section('meta')
    <meta name="keywords" content="Profilim, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="Profilim, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/users/profile" />
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            <div class="col-sm-8">
                @if($errors->any())
                    <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {!!$errors->first('message')!!}
                    </div>
                @endif
                <div class="card profile-view">
                    <div class="pv-header">
                        <img src="{{url('backend/img/profile-pics/'.$user->profilePicture)}}" class="pv-main" alt="">
                    </div>

                    <div class="pv-body">
                        <h2><i class="md md-verified-user"></i> {{$user->username}}</h2>
                        <small>{{trans('pageTranslations.email_address')}} :  </small>

                        <ul class="pv-contact">
                            <li><i class="md md-account-circle"></i> {{trans('pageTranslations.member_type')}} : {{trans('pageTranslations.'.$user->memberType)}}</li>
                            <li></li>
                        </ul>

                        <ul class="pv-follow">
                            <li><i class="md md-poll"></i> {{trans('pageTranslations.translated_sentence_count')}} ({{$user->translateCount}}) <i class="md md-account-box"></i> {{trans('pageTranslations.'.$user->position)}}</li>
                        </ul>

                        <ul class="pv-follow">
                            <li><i class="md md-account-child"></i> @if(isset($friends)) {{count($friends)}} @endif {{trans('pageTranslations.friend')}}</li>
                        </ul>
                        @if(isset($owner))
                        {{Form::open(array('route' => array('send.friend.request', $crypted)))}}
                            {!!Form::button(trans('pageTranslations.send_friend_request'), ['name'=>'sentFriend','type'=>'submit','class' => 'btn btn-block pv-follow-btn'])!!}
                        {{Form::close()}}
                        <a href="{{url('users/messages/'.Crypt::encrypt($user->username.'-'.$user->userID))}}" class="pv-follow-btn">{{trans('pageTranslations.send_message')}}</a>
                        @endif
                    </div>
                </div>

                <div class="card">
                    <div class="listview">
                        <div class="lv-header">
                            {{trans('pageTranslations.followed_series_movies')}}
                        </div>
                        <div class="lv-body">
                                @foreach($followedVideos->chunk(2) as $rows)
                                <div class="row">
                                    @foreach($rows as $video)
                                        <div class="col-sm-6">
                                            <a class="lv-item" href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID)}}">
                                                <div class="media">
                                                    <div class="pull-left">
                                                        <img class="lv-img-sm" src="{{url(\App\Functions::$videoImagePath.$video->picture)}}" title="{{stripslashes($video->name)}}" alt="{{stripslashes($video->name)}}">
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="lv-title">{{stripslashes($video->name)}} <i>({{trans('pageTranslations.'.$video->type)}})</i></div>
                                                        <small>{{$video->turkishName}}</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                @endforeach
                        </div>
                        <a class="lv-footer" href="#">View All</a>
                    </div>
                </div>
            </div>
            @if(!empty($activities))
                <div class="col-sm-4">
                    <div class="card">
                        <div class="listview">
                            <div class="lv-header">
                                {{trans('pageTranslations.activities')}}
                            </div>
                            <div class="lv-body">

                                @foreach($activities as $index => $activity)
                                    <a class="lv-item" href="#">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="lv-title">{{(($activities->currentPage()-1)*$activities->perPage())+$index+1}} - {{trans('pageTranslations.'.$activity->activityName)}}  <small>({{Date::parse($activity->activityDate)->diffForHumans()}})</small></div>

                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            {{$activities->render()}}
                        </div>
                    </div>
                </div>
            @endif
            <div class="clearfix"></div>
            @if(!empty($friends))
            <div class="col-sm-8">
                <div class="card">
                    <div class="listview">
                        <div class="lv-header">
                            {{trans('pageTranslations.friends')}}
                        </div>
                        <div class="lv-body">
                            @foreach($friends->chunk(3)->toArray() as $rows)
                                <div class="row">
                                    @foreach($rows as $user)
                                        <div class="col-sm-4">
                                            <a class="lv-item" href="{{url('users/profile/'.$user->username)}}">
                                                <div class="media">
                                                    <div class="pull-left">
                                                        <img class="lv-img-sm" src="{{url(\App\Functions::$profileImagePath.$user->profilePicture)}}" title="{{$user->username}}" alt="{{$user->username}}">
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="lv-title">{{$user->username}}</div>
                                                        <small>{{Date::parse($user->approveDate)->diffForHumans()}}</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                        <a class="lv-footer" href="#">View All</a>
                    </div>
                </div>
            </div>
            @endif
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')

@stop