@extends('backend.master')

@section('meta')
    <meta name="keywords" content="Film Dizi Ekle, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/addvideo" />
@stop

@section('title')
    {{trans('pageTranslations.add_serie_movie')}} - {{trans('pageTranslations.domain')}}
@stop

@if(Session::has('user'))
@section('container')
    <section id="content">
        <div class="container">
            <div class="block-header">
                <h2>{{trans('pageTranslations.add_serie_movie')}}</h2>
            </div>

            <div class="card col-sm-12">
                <div class="card-header">
                    <h2>{{trans('pageTranslations.serie_movie_adding_progress')}}</h2>
                </div>
                <div class="progressBigBlue">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" id="ilerleme" aria-valuemin="0" aria-valuemax="100" style="width:1%">

                    </div>
                </div>
            </div>

            <div class="card col-sm-12">
                <div id="properties">
                    <div class="card-header">
                        <h2>{{trans('pageTranslations.serie_movie_name_search')}}<small>{{trans('pageTranslations.firstly_enter_the_serie_movie_name')}}</small></h2>
                    </div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="row">
                                    <form id="form-add-video" method="POST" action="javascript:void(0);">
                                        <div class="form-group fg-float">
                                            <div class="fg-line">
                                                <input type="text" class="form-control" value="{{(isset(\Request::only('search')['search'])?\Request::only('search')['search']:"")}}" id="imdbtext">
                                            </div>
                                            <label class="fg-label">{{trans('pageTranslations.serie_movie_name')}}</label>
                                        </div>
                                        <button type="button" onclick="imdbAra(imdbtext.value)" class="btn btn-primary btn-icon pull-right" id="videoSearch"><span class="md md-search"></span></button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="card-header">
                                        <h2>{{trans('pageTranslations.found_on_imdb')}}</h2>
                                        <div id="aramacekimdb"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="card-header">
                                        <h2>{{trans('pageTranslations.found_on_fs')}}</h2>
                                        <div id="aramacekha"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.bottom')
        </div>
    </section>
@stop
@section('script')
    <script>
        $( "#imdbtext" ).keydown(function( event ) {
            if ( event.which == 13 ) {
                imdbAra($( "#imdbtext" ).val());
            }
        });
        $(function () {
            if($( "#imdbtext" ).val()!=""){
                imdbAra($( "#imdbtext" ).val());
            }
        })
    </script>
@stop
@endif

