@extends('backend.master')

@if(Session::has('user'))

@section('meta')
    <meta name="keywords" content="{{$title}} - {{trans('pageTranslations.edit_page')}} Film Dizi Ekle, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}} - {{trans('pageTranslations.edit_page')}} Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makeEditVideoUrl($videos->name,$videos->videoID)}}"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.edit_page')}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
            {!!Form::model($videos,['method'=>'PATCH','route'=>['videos.update',$videos->videoID]])!!}
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header bgm-red"><h2>{{$videos->name}} {{trans('pageTranslations.edit_page')}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                @if($errors->any())
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                {!!Form::label('name',trans('pageTranslations.serie_movie_name'))!!}
                                <div class="fg-line">
                                    {!!Form::text('name', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('turkishName',trans('pageTranslations.turkish_name'))!!}
                                <div class="fg-line">
                                    {!!Form::text('turkishName', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('duration',trans('pageTranslations.duration'))!!}
                                <div class="fg-line">
                                    {!!Form::text('duration', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('type',trans('pageTranslations.type'))!!}
                                <div class="fg-line">
                                    {!!Form::select('type', array('serie' => trans('pageTranslations.serie'), 'movie' => trans('pageTranslations.movie'), 'anime' => trans('pageTranslations.anime')), null , array('class' => 'selectpicker')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('kind',trans('pageTranslations.kind'))!!}
                                <div class="fg-line">
                                    {!!Form::text('kind', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('imdbPoint',trans('pageTranslations.imdb_point'))!!}
                                <div class="fg-line">
                                    {!!Form::text('imdbPoint', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('country',trans('pageTranslations.country'))!!}
                                <div class="fg-line">
                                    {!!Form::text('country', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('dateVision',trans('pageTranslations.vision_date'))!!}
                                <div class="fg-line">
                                    {!!Form::text('dateVision', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('abstract',trans('pageTranslations.content'))!!}
                                <div class="fg-line">
                                    {!!Form::textarea('abstract', stripslashes($videos->abstract), array('class' => 'form-control', 'placeholder'=>trans('pageTranslations.enter_a_content')))!!}
                                </div>
                            </div>

                        <div class="btn-colors btn-demo">
                            {!!Form::button(trans('pageTranslations.update_video').' <span></span>', ['id'=>'updateBtn','type'=>'submit','class' => 'btn btn-lg bgm-orange pull-right waves-effect'])!!}
                        </div>

                        <a href="https://www.google.com/search?q={{$videos->name}}&oq=google&sourceid=chrome&ie=UTF-8&gws_rd=ssl" target="_blank" class="btn btn-large btn-primary bgm-orange pull-left waves-effect">{{trans('pageTranslations.search',['name'=>$videos->name])}} </a>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header bgm-red"><h2>{{trans('pageTranslations.picture',['name'=>$videos->name])}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                {!!Form::label('picture',trans('pageTranslations.serie_movie_picture'))!!}
                                <div class="fg-line">
                                    {!!Form::hidden('picture') !!}
                                    {!!Form::image($videoImagePath.$videos->picture, 'picture1', ['id'=>'picture1','class' => 'img-thumbnail center-block', 'alt'=>$videos->name, 'title'=>$videos->name, 'style'=>'height: 250px;width: 175px;']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('newPic1',trans('pageTranslations.new_picture_link'))!!}
                                <div class="fg-line">
                                    {!!Form::text(null, null, array('id'=>'newPic1', 'class' => 'form-control','placeholder'=>trans('pageTranslations.enter_a_new_picture_link'))) !!}
                                </div>
                            </div>
                            <button type="button" id="getPicture" class="btn btn-primary bgm-orange pull-right">
                                {{trans('pageTranslations.get_new_picture')}}
                            </button>
                            <a href="https://www.google.com.tr/search?q={{$videos->name}}+picture&source=lnms&tbm=isch" target="_blank" class="btn btn-large btn-primary bgm-orange pull-left waves-effect">{{trans('pageTranslations.find_picture')}}</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header bgm-red"><h2>{{trans('pageTranslations.trailer',['name'=>$videos->name])}}</h2></div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="form-group">
                                {!!Form::label('trailer',trans('pageTranslations.serie_movie_trailer'))!!}
                                {!!Form::hidden('trailer') !!}
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe id="iframeTrailer" class="embed-responsive-item" src="{{stripslashes($videos->trailer)}}" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('newTra1',trans('pageTranslations.new_trailer_link'))!!}
                                <div class="fg-line">
                                    {!!Form::text(null, null, array('id'=>'newTra1', 'class' => 'form-control','placeholder'=>trans('pageTranslations.enter_a_new_trailer_link'))) !!}
                                </div>
                            </div>
                            <button type="button" id="getTrailer" class="btn btn-primary bgm-orange pull-right">
                               {{trans('pageTranslations.get_new_trailer')}}
                            </button>

                            <a href="https://www.youtube.com/results?search_query={{$videos->name}}+trailer" target="_blank" class="btn btn-large btn-primary bgm-orange pull-left waves-effect">{{trans('pageTranslations.find_trailer')}}</a>
                        </div>
                    </div>
                </div>
            </div>

            {!!Form::close()!!}
            @include('backend.bottom')
        </div>
    </section>
@stop

@section('script')
    <script>
        $(function(){
            $('#getPicture').click(function(){
                var picture = $('#newPic1').val();
                if(picture!=""){
                    $('#picture').val(picture);
                    $('#picture1').attr("src", picture).val(picture);
                }else{
                    notify('Picture link can not empty','','bgm-red',1000);
                }
            });
            $('#getTrailer').click(function(){
                var trailer = $('#newTra1').val();
                if(trailer!="" && trailer.indexOf('watch?v=')!=-1 && trailer.indexOf('youtube')!=-1){
                    var url="https://www.youtube.com/embed/"+trailer.split('watch?v=')[1];
                    $('#trailer').val(url);
                    $('#iframeTrailer').attr("src", url);
                }else{
                    notify('Trailer link empty or wrong','','bgm-red',1000);
                }
            });
            $('#updateBtn').click(function(){
                $('#updateBtn span').addClass("glyphicon glyphicon-refresh spinning");
                $('#updateBtn').addClass("disabled");
            });
        });
    </script>
@stop

@endif