@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{$title}}, Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{{$title}}, Altyazı çevirilerine hız katmak ve online çeviri ortamı oluşturmak için geliştirilmiştir.">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="https://www.fastsubtitle.com/video/last"/>
@stop

@section('title')
    {{$title}} - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
        <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h2>{{$title}}</h2>
                    </div>
                </div>
                @foreach($lastVideos as $videos)
                    <div class="card col-sm-12 card-padding">
                        <div class="col-sm-3">
                            <div class="image text-center">
                                <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}"><img src="{{url($videoImagePath.$videos->picture)}}" style="height:250px;width:175px;" alt="{{$videos->name}}" title="{{$videos->name}}" class="img-thumbnail"></a>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="card-header bgm-green">
                                <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}"><h2>{{stripslashes($videos->name)}} ({{trans('pageTranslations.'.$videos->type)}}) <small>{{stripslashes($videos->turkishName)}}</small></h2></a>

                                <ul class="actions actions-alt">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown" aria-expanded="false">
                                            <i class="md md-settings"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="#">{{trans('pageTranslations.watch_trailer')}}</a>
                                            </li>
                                            <li>
                                                <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID.'/request-subtitle')}}">{{trans('pageTranslations.add_subtitle')}}</a>
                                            </li>
                                            @if(Session::has('user'))
                                                <li>
                                                    <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID.'/edit')}}">{{trans('pageTranslations.edit')}}</a>
                                                </li>
                                            @endif
                                            <li>
                                                <a href="#">{{trans('pageTranslations.report')}}</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body card-padding">
                                <div class="pull-right bottom-left">
                                    <button onclick="location.href='{{url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}'" class="btn bgm-blue btn-float waves-effect"><i>{{$videos->imdbPoint}}</i></button>
                                </div>
                                {!!str_limit(stripslashes($videos->abstract), $limit = 500, $end = '...')!!}
                                <a href="{{url('video/'.\App\Functions::beGoodSeo(stripslashes($videos->name)).'/'.$videos->videoID)}}">{{trans('pageTranslations.more')}}</a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="card col-sm-12 card-padding">
                    <div class="card-body text-center">
                        {!! $lastVideos->render() !!}
                    </div>
                </div>
                @include('backend.bottom')

        </div>
    </section>
@stop
