@extends('backend.master')

@section('meta')
    <meta name="keywords" content="{{stripslashes($title)}},Türkçe Altyazı, Çeviri, Hızlı Altyazı, Dizi, Film, Sinema, Altyazı Sitesi">
    <meta name="description" content="{!!str_limit(stripslashes($videoProperties->abstract), $limit = 150, $end = '...')!!}">
    <meta name="robots" content="index,follow">
    <meta name="author" content="HizliAltyazi">
    <link rel="canonical" href="{{\App\Functions::makeVideoUrl($videoProperties->name,$videoProperties->videoID)}}"/>
@stop

@section('title')
    {{stripslashes($title)}} - {{trans('pageTranslations.'.$videoProperties->type)}} - Çeviri, Türkçe Altyazı - {{trans('pageTranslations.domain')}}
@stop

@section('container')
    <section id="content">
    <div class="container">

        @if($errors->any())
            @if($errors->first('for')=='video')
            <div class="col-sm-12">
                <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {!!$errors->first('message')!!}
                </div>
            </div>
            @endif
        @endif

        <div class="col-sm-8">
            <div class="card p-10">
                <div class="card-body">
                    <div class="row">
                        <div class="card-body">
                            <div class="col-sm-4 text-center" >
                                <img src="{{\App\Functions::url($videoImagePath.$videoProperties->picture)}}" title="{{stripslashes($videoProperties->name)}}" alt="{{stripslashes($videoProperties->name)}}" class="img-thumbnail"/>
                            </div>
                            <div class="col-sm-8">
                                <div class="card-header bgm-red">
                                    <h2>{{stripslashes($videoProperties->name)}} ({{$videoProperties->kind}})<small>{{stripslashes($videoProperties->turkishName)}}<i>({{trans('pageTranslations.'.$videoProperties->type)}})</i></small></h2>

                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" aria-expanded="false">
                                                <i class="md md-settings"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($videoProperties->name)).'/'.$videoProperties->videoID.'/follow')}}">{{trans('pageTranslations.subscribe')}}</a>
                                                </li>
                                                <li>
                                                    <a href="#">{{trans('pageTranslations.watch_trailer')}}</a>
                                                </li>
                                                <li>
                                                    <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($videoProperties->name)).'/'.$videoProperties->videoID.'/request-subtitle')}}">{{trans('pageTranslations.add_subtitle')}}</a>
                                                </li>
                                                @if(Session::has('user'))
                                                    <li>
                                                        <a href="{{\App\Functions::url('video/'.\App\Functions::beGoodSeo(stripslashes($videoProperties->name)).'/'.$videoProperties->videoID.'/edit')}}">{{trans('pageTranslations.edit')}}</a>
                                                    </li>
                                                @endif
                                                <li>
                                                    <a href="#">{{trans('pageTranslations.report')}}</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="col-sm-8 text-center">
                                <div class="col-sm-12 col-xs-12 p-0">
                                    <div class="col-lg-3 col-sm-3 col-xs-3 p-b-10 p-t-10">
                                        {{trans('pageTranslations.vision_date')}}: {{$videoProperties->dateVision}}
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-3 p-b-10 p-t-10">
                                        {{trans('pageTranslations.duration')}}: {{$videoProperties->duration}} {{trans('pageTranslations.min')}}
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-3 p-b-10 p-t-10">
                                        {{trans('pageTranslations.country')}}: {{$videoProperties->country}}
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-xs-3 p-b-10 p-t-10">
                                        Imdb: ({{$videoProperties->imdbPoint}})
                                    </div>
                                </div>
                                <div class="text-center col-sm-12 p-b-10 p-t-10 rating-list">
                                    <div class="rl-star">
                                        @for($i=0;$i<10;$i++)
                                            @if($i<$videoProperties->vote)
                                                <i class="md md-star active"></i>
                                            @else
                                                <i class="md md-star"></i>
                                            @endif
                                        @endfor
                                       <button class="btn"> {{$videoProperties->vote}}/10.0 - {{trans('pageTranslations.title')}}</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 p-10 p-l-25 p-r-25 text-center">
                                    @foreach(array_chunk($people->getCollection()->all(),6) as $row)
                                        <div class="row">
                                            @foreach($row as $person)
                                                <div class="col-sm-2 col-xs-4">
                                                    <div class="card">
                                                        <div class="card-header p-5 ch-alt text-center"><h2>{{trans('pageTranslations.'.$person->job)}}</h2></div>
                                                        <a href="{{\App\Functions::url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}" class="thumbnail p-0 m-0">
                                                            <img src="{{\App\Functions::url(stripslashes(\App\Functions::$peopleImagePath.$person->personPicture))}}" alt="{{stripslashes($person->personName)}}">
                                                        </a>
                                                        <div class="card-header p-5 ch-alt text-center"><h2><a class="f-400" href="{{url('people/'.\App\Functions::beGoodSeo(stripslashes($person->personName)).'/'.$person->personID)}}">{{stripslashes($person->personName)}}</a></h2><span>{{stripslashes($person->rol)}}</span></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                    <div class="card-body text-center">
                                        {!!  $people->appends(\Request::only('page_cast'))->render() !!}
                                    </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card-body card-padding bgm-gray c-white">{{stripslashes($videoProperties->abstract)}}
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-block bgm-red waves-effect" id="openTrailer">
                                    {{trans('pageTranslations.trailer',['name'=>stripslashes($videoProperties->name)])}}</button>
                                <div class="embed-responsive embed-responsive-16by9" id="trailer">
                                    <iframe class="embed-responsive-item" src="{{stripslashes($videoProperties->trailer)}}" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-sm-12 p-b-10 p-t-10 text-right">
                                <i class="md-remove-red-eye"></i> {{$videoProperties->view}} {{trans('pageTranslations.view')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header bgm-orange">
                    <h2>{{trans('pageTranslations.subtitles')}}</h2>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr class="text-center">
                            @if($videoProperties->type=='serie')
                            <th class="text-center">{{trans('pageTranslations.season')}}</th>
                            <th class="text-center">{{trans('pageTranslations.part')}}</th>
                            @endif
                            <th class="text-center">{{trans('pageTranslations.language')}}</th>
                            <th class="text-center">{{trans('pageTranslations.sender')}}</th>
                            <th class="text-center"><i class="md-file-download"></i></th>
                            <th class="text-center">{{trans('pageTranslations.downloads')}}</th>
                            <th class="text-center">{{trans('pageTranslations.sent_time')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($subtitles as $subtitle)
                        <tr class="text-center">
                            @if($videoProperties->type=='serie')
                            <td>{{$subtitle->season}}</td>
                            <td>{{$subtitle->part}}</td>
                            @endif
                            <td>{{trans('pageTranslations.'.$subtitle->language)}}</td>
                            <td><a href="{{\App\Functions::url('users/profile/'.$subtitle->username)}}">{{$subtitle->username}}</a></td>
                                <td ><a href="{{\App\Functions::url('subtitle/download/'.\App\Functions::cryptSubtitleAdress('srts@'.$subtitle->subtitleReadyID))}}">{{$subtitle->srtPath}}</a></td>
                            <td>{{$subtitle->downloads}}</td>
                            <td>{{Date::parse($subtitle->created_at)->diffForHumans()}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-body text-center">
                    {!!  $subtitles->appends(\Request::only('page_comment'))->render() !!}
                </div>
            </div>

            <div class="card">
                <div class="card-header bgm-deeporange">
                    <h2>{{trans('pageTranslations.comments')}} </h2>

                    <ul class="actions">
                        <li class="dropdown action-show">
                            <a href="#" data-toggle="dropdown">
                                <i class="md md-more-vert"></i>
                            </a>

                            <div class="dropdown-menu pull-right">
                                <p class="p-10">{{trans('pageTranslations.order_by')}}</p>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="card-body card-padding">
                    <br>
                    <div class="media-demo">
                        @foreach($comments as $comment)
                            <div class="media">
                                <div class="pull-left">
                                    <a href="#">
                                        <img class="media-object" src="{{url('backend/img/profile-pics/'.$comment->profilePicture)}}" alt="">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$comment->username}} <small>{{Date::parse($comment->created_at)->diffForHumans()}}</small></h4>
                                    {{$comment->comment}}
                                    <span class="pull-right">
                                    <button class="btn bgm-green waves-effect"><span class="md-thumb-up"></span> {{$comment->like}}</button>
                                    <button class="btn bgm-red waves-effect"><span class="md-thumb-down"></span> {{$comment->unlike}}</button>
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="card-body text-center">
                        {!!  $comments->appends(\Request::only('page_subtitle'))->render() !!}
                    </div>
                    <div class="row">
                        @if($errors->any() && $errors->first('result')=='danger')
                            @if($errors->first('for')=='comment')
                            <div class="alert alert-{{$errors->first('result')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                {!!$errors->first('message')!!}
                            </div>
                            @endif
                        @endif
                    <div class="col-sm-12 text-center">
                        <br>
                        <button class="btn bgm-deeporange waves-effect" id="addComment">{{trans('pageTranslations.add_comment')}}</button>
                        <div class="p-25" id="commentArea" style="display: none;">
                            {!!Form::open(['route'=>['comment.add',$videoProperties->videoID]])!!}
                            <textarea name="comment" id="addcommenttext" class="form-control b-4" placeholder="{{trans('pageTranslations.please_enter_a_comment')}}"></textarea>
                            <button type="submit" class="m-5 btn waves-effect ">{{trans('pageTranslations.send')}}</button>
                            {{Form::close()}}
                        </div>
                     </div>
                    </div>
                </div>
            </div>

        </div>
        @include('backend.right')
        @include('backend.bottom')

    </div>
    </section>

@stop

@section('script')
    <script>
        $('#openTrailer').click(function(){
            $('#trailer').slideToggle(600,function(){
                if($('#trailer').is(":visible") )
                    $('#openTrailer').text($('#openTrailer').text().replace("Aç","Kapat"));
                else
                    $('#openTrailer').text($('#openTrailer').text().replace("Kapat","Aç"));
            });
        });
        $('#addComment').click(function(){
            $('#commentArea').slideToggle(600,function(){
                $('#addcommenttext').focus();
            });
        });
    </script>
@stop
