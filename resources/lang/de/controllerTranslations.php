<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Controller Language Translate Lines
    |--------------------------------------------------------------------------
    |
    | Inside of Controller sentences
    |
    */

    'please_login'                              => 'Bitte anmelden',
    'successfully_added_to_waiting'             => 'Erfolgreich zum Warten hinzugefügt',
    'translate_processing_already_started'      => 'Übersetzen Verarbeitung bereits begonnen!',
    'participate_to_translate'                  => 'Teilnehmen an Übersetzen.',
    'already_requested'                         => 'Bereits angefordert!',
    'not_added'                                 => 'Nicht hinzugefügt!',
    'user_information'                          => 'Benutzerinformationen',
    'password_reset_request_receipt'            => 'Kennwort zurücksetzen Anforderung empfangen!',
    'click_link_and_reset'                      => 'Sie können Ihr Passwort ändern, indem Sie auf den obigen Link klicken.',
    'click_link_and_login'                      => 'Sie können die Website aufrufen, indem Sie auf den obigen Link klicken.',
    'have_a_nice_day'                           => 'Haben Sie einen schönen Tag.',
    'password_reset'                            => 'Passwort zurücksetzen',
    'page_not_found'                            => 'Seite nicht gefunden',
    'your_account_banned'                       => 'Konto ist gesperrt.',
    'make_email_verification'                   => 'Bitte überprüfen Sie Ihre E-Mail',
    'wrong_last_password'                       => 'Falsches letztes Passwort.',
    'user_not_found'                            => 'Benutzer nicht gefunden!',
    'wrong_username_or_password'                => 'Falscher Benutzername oder Passwort!',
    'user_successfully_verified'                => 'Benutzer erfolgreich verifiziert.',
    'user_already_verified'                     => 'Benutzer bereits bestätigt.',
    'expired_confirmation_link'                 => 'Abgelaufener Bestätigungslink.',
    'wrong_url'                                 => 'Falsche URL.',
    'choose_and_next'                           => 'Auswählen und Weiter',
    'trailer_not_found'                         => 'Trailer nicht gefunden',
    'subtitle_added_successfully'               => 'Untertitel erfolgreich hinzugefügt',
    'please_firstly_add_mean'                   => 'Bitte zuerst einen Durchschnitt hinzufügen.',
    'already_taken_back'                        => 'Schon zurückgenommen.',
    'user_profile'                              => 'Benutzerprofil.',
    'all_users'                                 => 'Alle Benutzer',
    'you_are_not_a_controller_member'           => 'Sie sind kein Controller-Mitglied.',
    'you_are_not_a_translator_member'           => 'Sie sind kein Übersetzer-Mitglied.',
    'you_are_not_a_admin'                       => 'Du bist kein Admin.',
    'successfully_added_followed'               => 'Successfully Added Followed.',
    'you_are_already_follower'                  => 'Sie sind schon Follower',
    'new_subtitle_translate_is_complated'       => 'Neue Untertitelübersetzung ist kompliziert.',
    'video_link'                                => 'Videoverbindung',
    'friend_request_has_been_send'              => 'Freundschaftswunsch wurde gesendet',
    'you_are_already_friend'                    => 'Du bist schon Freund',
    'waiting_for_approve'                       => 'Warten auf Genehmigen',
    'unauthorized_access'                       => 'Unautorisierter Zugriff',
    'became_friends'                            => 'Ist Freunde',
    'friend_request_not_found'                  => 'Freundschaftsanfrage nicht gefunden',
    'approve_friend_request'                    => 'Freundschaftsanfrage zulassen',
    'video_already_added'                       => 'Video wurde bereits hinzugefügt',

];
