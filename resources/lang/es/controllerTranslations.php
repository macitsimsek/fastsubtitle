<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Controller Language Translate Lines
    |--------------------------------------------------------------------------
    |
    | Inside of Controller sentences
    |
    */

    'please_login'                              => 'Please Login',
    'successfully_added_to_waiting'             => 'Succesfully Added to Waiting',
    'translate_processing_already_started'      => 'Translate Processing Already Started!',
    'participate_to_translate'                  => 'Participate to Translate.',
    'already_requested'                         => 'Already Requested!',
    'not_added'                                 => 'Not Added!',
    'user_information'                          => 'User Informations',
    'password_reset_request_receipt'            => 'Password Reset Request Receipt!',
    'click_link_and_reset'                      => 'You can change your password reset by clicking on the above link.',
    'click_link_and_login'                      => 'You can enter to website by clicking on the above link.',
    'have_a_nice_day'                           => 'Have a Nice Day.',
    'password_reset'                            => 'Password Reset',
    'page_not_found'                            => 'Page Not Found',
    'your_account_banned'                       => 'Account Has Banned.',
    'make_email_verification'                   => 'Please Verify Your E-mail',
    'wrong_last_password'                       => 'Wrong Last Password.',
    'user_not_found'                            => 'User Not Found!',
    'wrong_username_or_password'                => 'Wrong Username or Password!',
    'user_successfully_verified'                => 'User Successfully Verified.',
    'user_already_verified'                     => 'User Already Verified.',
    'expired_confirmation_link'                 => 'Expired Confirmation Link.',
    'wrong_url'                                 => 'Wrong URL.',
    'choose_and_next'                           => 'Choose and Next',
    'trailer_not_found'                         => 'Trailer Not Found',
    'subtitle_added_successfully'               => 'Subtitle Added Successfully',
    'please_firstly_add_mean'                   => 'Please Firstly Add an Mean.',
    'already_taken_back'                        => 'Already Taken Back.',
    'user_profile'                              => 'User Profile.',
    'all_users'                                 => 'All Users',
    'you_are_not_a_controller_member'           => 'You Are Not a Controller Member.',
    'you_are_not_a_translator_member'           => 'You Are Not a Translator Member.',
    'you_are_not_a_admin'                       => 'You Are Not a Admin.',
    'successfully_added_followed'               => 'Successfully Added Followed.',
    'you_are_already_follower'                  => 'You are Already Follower',
    'new_subtitle_translate_is_complated'       => 'New Subtitle Translate is Complated.',
    'video_link'                                => 'Video Link',
    'friend_request_has_been_send'              => 'Friend Request Has Been Send',
    'you_are_already_friend'                    => 'You are Already Friend',
    'waiting_for_approve'                       => 'Waiting for Approve',
    'unauthorized_access'                       => 'Unauthorized Access',
    'became_friends'                            => 'Became Friends',
    'friend_request_not_found'                  => 'Friend Request Not Found',
    'approve_friend_request'                    => 'Approve Friend Request',
    'video_already_added'                       => 'Video Already Added',

];
