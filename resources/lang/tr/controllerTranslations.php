<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Controller Language Translate Lines
    |--------------------------------------------------------------------------
    |
    | Inside of Controller sentences
    |
    */

    'please_login'                              => 'Kullanıcı Girişi Yapınız',
    'successfully_added_to_waiting'             => 'Başarıyla Beklenenlere Eklendi!',
    'translate_processing_already_started'      => 'Çeviri Zaten Başladı!',
    'participate_to_translate'                  => 'Çeviriye Katıl.',
    'already_requested'                         => 'Zaten İstekte Bulundunuz!',
    'not_added'                                 => 'Eklenemedi!',
    'user_information'                          => 'Üyelik Bilgileri',
    'password_reset_request_receipt'            => 'Şifre Sıfırlama Talebiniz Alındı!',
    'click_link_and_reset'                      => 'Yukarıdaki Sıfırlama Linkine Tıklayarak Şifrenizi Değiştirebilirsiniz.',
    'click_link_and_login'                      => 'Yukarıdaki Doğrulama Linkine Tıklayarak Giriş Yapabilirsiniz.',
    'have_a_nice_day'                           => 'İyi Günler Dileriz.',
    'password_reset'                            => 'Şifre Sıfırlama',
    'page_not_found'                            => 'Sayfa Bulunamadı',
    'your_account_banned'                       => 'Hesabınız Engellendi.',
    'make_email_verification'                   => 'Email Doğrulaması Yapınız.',
    'wrong_last_password'                       => 'Eski Şifre Yanlış.',
    'user_not_found'                            => 'Kullanıcı Bulunamadı!',
    'wrong_username_or_password'                => 'Kullanıcı Adı Veya Şifre Yanlış!',
    'user_successfully_verified'                => 'Üyelik Başarıyla Doğrulandı.',
    'user_already_verified'                     => 'Üyelik Zaten Doğrulandı.',
    'expired_confirmation_link'                 => 'Doğrulama Linkinin Süresi Doldu.',
    'wrong_url'                                 => 'Hatalı URL.',
    'choose_and_next'                           => 'Seç ve Devam Et',
    'trailer_not_found'                         => 'Trailer Bulunamadı',
    'subtitle_added_successfully'               => 'Altyazı Başarıyla Eklendi.',
    'please_firstly_add_mean'                   => 'Öncelikle Bir Anlam Ekleyiniz.',
    'already_taken_back'                        => 'Zaten Geri Alındı.',
    'user_profile'                              => 'Kullanıcı Profili',
    'all_users'                                 => 'Tüm Kullanıcılar',
    'you_are_not_a_controller_member'           => 'Üyeliğiniz Bir Kontrolcü Üyelik Değil.',
    'you_are_not_a_translator_member'           => 'Üyeliğiniz Bir Çevirmen Üyelik Değil.',
    'you_are_not_a_admin'                       => 'Üyeliğiniz Bir Admin Değil.',
    'successfully_added_followed'               => 'Başarıyla Takip Edilenlere Eklendi.',
    'you_are_already_follower'                  => 'Zaten Takiptesiniz.',
    'new_subtitle_translate_is_complated'       => 'Yeni Altyazı Çevirisi Tamamlandı.',
    'video_link'                                => 'Video Linki',
    'friend_request_has_been_send'              => 'Arkadaşlık İsteği Gönderildi',
    'you_are_already_friend'                    => 'Zaten Arkadaşsınız',
    'waiting_for_approve'                       => 'Onay için Bekleniyor',
    'unauthorized_access'                       => 'Yetkisiz Erişim',
    'became_friends'                            => 'Arkadaş Olundu',
    'friend_request_not_found'                  => 'Arkadaşlık İsteği Bulunamadı',
    'approve_friend_request'                    => 'Arkadaşlık İsteği Onaylandı',
    'video_already_added'                       => 'Video Zaten Eklenmiş',
    'message_already_sent'                      => 'Mesaj Zaten Gönderilmiş',
    'message_sent_successfully'                 => 'Mesaj Başarıyla Gönderildi',
    'ready_subtitle_now_found'                  => 'HAzir Altyazi Bulunamadi',
    'decrypt_not_correct'                       => 'HAtali Sifreleme',

];
