<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'   => ':attribute kabul edilmelidir.',
    'active_url' => ':attribute geçerli bir URL olmalıdır.',
    'after'      => ':attribute şundan daha eski bir tarih olmalıdır :date.',
    'alpha'      => ':attribute sadece harflerden oluşmalıdır.',
    'alpha_dash' => ':attribute sadece harfler, rakamlar ve tirelerden oluşmalıdır.',
    'alpha_num'  => ':attribute sadece harfler ve rakamlar içermelidir.',
    'array'      => ':attribute dizi olmalıdır.',
    'before'     => ':attribute şundan daha önceki bir tarih olmalıdır :date.',
    'between'    => [
        'numeric' => ':attribute :min - :max arasında olmalıdır.',
        'file'    => ':attribute :min - :max arasındaki kilobayt değeri olmalıdır.',
        'string'  => ':attribute :min - :max arasında karakterden oluşmalıdır.',
        'array'   => ':attribute :min - :max arasında nesneye sahip olmalıdır.'
    ],
    'boolean'        => ':attribute alanı sadece doğru veya yanlış olabilir.',
    'confirmed'      => ':attribute tekrarı eşleşmiyor.',
    'date'           => ':attribute geçerli bir tarih olmalıdır.',
    'date_format'    => ':attribute :format biçimi ile eşleşmiyor.',
    'different'      => ':attribute ile :other birbirinden farklı olmalıdır.',
    'digits'         => ':attribute :digits rakam olmalıdır.',
    'digits_between' => ':attribute :min ile :max arasında rakam olmalıdır.',
    'distinct'       => ':attribute tekrarlanan bir değere sahiptir.',
    'email'          => ':attribute doğru bir e-posta olmalıdır.',
    'filled'         => 'Seçili :attribute alanı doldurulmak zorundadır.',
    'exists'         => 'Seçili :attribute geçersiz.',
    'image'          => ':attribute alanı resim dosyası olmalıdır.',
    'in'             => ':attribute değeri geçersiz.',
    'in_array'       => ':attribute değeri :other içinde mevcut değil.',
    'integer'        => ':attribute rakam olmalıdır.',
    'ip'             => ':attribute geçerli bir IP adresi olmalıdır.',
    'json'           => ':attribute geçerli bir JSON dizesi olmalıdır.',
    'max'            => [
        'numeric' => ':attribute değeri :max değerinden küçük olmalıdır.',
        'file'    => ':attribute değeri :max kilobayt değerinden küçük olmalıdır.',
        'string'  => ':attribute değeri :max karakter değerinden küçük olmalıdır.',
        'array'   => ':attribute değeri :max adedinden az nesneye sahip olmalıdır.'
    ],
    'mimes' => ':attribute dosya biçimi :values olmalıdır.',
    'min'   => [
        'numeric' => ':attribute değeri :min değerinden büyük olmalıdır.',
        'file'    => ':attribute değeri :min kilobayt değerinden büyük olmalıdır.',
        'string'  => ':attribute değeri :min karakter değerinden büyük olmalıdır.',
        'array'   => ':attribute en az :min nesneye sahip olmalıdır.'
    ],
    'not_in'               => 'Seçili :attribute geçersiz.',
    'numeric'              => ':attribute rakam olmalıdır.',
    'regex'                => ':attribute biçimi geçersiz.',
    'required'             => ':attribute alanı gereklidir.',
    'required_if'          => ':attribute alanı, :other :value değerine sahip olduğunda zorunludur.',
    'required_unless'      => ':attribute alanı, :other :values değerine sahip olmadığında zorunludur.',
    'required_with'        => ':attribute alanı :values varken zorunludur.',
    'required_with_all'    => ':attribute alanı :values varken zorunludur.',
    'required_without'     => ':attribute alanı :values yokken zorunludur.',
    'required_without_all' => ':attribute alanı :values yokken zorunludur.',
    'same'                 => ':attribute ile :other eşleşmelidir.',
    'size'                 => [
        'numeric' => ':attribute :size olmalıdır.',
        'file'    => ':attribute :size kilobayt olmalıdır.',
        'string'  => ':attribute :size karakter olmalıdır.',
        'array'   => ':attribute :size nesneye sahip olmalıdır.'
    ],
    'string'   => ':attribute karakterlerden oluşmalıdır.',
    'timezone' => ':attribute geçerli bir zaman bölgesi olmalıdır.',
    'unique'   => ':attribute daha önceden kayıt edilmiş.',
    'url'      => ':attribute biçimi geçersiz.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes'           => [
        'name'                  => 'Ad',
        'username'              => 'Kullanıcı Adı',
        'email'                 => 'E-mail',
        'first_name'            => 'Ad',
        'last_name'             => 'Soyad',
        'password'              => 'Parola',
        'password_confirm'      => 'Parola Doğrulama',
        'city'                  => 'Şehir',
        'country'               => 'Ülke',
        'address'               => 'Adres',
        'phone'                 => 'Telefon',
        'mobile'                => 'Cep Telefonu',
        'age'                   => 'Yaş',
        'sex'                   => 'Cinsiyet',
        'gender'                => 'Cinsiyet',
        'day'                   => 'Gün',
        'month'                 => 'Ay',
        'year'                  => 'Yıl',
        'Y'                     => 'Yıl',
        'hour'                  => 'Saat',
        'minute'                => 'Dakika',
        'second'                => 'Saniye',
        'title'                 => 'Başlık',
        'content'               => 'İçerik',
        'description'           => 'Açıklama',
        'excerpt'               => 'Alıntı',
        'date'                  => 'Tarih',
        'time'                  => 'Zaman',
        'available'             => 'Uygun',
        'size'                  => 'Boyut',
        'profilePicture'        => 'Profil Fotoğrafı',
        'position'              => 'Pozisyon',
        'type'                  => 'Tip',
        'turkishName'           => 'Türkçe Ad',
        'kind'                  => 'Çeşit',
        'imdbPoint'             => 'Imdb Puanı',
        'duration'              => 'Süre',
        'dateVision'            => 'Vizyon Tarihi',
        'abstract'              => 'Özet',
        'trailer'               => 'Fragman',
        'picture'               => 'Resim',
        'translatingSrtPath'    => 'Çevirilen Altyazı Yolu',
        'translatedSrtPath'     => 'Çevirilmiş Altyazı Yolu',
        'fromLanguage'          => 'Hangi Dilden',
        'toLanguage'            => 'Hangi Dile',
        'season'                => 'Sezon',
        'part'                  => 'Bölüm',
        'partTrailer'           => 'Bölüm Fragmanı',
        'newMean'               => 'Yeni Anlam',
        'personName'            => 'Kişi Adı',
        'personPicture'         => 'Kişi Resmi',
        'job'                   => 'İş',
        'rol'                   => 'Rol',
        'comment'               => 'Yorum',
        'subject'               => 'Konu',
        'message'               => 'Mesaj',
        'fileType'              => 'Dosya Tipi',
    ],

];
