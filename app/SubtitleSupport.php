<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class SubtitleSupport extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subtitlesupports';//Tablo adı

    protected $primaryKey = 'subtitleSupportID';

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fromLanguage', //Bize verilen altyazı dosyası dili
        'toLanguage',   //Çevirilecek olan dil
        'sentence',     //Altyazı satırı
        'mean',         //Çevrilmiş anlam
        'createTime'    //Oluşturulma Zamanı
        ];

    public static $rules = array(
        'fromLanguage'  => 'required|in:turkish,english,french,spanish,italian,german',
        'toLanguage'    => 'required|in:turkish,english,french,spanish,italian,german|different:fromLanguage',
        'sentence'      => 'required',
        'mean'          => 'required'
    );
}
