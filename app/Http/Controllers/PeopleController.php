<?php

namespace App\Http\Controllers;

use App\Functions;
use App\People;
use App\VideoProperty;
use Illuminate\Http\Request;
use App\Activity;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class PeopleController extends Controller
{
    /**
     * @param $personName
     * @param null $personID
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showPeople($personName, $personID=null){
        if($personName=='last' && $personID==null){
            $users = DB::table('people')->orderBy('personID', 'DESC')->paginate(10);
            return view('backend.people.lastPeople',['lastPeople'=>$users,'peopleImagePath'=>$this->peopleImagePath]);
        }elseif($personName=='last' && $personID!=null) {
            return redirect('/');
        }else{
            $validator = Validator::make(
                ['personId'  =>  $personID],
                ['personId'  =>  'required|numeric|digits_between:1,10']);
            if($validator->fails()){
                return redirect('/');
            }else{
                $person =   People::where(['personID' => $personID]);
                $datas  =   $person->first();
                if ($datas) {
                    $personVideos = DB::table('people')
                        ->join('videopeople',       'videopeople.personID',     '=', 'people.personID')
                        ->join('videoproperties',   'videopeople.videoID',      '=', 'videoproperties.videoID')
                        ->select('videoproperties.videoID', 'videoproperties.name', 'videoproperties.type',
                            'videoproperties.turkishName', 'videoproperties.picture','videopeople.job')
                        ->where('people.personID','=',$datas['personID'])
                        ->orderBy('videoproperties.videoID', 'DESC')
                        ->paginate(3);
                    $popularSeries = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->where('type','serie')
                        ->orderBy('view', 'DESC')
                        ->limit(5)
                        ->get();
                    $popularMovies = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->where('type','movie')
                        ->orderBy('view', 'DESC')
                        ->limit(5)
                        ->get();
                    $popularAnimes = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->where('type','anime')
                        ->orderBy('view', 'DESC')
                        ->limit(5)
                        ->get();
                    $lastVideos = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->orderBy(DB::raw('RAND()'))
                        ->where('view','<',50)
                        ->limit(5)
                        ->get();
                    if(!Cookie::has('person-'.$datas['personID'])){
                        $datas->view++;
                        $datas->save();
                        $view = view('backend.people.person',[
                            'personVideos'      =>  $personVideos,
                            'title'             =>  $datas['personName'],
                            'person'            =>  $datas,
                            'peopleImagePath'   =>  $this->peopleImagePath,
                            'videoImagePath'    =>  $this->videoImagePath,
                            'profilImagePath'   =>  $this->profilImagePath,
                            'popularSeries'     =>  $popularSeries,
                            'popularMovies'     =>  $popularMovies,
                            'popularAnimes'     =>  $popularAnimes,
                            'lastVideos'        =>  $lastVideos]);
                        $cookie = Cookie::make('person-'.$datas['personID'], '1', 60*24);
                        return response()->make($view)->withCookie($cookie);
                    }else{
                        return view('backend.people.person',[
                            'personVideos'      =>  $personVideos,
                            'title'             =>  $datas['personName'],
                            'person'            =>  $datas,
                            'peopleImagePath'   =>  $this->peopleImagePath,
                            'videoImagePath'    =>  $this->videoImagePath,
                            'profilImagePath'   =>  $this->profilImagePath,
                            'popularSeries'     =>  $popularSeries,
                            'popularMovies'     =>  $popularMovies,
                            'popularAnimes'     =>  $popularAnimes,
                            'lastVideos'        =>  $lastVideos]);
                    }
                }else{
                    return redirect('/');
                }
            }
        }
    }

    public function peopleEdit($personName,$personID){
        $peopleImagePath     =   'backend/peopleImages/';
        if($personName!=null && $personID!=null) {
            $users  =   User::where(['username' => Session::get('user')]);
            $datas  =   $users->first();
            if ($datas) {
            $person = People::findOrFail($personID);
            $title=$person->personName;
            return view('backend.people.editPerson',compact('person','title','peopleImagePath'));
            }else{
                $title      =   'Login';
                $page       =   'login';
                $response   =   'Üye Girişi Yapınız';
                return view('backend.user.login',['page'=>$page,'title'=>$title,'response'=>$response]);
            }
        }else{
            return redirect('/');
        }
    }


    public function update($personID,Request $request){
        $users  =   User::where(['username' => Session::get('user')]);
        $datas  =   $users->first();
        if ($datas) {
            $validator = Validator::make(Input::only(['personName','content']),
                [   'personName'        => 'required|max:255|unique:people,personName,'.$personID.',personID',
                    'content'           => 'required'
                ]);
            if($validator->fails())return redirect()->back()->withErrors($validator->errors()->all());

            $person = People::findOrFail($personID);
            if($person){
                if(Input::get('personPicture')!=$person->personPicture){
                $validatorUrl =Validator::make(Input::only(['personPicture']),
                    [   'personPicture'     => 'required|url'
                    ]);
                if($validatorUrl->fails()) return redirect()->back()->withErrors($validatorUrl->errors()->all());
                $newPictureLink=Functions::downloadPicture(Input::get('personPicture'),Input::get('personName'),'peopleImages');
                Input::merge(array('personPicture' => $newPictureLink));
                }
                $person->fill(Input::only(['personName', 'personPicture', 'content']));
                if(!$person->isDirty()) return redirect('/people/' . Functions::beGoodSeo($person->personName) . '/' . $person->personID);
                $person->save();
                $users->update(['ipAdress' => $request->ip()]);
                $activity = new Activity([
                    'userID'        => $datas['userID'],
                    'committedID'   => $person->personID,
                    'activityName'  => 'people_update',
                    'activityDate'  => Carbon::now()]);
                $activity->save();
                return redirect('/people/' . Functions::beGoodSeo($person->personName) . '/' . $person->personID);
            }else{
                return redirect('/');
            }
        }else{
            $title      =   'Login';
            $page       =   'login';
            $response   =   'Üye Girişi Yapınız';
            return view('backend.user.login',['page'=>$page,'title'=>$title,'response'=>$response]);
        }
    }
}