<?php

namespace App\Http\Controllers;


use App\Friend;
use App\Functions;
use App\Message;
use App\UserUpgrade;
use App\VideoFollower;
use App\Activity;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{

    public function login()
    {
        return view('backend.user.login');
    }

    public function userLogin(Request $request){
        $values = array(
            'username'         => $request['lusername'],
            'password'         => $request['lpassword']
        );
        $validator = Validator::make($values, [
            'username'         => 'required|min:6|max:16|alpha_dash',
            'password'         => 'required|min:6|max:16|alpha_dash']);
        if($validator->fails()){
            $messages = $validator->errors()->all();
            // hataları döndük
            return $messages;
        }
        else{
            try {
                $users = User::where(['username' => $request['lusername'], 'password' => md5($request['lpassword'])]);
                $datas=$users->first();
                if ($datas) {
                    if ($datas['approved'] == 1) {
                        if ($datas['ban'] == 0) {
                            $users->update(['ipAdress' => $request->ip()]);
                            $activity = new Activity([
                                'userID'        => $datas['userID'],
                                'committedID'   => $datas['userID'],
                                'activityName'  => 'user_login',
                                'activityDate'  => Carbon::now()]);
                            $activity->save();
                            $request->session()->put('user', $datas['username']);
                            return "true";
                        } else {
                            return trans('controllerTranslations.your_account_banned');
                        }
                    } else {
                        return trans('controllerTranslations.make_email_verification');
                    }
                } else {
                    return trans('controllerTranslations.wrong_username_or_password');
                }
            }
            catch(\Exception $e){
                return $e->getMessage();
            }
        }
    }

    public function createUser(Request $request)
    {
        $values = array(
            'username'         => $request['rusername'],
            'password'         => $request['rpassword'],
            'password_confirm' => $request['rrepassword'],
            'email'            => $request['rmail']
        );
        //hata kontrolü yaptık user içindeki belirlediğimiz kurala göre
        $validator = Validator::make($values, User::$rules);

        // eğer hata varsa hatalı mesaj dön
        if ($validator->fails()) {

            // hataları değişkene atadık
            $messages = $validator->errors()->all();

            // hataları döndük
            return $messages;
        } else {
            // doğrulama başarılıysa
            // user modeli yarat ve değerleri gir
            try{
                $user           =   new User;
                $user->username =   $values['username'];
                $user->password =   md5($values['password']);
                $user->email    =   $values['email'];
                $user->ipAdress =   $request->ip();
                $user->profilePicture =   rand(1,4).'.jpg';

                // veritabanı kaydı
                $user->save();

                $activity = new Activity([
                    'userID'        =>  $user->userID,
                    'committedID'   =>  $user->userID,
                    'activityName'  =>  'create_user',
                    'activityDate'  =>  Carbon::now()]);
                $activity->save();

                $validation=$user->userID.'_'.$values['username'].'_'.Carbon::now()->addDay().'_create';;
                $validation=Crypt::encrypt($validation);
                Mail::send('backend.user.mail',[
                    'username'  =>  $values['username'],
                    'email'     =>  $values['email'],
                    'subject'   =>  trans('pageTranslations.welcome',['username'=>$values['username']]),
                    'comment'   =>  trans('controllerTranslations.click_link_and_login').' <br />'.trans('controllerTranslations.have_a_nice_day'),
                    'validation'=>$validation],function($message) use ($values){
                    $message->from("noreply@fastsubtitle.com",trans('pageTranslations.domain'))->to($values['email'],$values['username'])->subject(trans('controllerTranslations.user_information'));
                });
                // başarılı dön
                return 'true';
            }
            catch(\Exception $e){
                // do task when error
                return $e->getMessage();   // mesajları at
            }
        }
    }

    public function result($crypt){
        try{
            if(Crypt::decrypt($crypt)){
                $sonuc =  Crypt::decrypt($crypt);
                $values = explode('_',$sonuc);
                $user = User::where(['username' => $values[1], 'userID' => $values[0]])->first();
                if ($user && Carbon::now() <= $values[2]) {
                    if ($user['approved'] == 0 && $values[3]=='create') {
                        $user->approved= "1";
                        $user->save();
                        return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.user_successfully_verified')]);
                    } else if($user['approved'] == 1 && $values[3]=='create'){
                        return redirect()->action('UserController@pageShow',['page'=>'login','response'=>trans('controllerTranslations.user_already_verified')]);
                    } else if($user['approved'] == 0 && $values[3]=='forgot'){
                        $user->approved= "1";
                        $user->save();
                        return redirect()->action('UserController@pageShow',['page'=>'login','response'=>trans('controllerTranslations.user_successfully_verified')]);
                    } else if($user['approved'] == 1 && $values[3]=='forgot'){
                        return redirect()->action('UserController@pageShow',['page'=>'newpass','response'=>base64_encode($datas['username'].'ö'.crypt('newpass'))]);
                    }
                    else{
                        return view('backend.user.login');
                    }
                }
                else{
                    return redirect()->action('UserController@pageShow', ['page'=>'forgot','response'=>trans('controllerTranslations.expired_confirmation_link')]);
                }
            }
        }
        catch(\Exception $e){
            return redirect()->action('UserController@pageShow', ['page'=>'forgot','response'=>trans('controllerTranslations.wrong_url')]);
        }
    }

    public function pageShow($page,$response=null){
        $title=trans('controllerTranslations.page_not_found');
        if($page=='login' && !session('user')) $title=trans('pageTranslations.user_login');
        elseif($page=='changepass' && !session('user')){
            $title=trans('pageTranslations.user_login');
            $page='login';
            $response=trans('controllerTranslations.please_login');
        }
        elseif($page=='changepass' && session('user')) $title=trans('pageTranslations.change_password');
        elseif($page=='newpass' && !session('user') && $response!=null) {
            $values = explode('ö',base64_decode($response));
            if(count($values)>1 && crypt($values[1],'newpass')) {
                $response=base64_encode($values[0]);
                $title=trans('pageTranslations.new_password');
            }else{
                return redirect()->back();
            }
        }
        elseif($page=='createuser' && !session('user')) $title=trans('pageTranslations.sign_up');
        elseif($page=='forgot' && !session('user')) $title=trans('pageTranslations.forgot_password');
        else
            return redirect('/');
        return view('backend.user.login',['page'=>$page,'title'=>$title,'response'=>$response]);
    }

    public function changePass(Request $request){
        $values = array(
            'old_password'      => $request['coldpassword'],
            'password'          => $request['cpassword'],
            'password_confirm'  => $request['crepassword']
        );
        $validator = Validator::make($values, [
            'old_password'          => 'required|min:6|max:16|alpha_dash',
            'password'              => 'required|min:6|max:16|alpha_dash|different:old_password',
            'password_confirm'      => 'required|min:6|max:16|alpha_dash|same:password']);
        if($validator->fails()){
            $messages = $validator->errors()->all();

            // hataları döndük
            return $messages;
        }
        else
        {
            try {
                if(session('user')){
                    $username=session('user');
                    $users = User::where(['username' => $username, 'password' => md5($values['old_password'])]);
                    $datas=$users->first();
                    if ($datas) {
                        if ($datas['approved'] == 1) {
                            if ($datas['ban'] == 0) {
                                $users->update(['ipAdress' => $request->ip(),'password'=>md5($values['password'])]);
                                $activity = new Activity([
                                    'userID'        => $datas['userID'],
                                    'committedID'   => $datas['userID'],
                                    'activityName'  => 'change_password',
                                    'activityDate'  => Carbon::now()]);
                                $activity->save();
                                return "true";
                            } else {
                                return trans('controllerTranslations.your_account_banned');
                            }
                        } else {
                            return trans('controllerTranslations.make_email_verification');
                        }
                    } else {
                        return trans('controllerTranslations.wrong_last_password');
                    }
                } else{
                    return trans('controllerTranslations.please_login');
                }
            }
            catch(\Exception $e){
                return $e->getMessage();
            }
        }
    }

    public function upgradeAccount(){
        if(session('user')){
            return view('backend.user.upgradeAccount')->with(['title' => trans('pageTranslations.upgrade_account')]);
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function showUpgradeRequests(){
        if(session('user')){
            $users = User::where(['username' => session('user')])->first();
            if($users->memberType=='admin'){
                $userUpgrades = DB::table('userupgrade')
                    ->join('users', 'users.userID', '=', 'userupgrade.userID')
                    ->select('users.userID', 'users.username', 'userupgrade.memberType',
                        'userupgrade.subtitleName', 'userupgrade.success')
                    ->orderBy('upgradeID', 'DESC')
                    ->paginate(25);
                return view('backend.user.showUpgradeRequests')->with(['title' => trans('pageTranslations.last_upgrade_requests'), 'userUpgrades'=>$userUpgrades]);
            }else{
                return redirect('/');
            }
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }
    public function approveUpgradeUser($userID){
        if(session('user')){
            $approveUserUpgrade = UserUpgrade::where(['userID'=>$userID])->first();
            $users =    User::where(['userID' => $userID])->first();
            $approveUserUpgrade->success="1";
            $approveUserUpgrade->save();
            $users->memberType=$approveUserUpgrade->memberType;
            $users->save();
            return redirect()->back()
                ->withErrors(['result'=>'success','message'=>trans('pageTranslations.approved')]);
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }
    public function showExampleSubtitle($userID){
        if(session('user')){
            $upgrade = UserUpgrade::where(['userID'=>$userID])->first();
            return view('backend.user.showExampleSubtitle')->with(['title' => trans('pageTranslations.show_subtitle'), 'upgrade'=>$upgrade]);
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function upgradeAccountPost(){
        if(session('user')){
            $yol = 'upgradeSrt/';
            $filePath='backend/'.$yol;
            $username=session('user');
            $users = User::where(['username' => $username])->first();

            $aValidate = Validator::make(
                [   'fileType'              =>  Input::file('srt')->getClientOriginalExtension(),
                    'file'                  =>  Input::file('srt'),
                    'userID'                =>  $users->userID],
                [   'fileType'              =>  'in:srt',
                    'file'                  =>  'required|max:1000',
                    'userID'                =>  'required|unique:userupgrade']);
            if($aValidate->fails()) return redirect()->back()->withErrors($aValidate->errors()->all());
            $memberType =Input::get('apply');
            $fileName = Input::file('srt')->getClientOriginalName();
            $fileName = str_replace('.srt','',$fileName).'_'.session('user').'.srt';
            $userUpgrade = new UserUpgrade();
            $userUpgrade->userID        =   $users->userID;
            $userUpgrade->memberType    =   $memberType;
            $userUpgrade->subtitleName  =   $fileName;
            if (!file_exists(storage_path().'/'.$filePath)) mkdir(storage_path().'/'.$filePath, 0777, true);
            Input::file('srt')->move(storage_path().'/'.$filePath,  $fileName);
            $userUpgrade->save();
            $activity = new Activity([
                'userID'        => $users->userID,
                'committedID'   => $userUpgrade->upgradeID,
                'activityName'  => 'upgrade_requested',
                'activityDate'  => Carbon::now()]);
            $activity->save();
            return redirect()->back()
                ->withErrors(['result'=>'success','message'=>trans('pageTranslations.upgrade_requested')]);
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function newPass(Request $request){
        $values = array(
            'token'             => $request['token'],
            'password'          => $request['newpassword'],
            'password_confirm'  => $request['renewpassword']
        );
        $validator = Validator::make($values, [
            'token'                 => 'required',
            'password'              => 'required|min:6|max:16|alpha_dash',
            'password_confirm'      => 'required|min:6|max:16|alpha_dash|same:password']);
        if($validator->fails()){
            $messages = $validator->errors()->all();

            // hataları döndük
            return $messages;
        }
        else
        {
            try {
                $users = User::where(['username' => base64_decode($values['token'])]);
                $datas=$users->first();
                if ($datas) {
                    if ($datas['approved'] == 1) {
                        if ($datas['ban'] == 0) {
                            $users->update(['ipAdress' => $request->ip(),'password'=>md5($values['password'])]);
                            $activity = new Activity([
                                'userID'        => $datas['userID'],
                                'committedID'   => $datas['userID'],
                                'activityName'  => 'new_password',
                                'activityDate'  => Carbon::now()]);
                            $activity->save();
                            return "true";
                        } else {
                            return trans('controllerTranslations.your_account_banned');
                        }
                    } else {
                        return trans('controllerTranslations.make_email_verification');
                    }
                } else {
                    return trans('controllerTranslations.user_not_found');
                }
            }
            catch(\Exception $e){
                return $e->getMessage();
            }
        }
    }

    public function forgotPass(Request $request){
        $validator = Validator::make([
            'email'          => $request['femail']], [
            'email'          => 'required|email|exists:users']);

        if($validator->fails()){
            $messages = $validator->errors()->all();

            // hataları döndük
            return $messages;
        }else{
            try{
                $user = User::where(['email' => $request['femail']]);
                $datas=$user->first();
                $activity = new Activity([
                    'userID'        =>  $datas['userID'],
                    'committedID'   =>  $datas['userID'],
                    'activityName'  =>  'forgot_password',
                    'activityDate'  =>  Carbon::now()]);
                $activity->save();

                $validation=$datas['userID'].'_'.$datas['username'].'_'.Carbon::now()->addDay().'_forgot';
                $validation=Crypt::encrypt($validation);
                Mail::queue('backend.user.mail',[
                    'username'  =>  $datas['username'],
                    'email'     =>  $datas['email'],
                    'subject'   =>  trans('controllerTranslations.password_reset_request_receipt'),
                    'comment'   =>  trans('controllerTranslations.click_link_and_reset').'<br />'.trans('controllerTranslations.have_a_nice_day'),
                    'validation'=>  $validation],function($message) use ($datas){
                    $message->from('noreply@fastsubtitle.com',trans('pageTranslations.domain'))->to($datas['email'],$datas['username'])->subject(trans('controllerTranslations.password_reset'));
                });
                // başarılı dön
                return 'true';
            }catch (\Exception $e){
                return $e->getMessage();
            }
        }
    }

    public function userExit(){
        if(session('user')){
            session()->forget('user');
        }
        return redirect('/');
    }

    public function profile($username=null){
        try {
            if($username!=null && session('user')!=$username){
                if(session('user')){
                    $user = User::where(['username' => $username])->first();
                    if($user){
                        $followedVideos = VideoFollower::where(['followerID'=>$user->userID])
                            ->join('videoproperties','videoproperties.videoID','=','videofollowers.videoID')
                            ->get();
                        $usernameSigned=session('user');
                        $userSigned = User::where(['username' => $usernameSigned])->first();
                        $crypted= Crypt::encrypt($userSigned->userID."-".$user->userID);
                        $query1=DB::table('friends')->select(DB::raw('senderID as userID, users.username, users.profilePicture, friends.approveDate'))->where('receiverID',$user->userID)
                            ->orderBy('approveDate','DESC')->where(['friends.approve'=>"1"])
                            ->join('users','friends.senderID','=','users.userID');
                        $friends=DB::table('friends')->select(DB::raw('receiverID as userID, users.username, users.profilePicture, friends.approveDate'))->where('senderID',$user->userID)
                            ->orderBy('approveDate','DESC')->where(['friends.approve'=>"1"])
                            ->join('users','friends.receiverID','=','users.userID')->union($query1)->get();
                        $userNotifications = Friend::where(['friends.receiverID'=>$userSigned->userID])
                            ->join('users','friends.receiverID','=','users.userID')
                            ->join('activities','friends.friendID','=','activities.committedID')
                            ->where(['activities.activityName'=>'friend_request_has_been_send'])
                            ->where(['friends.approve'=>"0"])
                            ->get();
                        return view('backend.user.profile',[
                            'userNotifications' =>  $userNotifications,
                            'crypted'           =>  $crypted,
                            'owner'             =>  "owner" ,
                            'friends'           =>  $friends,
                            'user'              =>  $user ,
                            'title'             =>  trans('controllerTranslations.user_profile'),
                            'profilImagePath'   =>  $this->profilImagePath,
                            'videoImagePath'    =>  $this->videoImagePath,
                            'followedVideos'    =>  $followedVideos]);
                    }else{
                        return redirect('/user/login');
                    }
                }else{
                    return redirect('/user/login');
                }
            }else{
                if(session('user')){
                    $username=session('user');
                    $user = User::where(['username' => $username])->first();
                    $activities = Activity::where(['userID'=>$user->userID])->orderBy('activityID','desc')->simplePaginate(20);
                    if($user){
                        $followedVideos = VideoFollower::where(['followerID'=>$user->userID])
                            ->join('videoproperties','videoproperties.videoID','=','videofollowers.videoID')
                            ->get();
                        $userNotifications = Friend::where(['friends.receiverID'=>$user->userID])
                            ->join('users','friends.receiverID','=','users.userID')
                            ->join('activities','friends.friendID','=','activities.committedID')
                            ->where(['activities.activityName'=>'friend_request_has_been_send'])
                            ->where(['friends.approve'=>"0"])
                            ->get();
                        $query1=DB::table('friends')->select(DB::raw('senderID as userID, users.username, users.profilePicture, friends.approveDate'))->where('receiverID',$user->userID)->join('users','friends.senderID','=','users.userID')
                            ->where(['friends.approve'=>"1"]);
                        $friends=DB::table('friends')->select(DB::raw('receiverID as userID, users.username, users.profilePicture, friends.approveDate'))->where('senderID',$user->userID)->union($query1)
                            ->where(['friends.approve'=>"1"])
                            ->join('users','friends.receiverID','=','users.userID')->get();
                        return view('backend.user.profile',[
                            'userNotifications' =>  $userNotifications,
                            'friends'           =>  $friends,
                            'user'              =>  $user,
                            'activities'        =>  $activities,
                            'title'             =>  trans('controllerTranslations.user_profile'),
                            'profilImagePath'   =>  $this->profilImagePath,
                            'videoImagePath'    =>  $this->videoImagePath,
                            'followedVideos'    =>  $followedVideos]);
                    }else{
                        return redirect('/user/login');
                    }
                }else{
                    return redirect('/user/login');
                }
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }


    public function messages($token=null){
        try {
            if($token!=null && session('user')){
                $result         =   Crypt::decrypt($token);
                $result         =   explode('-',$result);
                $userID         =   $result[1];
                $username       =   session('user');
                $user           =   User::where(['username' => $username])->first();
                $messageReceiver=   User::where(['userID' => $userID])->first();
                if($user){
                    $senderQuery=DB::table('friends')->select(DB::raw('senderID as userID, users.username, users.profilePicture'))->where('receiverID',$user->userID)->join('users','friends.senderID','=','users.userID')
                        ->where(['friends.approve'=>"1"]);
                    $messagersQuery=DB::table('messages')->select(DB::raw('receiverID as userID, users.username, users.profilePicture'))->where(['senderID'=>$user->userID])
                        ->join('users','messages.senderID','=','users.userID');
                    $messagersQuery2=DB::table('messages')->select(DB::raw('senderID as userID, users.username, users.profilePicture'))->where(['receiverID'=>$user->userID])
                        ->join('users','messages.receiverID','=','users.userID');
                    $friends=DB::table('friends')->select(DB::raw('receiverID as userID, users.username, users.profilePicture'))->where('senderID',$user->userID)
                        ->union($senderQuery)
                        ->where(['friends.approve'=>"1"])
                        ->join('users','friends.receiverID','=','users.userID')->distinct()->get();
                    $allMessages= DB::table('messages')->where(['senderID'=>$user->userID,'receiverID'=>$messageReceiver->userID])
                        ->join('users','messages.senderID','=','users.userID')
                        ->orWhere(['senderID'=>$messageReceiver->userID,'receiverID'=>$user->userID])->orderBy('messageDate')->get();
                    if($allMessages){
                        Message::where(['senderID'=>$messageReceiver->userID,'receiverID'=>$user->userID])->update(['seen'=>"1"]);
                    }
                    return view('backend.user.message',[
                        'token'             =>  $token,
                        'friends'           =>  $friends,
                        'allMessages'       =>  $allMessages,
                        'messageReceiver'   =>  $messageReceiver,
                        'title'=>$username]);

                }else{
                    return redirect('/user/login');
                }
            }if($token==null && session('user')) {
                $username=session('user');
                $user = User::where(['username' => $username])->first();
                if($user){
                    $query1=DB::table('friends')->select(DB::raw('senderID as userID, users.username, users.profilePicture, friends.approveDate'))->where('receiverID',$user->userID)->join('users','friends.senderID','=','users.userID')
                        ->where(['friends.approve'=>"1"]);
                    $friends=DB::table('friends')->select(DB::raw('receiverID as userID, users.username, users.profilePicture, friends.approveDate'))->where('senderID',$user->userID)->union($query1)
                        ->where(['friends.approve'=>"1"])
                        ->join('users','friends.receiverID','=','users.userID')->get();

                    return view('backend.user.message',[
                        'friends'           =>  $friends,
                        'title'=>$username]);

                }else{
                    return redirect('/user/login');
                }
            }else{
                return redirect('/user/login');
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function sendMessage($token){
        try {
            if($token!=null && session('user')){
                $result         =   Crypt::decrypt($token);
                $result         =   explode('-',$result);
                $userID         =   $result[1];
                $username       =   session('user');
                $user           =   User::where(['username' => $username])->first();
                $messageReceiver=   User::where(['userID' => $userID])->first();
                $messageText=trim(Functions::addSlashAndRemoveTags(Input::get('message')));
                if($user && $messageReceiver){
                    $lastMessageID=Message::orderBy('messageID','DESC')->select('messageID')->first()->messageID;
                    $messageCheck = DB::table('messages')->where(['senderID'=>$user->userID,'receiverID'=>$messageReceiver->userID,'message'=>$messageText,'messageID'=>$lastMessageID])->first();
                    if ($messageCheck)return redirect()->back()
                        ->withErrors(['result'=>'warning','message'=>trans('controllerTranslations.message_already_sent')]);
                    $message = new Message();
                    $message->senderID=$user->userID;
                    $message->receiverID=$messageReceiver->userID;
                    $message->message=$messageText;
                    $message->messageDate=Carbon::now();
                    if($message->save()) {
                        $activity = new Activity([
                            'userID'        => $user->userID,
                            'committedID'   => $message->messageID,
                            'activityName'  => 'send_message',
                            'activityDate'  => Carbon::now()]);
                        $activity->save();
                        $allMessages= DB::table('messages')->where(['senderID'=>$user->userID,'receiverID'=>$messageReceiver->userID])
                            ->join('users as u1','messages.senderID','=','u1.userID')
                            ->orWhere(['senderID'=>$messageReceiver->userID,'receiverID'=>$user->userID])->get();
                        $query1=DB::table('friends')->select(DB::raw('senderID as userID, users.username, users.profilePicture, friends.approveDate'))->where('receiverID',$user->userID)->join('users','friends.senderID','=','users.userID')
                            ->where(['friends.approve'=>"1"]);
                        $friends=DB::table('friends')->select(DB::raw('receiverID as userID, users.username, users.profilePicture, friends.approveDate'))->where('senderID',$user->userID)->union($query1)
                            ->where(['friends.approve'=>"1"])
                            ->join('users','friends.receiverID','=','users.userID')->get();
                        return view('backend.user.message',[
                            'token'             =>  $token,
                            'friends'           =>  $friends,
                            'allMessages'       =>  $allMessages,
                            'messageReceiver'   =>  $messageReceiver,
                            'title'=>$username])->withErrors(['result'=>'success','message'=>trans('controllerTranslations.message_sent_successfully')]);
                    }else{
                        return redirect()->back();
                    }
                }else{
                    return redirect('/user/login');
                }
            }else{
                return redirect('/user/login');
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function allProfiles(){
        try {
            $users = User::all();
            if($users){
                return view('backend.user.allProfiles',['users'=>$users ,'title'=>trans('controllerTranslations.all_users')]);
            }else{
                return redirect('/user/login');
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function sendFriendRequest($cryptedstring){
        try{
            if(Crypt::decrypt($cryptedstring)) {
                $result         =   Crypt::decrypt($cryptedstring);
                $result         =   explode('-',$result);
                $sender         =   $result[0];
                $receiver       =   $result[1];
                $userSender     =   User::where(['userID' => $sender])->first();
                $userReceiver   =   User::where(['userID' => $receiver])->first();
                if(!$userSender || !$userReceiver) return redirect()->back()
                    ->withErrors(['result'=>'warning','message'=>trans('controllerTranslations.user_not_found')]);
                $friendCheck = Friend::where(['senderID'=>$sender,'receiverID'=>$receiver])
                    ->orWhere(['senderID'=>$receiver,'receiverID'=>$sender])
                    ->first();
                if(!$friendCheck){
                    $friend = new Friend();
                    $friend->senderID=$sender;
                    $friend->receiverID=$receiver;
                    $friend->save();
                    $activity = new Activity([
                        'userID'        => $sender,
                        'committedID'   => $friend->friendID,
                        'activityName'  => 'friend_request_has_been_send',
                        'activityDate'  => Carbon::now()]);
                    $activity->save();
                    return redirect()->back()
                        ->withErrors(['result'=>'success','message'=>trans('controllerTranslations.friend_request_has_been_send')]);
                }else{
                    if($friendCheck->approve==1){
                        return redirect()->back()
                            ->withErrors(['result'=>'warning','message'=>trans('controllerTranslations.you_are_already_friend')]);
                    }else{
                        return redirect()->back()
                            ->withErrors(['result'=>'warning','message'=>trans('controllerTranslations.waiting_for_approve')]);
                    }
                }
            }else{
                return redirect()->back()
                    ->withErrors(['result'=>'warning','message'=>trans('controllerTranslations.unauthorized_access')]);
            }
        }
        catch(\Exception $e){
            return redirect('/');
        }
    }

    public function approveFriendRequest($cryptedstring){
        try{
            if(Crypt::decrypt($cryptedstring)) {
                $result = Crypt::decrypt($cryptedstring);
                $result=explode('-',$result);
                $sender=trim($result[0]);
                $receiver=trim($result[1]);
                $friendCheck = Friend::where(['senderID'=>$sender,'receiverID'=>$receiver])
                    ->orWhere(['senderID'=>$receiver,'receiverID'=>$sender])
                    ->first();
                if($friendCheck){
                    if($friendCheck->approve==0){
                        $friendCheck->approve="1";
                        $friendCheck->approveDate=Carbon::now();
                        $friendCheck->save();
                        $activity = new Activity([
                            'userID'        => $sender,
                            'committedID'   => $friendCheck->friendID,
                            'activityName'  => 'approve_friend_request',
                            'activityDate'  => Carbon::now()]);
                        $activity->save();
                        return ['code'=>200,'message'=>trans('controllerTranslations.became_friends')];
                    }else{
                        return ['code'=>400,'message'=>trans('controllerTranslations.you_are_already_friend')];
                    }
                }else{
                    return ['code'=>400,'message'=>trans('controllerTranslations.friend_request_not_found')];
                }
            }else{
                return ['code'=>400,'message'=>trans('controllerTranslations.unauthorized_access')];
            }
        }
        catch(\Exception $e){
            return ['code'=>400,'message'=>$e->getMessage()];
        }
    }


}
