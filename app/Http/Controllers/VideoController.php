<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use App\Functions;
use App\People;
use App\VideoComment;
use App\VideoFollower;
use App\VideoPeople;
use App\VideoProperty;
use App\Activity;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Stichoza\GoogleTranslate\TranslateClient;

class VideoController extends Controller{
    public function getFromImdb(Request $request){
        $sorgu		=	$request['sorgu'];
        $sorgu		=	str_replace(" ", "+", $sorgu);
        $page	 	=	file_get_contents('http://www.imdb.com/find?q='.$sorgu);
        $parcala	=	('@<tr class="(.*?)"> <td class="primary_photo"> <a href="(.*?)" ><img src="(.*?)" /></a> </td> <td class="result_text"> <a href="(.*?)" >(.*?)</a>(.*?)</td> </tr>@');
        preg_match_all($parcala,$page,$cikti);
        $sonucdizi	=	$cikti[5];
        $linkdizi	=	$cikti[2];
        $turdizi	=	$cikti[6];
        for ($i=0; $i < count($sonucdizi); $i++) {
            echo '<div class="radio m-b-15">
	    <label>
	    <input type="radio" name="radios1" value="'.explode("?",trim($linkdizi[$i]))[0].'">
	    <i class="input-helper"></i>
	    '.trim($sonucdizi[$i]).' '.strip_tags($turdizi[$i]).'
	    </label>
	    </div>';
        }
        if(count($sonucdizi)>0)
            echo '<button type="button" onclick="getVideo()" class="btn btn-primary btn-block waves-effect" id="btnImdb">'.trans('controllerTranslations.choose_and_next').'<span class="md md-arrow-forward"></span></button>';

    }

    public function getFromFS(Request $request){
        $input 	=	$request['sorgu'];
        $input 	=	strtolower($input);
        $words	=	array();
        $video  =   VideoProperty::select('videoID','name','type');
        $videos=    $video->get();

        foreach($videos as $items){
            $videoName		=	strtolower($items['name']);
            $lev			=   self::lev($input,$videoName);
            if($lev<=(strlen($input)*0.5)){
                $helperArray	=	array();
                array_push($helperArray, $items['name']);
                array_push($helperArray, $lev);
                array_push($helperArray, $items['type']);
                array_push($helperArray, $items['videoID']);
                array_push($words, $helperArray);
            }
        }


        if(count($words)!=0)
        {
            $cikti	=	self::dizi_sirala($words,1,'asort');

            $lenght=10;
            if (count($cikti)<$lenght)
            {
                $lenght		=	count($cikti);
            }

            for ($i=0; $i < $lenght; $i++) {
                echo '<div class="radio m-b-15">
	    <label>
	    <input type="radio" name="radios2" value="'.url('video/'.\App\Functions::beGoodSeo(stripslashes($cikti[$i][0])).'/'.$cikti[$i][3]).'">
	    <i class="input-helper"></i>
	    '.$cikti[$i][0].' ('.$cikti[$i][2].')
	    </label>
	    </div>';
            }
            echo '<button type="button" class="btn btn-primary btn-block waves-effect" id="btnHa" onclick="goToVideo()">
                    '.trans('controllerTranslations.choose_and_next').'
                    <span class="md md-arrow-forward"></span></button>';
        }
    }

    private function dizi_sirala($dizi,$ref,$fonk){
        $son=array();
        for($a=0;$a<count($dizi);$a++){
            $tampon[$a]=$dizi[$a][$ref];
        }
        $fonk($tampon);
        foreach($tampon as $key=>$data){
            $son[]=$dizi[$key];
        }
        return $son;
    }

    private function lev($s,$t) {
        $m = strlen($s);
        $n = strlen($t);

        for($i=0;$i<=$m;$i++) $d[$i][0] = $i;
        for($j=0;$j<=$n;$j++) $d[0][$j] = $j;

        for($i=1;$i<=$m;$i++) {
            for($j=1;$j<=$n;$j++) {
                $c = ($s[$i-1] == $t[$j-1])?0:1;
                $d[$i][$j] = min($d[$i-1][$j]+1,$d[$i][$j-1]+1,$d[$i-1][$j-1]+$c);
            }
        }

        return $d[$m][$n];
    }

    public function getPageHtml(Request $request){
        $input 	    =	$request['search'];
        $page		=	file_get_contents($input);
        return $page;
    }

    public function getIMDB(Request $request){
        $input 	    =	$request['search'];
        $name =\GuzzleHttp\json_decode(file_get_contents('http://imdb.wemakesites.net/api/tt0234215?api_key=a9a4aa47-9dc3-428c-ac9e-546a4778aed9'),true);
        return $name['data']['cast'];
        return \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&formatversion=2&titles='.$name),true)['query']['pages'][0]['extract'];
    }

    public function getWiki(Request $request){
        $input 	    =	$request['search'];
        $input      =   str_replace(' ','+',$input);
        $name       =   \GuzzleHttp\json_decode(file_get_contents('https://en.wikipedia.org/w/api.php?action=query&list=search&utf8=&formatversion=2&format=json&srsearch='.$input),true);
        foreach ($name['query']['search'] as $perName){
            $name      =   str_replace(' ','+',$perName['title']);
            if($name==$input){
                $content    =   \GuzzleHttp\json_decode(file_get_contents('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&formatversion=2&titles='.$name),true)['query']['pages'][0]['extract'];
                $tr     =   new TranslateClient('en','tr');
                $googleTranslation  =   $tr->translate($content);
                return $googleTranslation;
            }
        }
        $name       =   \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?action=query&list=search&utf8=&formatversion=2&format=json&srsearch='.$input),true);
        foreach ($name['query']['search'] as $perName){
            $name   =   str_replace(' ','+',$perName['title']);
            if($name==$input){
                return \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&formatversion=2&titles='.$name),true)['query']['pages'][0]['extract'];
            }
        }
    }

    public function getWikiwithPictures(Request $request){
        $input 	    =	$request['search'];
        $input      =   str_replace(' ','+',$input);
        $name       =   \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?action=query&list=search&utf8=&formatversion=2&format=json&srsearch='.$input),true);
        $name       =   str_replace(' ','+',$name['query']['search'][0]['title']);
        if($name!=$input){ return "false";}
        $info   =   \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts|pageimages&exintro=&explaintext=&formatversion=2&pithumbsize=250&titles='.$name),true);
        return ['abstract'=>isset($info['query']['pages'][0]['extract'])?$info['query']['pages'][0]['extract']:'','picture'=>isset($info['query']['pages'][0]['thumbnail']['source'])?$info['query']['pages'][0]['thumbnail']['source']:'backend/img/empty.jpg'];
    }

    public function getOnlyWikiPictures(Request $request){
        $input 	    =	$request['search'];
        $input      =   str_replace(' ','+',$input);
        $name       =   \GuzzleHttp\json_decode(file_get_contents('https://en.wikipedia.org/w/api.php?action=query&list=search&utf8=&formatversion=2&format=json&srsearch='.$input),true);
        $name       =   str_replace(' ','+',$name['query']['search'][0]['title']);
        if($name!=$input){ return "false";}
        $info   =   \GuzzleHttp\json_decode(file_get_contents('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=pageimages&formatversion=2&pithumbsize=250&titles='.$name),true);
        return ['picture'=>isset($info['query']['pages'][0]['thumbnail']['source'])?$info['query']['pages'][0]['thumbnail']['source']:'backend/img/empty.jpg'];
    }

    public function getIMDBPictures(Request $request){
        $input 	    =	$request['search'];
        $input      =   str_replace(' ','+',$input);
        $imdbID     =   "";
        $name       =   \GuzzleHttp\json_decode(file_get_contents('http://imdb.wemakesites.net/api/search?api_key=a9a4aa47-9dc3-428c-ac9e-546a4778aed9&q='.$input),true);
        foreach ($name['data']['results']['names'] as $perName){
            $name      =   str_replace(' ','+',$perName['title']);
            if($name==$input){
                $imdbID=$perName['id'];
                break;
            }
        }
        if($imdbID==""){ return "false";}
        $result     =   \GuzzleHttp\json_decode(file_get_contents('http://imdb.wemakesites.net/api/'.$imdbID.'?api_key=a9a4aa47-9dc3-428c-ac9e-546a4778aed9'),true);
        return ['picture'=>$result['data']['image']];
    }

    public function getTrailer(Request $request){
        $videoName  =   $request['sorgu'];
        $input		=	$videoName." trailer";
        $results = Youtube::searchVideos($input,4);
        foreach ($results as $index=>$video){
            $links[$index]['id']=$video->id->videoId;
            $links[$index]['title'] = Youtube::getVideoInfo([$video->id->videoId])[0]->snippet->title;
        }
        return self::trailerPush($videoName,$links);
    }

    public function searchIMDB(Request $request){
        $input 	    =	$request['search'];
        $where 	    =	$request['where'];
        $ready      =   str_replace(' ','+',$input);
        $name       =   \GuzzleHttp\json_decode(file_get_contents('http://imdb.wemakesites.net/api/search?api_key=a9a4aa47-9dc3-428c-ac9e-546a4778aed9&q='.$ready),true);
        if(!isset($name['data']['results'][$where])) return [false];
        foreach ($name['data']['results'][$where] as $perName){
            if($perName['title']==$input){
                return  \GuzzleHttp\json_decode(file_get_contents('http://imdb.wemakesites.net/api/'.$perName['id'].'?api_key=a9a4aa47-9dc3-428c-ac9e-546a4778aed9'),true)['data']['filmography'];
            }
        }
        return [false];
    }

    public function searchIMDBwithID(Request $request){
        $input 	    =	$request['search'];
        $imdbID     =   str_replace(' ','+',$input);
        $result     =   \GuzzleHttp\json_decode(file_get_contents('http://imdb.wemakesites.net/api/'.$imdbID.'?api_key=a9a4aa47-9dc3-428c-ac9e-546a4778aed9'),true);
        return $result['data'];
    }

    public function trailerPush($name,$links){
        if(count($links)>0){
            echo '
    <div class="card-header">
	<h2>Trailer Seçiniz</h2>
	</div>
	<div class="col-sm-12">';
            for ($i=0; $i < 4; $i++)
            {
                echo '
    <div class="col-sm-3">
        <div class="card">
            <button class="btn bgm-red btn-block waves-effect" onclick="$(\'#trailer\').val(\'https://www.youtube.com/embed/'.$links[$i]['id'].'\')"><i class="md md-add"></i></button>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$links[$i]['id'].'" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="card-header ch-alt">'.$links[$i]['title'].'
            </div>
        </div>
    </div>';
            }
            echo '</div>';
        }
        else
        {
            $found = trans('controllerTranslations.trailer_not_found');
            echo '<button class="btn bgm-orange notfound"  onclick="getTrailer('.$name.')">'.$found.'</button>';
        }
    }

    public function videoSearch(Request $request){
        $videoImagePath     =   'backend/videoImages/';
        $videoName  =$request->input('name');
        $lastvideos = VideoProperty::select('videoID', 'name', 'type', 'turkishName', 'imdbPoint', 'abstract', 'picture')
            ->where('name', 'like', '%' . $videoName . '%')
            ->orWhere('turkishName', 'like', '%' . $videoName . '%')
            ->orWhere('turkishName', 'like', '%' . $videoName . '%')
            ->orWhere('abstract', 'like', '%' . $videoName . '%')
            ->orderBy('videoID', 'DESC')
            ->paginate(10);
        return view('backend.video.lastVideos',['title'=>trans('pageTranslations.serie_movie_name_search'),'lastVideos'=>$lastvideos,'videoImagePath'=>$videoImagePath]);
    }

    public function addVideo(Request $request){
        $peoples = $request['video']['People'];
        foreach($peoples as $people){
            $pValues =  array(
                'personName'        => $people['name'],
                'personPicture'     => $people['personPicture'],
                'content'           => $people['content']
            );
            $pValidator = Validator::make($pValues, People::$rules);
            if ($pValidator->fails()) {

                // hataları değişkene atadık
                $messages = $pValidator->errors()->all();

                // hataları döndük
                return $messages;
            }
        }

        $ifexits = VideoProperty::where(['type'=>$request['video']['Type'],'name'=>$request['video']['Name']])->first();
        if(!$ifexits){
            $values = array(
                'type'          => $request['video']['Type'],
                'name'          => $request['video']['Name'],
                'turkishName'   => $request['video']['TurkishName'],
                'kind'          => $request['video']['Kind'],
                'imdbPoint'     => $request['video']['ImdbPoint'],
                'duration'      => $request['video']['Duration'],
                'country'       => $request['video']['Country'],
                'dateVision'    => $request['video']['VisionDate'],
                'abstract'      => $request['video']['Abstract'],
                'trailer'       => $request['video']['Trailer'],
                'picture'       => $request['video']['Picture']
            );

            $validator = Validator::make($values, VideoProperty::$rules);

            if ($validator->fails()) {

                // hataları değişkene atadık
                $messages = $validator->errors()->all();

                // hataları döndük
                return $messages;
            } else {
                // doğrulama başarılıysa
                // user modeli yarat ve değerleri gir
                try{
                    $video              =   new VideoProperty();
                    $video->type        =   Functions::addSlashAndRemoveTags($values['type']);
                    $video->name        =   Functions::addSlashAndRemoveTags($values['name']);
                    $video->turkishName =   Functions::addSlashAndRemoveTags($values['turkishName']);
                    $video->kind        =   Functions::addSlashAndRemoveTags($values['kind']);
                    $video->imdbPoint   =   Functions::addSlashAndRemoveTags($values['imdbPoint']);
                    $video->duration    =   Functions::addSlashAndRemoveTags($values['duration']);
                    $video->country     =   Functions::addSlashAndRemoveTags($values['country']);
                    $video->dateVision  =   Functions::addSlashAndRemoveTags($values['dateVision']);
                    $video->abstract    =   Functions::addSlashAndRemoveTags($values['abstract']);
                    $video->trailer     =   Functions::addSlashAndRemoveTags($values['trailer']);
                    $video->picture     =   Functions::addSlashAndRemoveTags(Functions::downloadPicture($values['picture'], $values['name'], 'videoImages'));
                    $video->view        =   rand(9,20);

                    // veritabanı kaydı ilk veri eklemesi gerçekleşti burdaki video id aktivitede ve peoplevideoda kullanılacak
                    $video->save();

                    $users  =   User::where(['username' => Session::get('user')]);
                    $datas  =   $users->first();
                    if ($datas) {
                        $users->update(['ipAdress' => $request->ip()]);
                        $activity           =   new Activity([
                            'userID'        =>  $datas['userID'],
                            'committedID'   =>  $video->videoID,
                            'activityName'  =>  'add_video',
                            'activityDate'  =>  Carbon::now()]);
                        $activity->save();

                        foreach($peoples as $people){
                            $findPeople     =   People::where(['personName' => $people['name']]);
                            $datasPeople    =   $findPeople->first();
                            $peopleID       =   0;
                            if ($datasPeople) {
                                $peopleID   =   $datasPeople['personID'];
                            }else{
                                $peopleDB = new People([
                                    'personName'    => Functions::addSlashAndRemoveTags($people['name']),
                                    'personPicture' => Functions::addSlashAndRemoveTags(Functions::downloadPicture($people['personPicture'], $people['name'], 'peopleImages')),
                                    'content'       => Functions::addSlashAndRemoveTags($people['content'])
                                ]);
                                $peopleDB->save();
                                $peopleID   =   $peopleDB->personID;
                            }

                            $activity = new Activity([
                                'userID'        => $datas['userID'],
                                'committedID'   => $peopleID,
                                'activityName'  => 'add_people',
                                'activityDate'  => Carbon::now()]);
                            $activity->save();

                            $peopleVideo = new VideoPeople([
                                'videoID'   => $video->videoID,
                                'personID'  => $peopleID,
                                'job'       => Functions::addSlashAndRemoveTags($people['job']),
                                'rol'       => Functions::addSlashAndRemoveTags($people['rol'])
                            ]);

                            $peopleVideo->save();
                        }

                        return "true-".$video->videoID;
                    } else {
                        return trans('controllerTranslations.user_not_found');
                    }
                }
                catch(\Exception $e){
                    // do task when error
                    return $e->getMessage();   // mesajları at
                }
            }
        }else{
            return trans('controllerTranslations.video_already_added');
        }
    }


    public function videoFind()
    {
        if(session('user')){
            return view('backend.video.addVideo');
        }else{
            return redirect('/user/login');
        }
    }

    public function videoEdit($videoName,$videoID){
        $videoImagePath     =   'backend/videoImages/';
        if($videoName!=null && $videoID!=null) {
            $users  =   User::where(['username' => Session::get('user')])->first();
            if ($users) {
                $videos =   VideoProperty::findOrFail($videoID);
                $title  =   $videos->name;
                return view('backend.video.editVideo',compact('videos','title','videoImagePath'));
            }else{
                $title      =   trans('pageTranslations.user_login');
                $page       =   'login';
                $response   =   trans('controllerTranslations.please_login');
                return view('backend.user.login',['page'=>$page,'title'=>$title,'response'=>$response]);
            }
        }else{
            return redirect('/');
        }
    }

    public function showVideo($videoName,$videoID=null){
        $videoImagePath     =   'backend/videoImages/';
        $profilImagePath    =   'backend/img/profile-pics/';
        if($videoName=='last' && $videoID==null){
            $lastvideos = VideoProperty::select('videoID', 'name', 'type', 'turkishName', 'imdbPoint', 'abstract', 'picture')
                        ->orderBy('videoID', 'DESC')
                        ->paginate(10);
            return view('backend.video.lastVideos',['title'=>trans('pageTranslations.last_added_series_movies'),'lastVideos'=>$lastvideos,'videoImagePath'=>$videoImagePath]);
        }elseif($videoName=='last' && $videoID!=null) {
            return redirect('/');
        }else{
            $validator = Validator::make(
                ['videoId'  =>  $videoID],
                ['videoId'  =>  'required|numeric|digits_between:1,10']);
            if($validator->fails()){
                return redirect('/');
            }else{
                $video = VideoProperty::find($videoID);
                if ($video) {
                    $people = DB::table('people')
                        ->join('videopeople', 'videopeople.personID', '=', 'people.personID')
                        ->select('people.personID', 'people.personName',  'people.personPicture', 'videopeople.rol', 'videopeople.job')
                        ->where('videopeople.videoID','=',$videoID)
                        ->orderBy('videopeople.job', 'DESC')
                        ->paginate(12, ['*'], 'page_cast');;
                    $subtitles = DB::table('videoproperties')
                        ->join('subtitleready', 'subtitleready.videoID', '=', 'videoproperties.videoID')
                        ->join('users', 'users.userID', '=', 'subtitleready.uploaderID')
                        ->select('users.username', 'subtitleready.subtitleReadyID','subtitleready.created_at','subtitleready.downloads',
                            'subtitleready.season', 'subtitleready.part', 'subtitleready.srtPath',
                            'subtitleready.language')
                        ->where('videoproperties.videoID','=',$videoID)
                        ->orderBy('subtitleready.season', 'DESC')
                        ->orderBy('subtitleready.part', 'DESC')
                        ->paginate(10, ['*'], 'page_subtitle');
                    $comments = DB::table('videocomments')
                        ->join('users', 'users.userID', '=', 'videocomments.commenterID')
                        ->select('users.username', 'users.profilePicture', 'videocomments.comment',
                            'videocomments.like', 'videocomments.unlike','videocomments.created_at')
                        ->where('videocomments.videoID','=',$videoID)
                        ->orderBy('videocomments.commentID', 'DESC')
                        ->paginate(10, ['*'], 'page_comment');
                    $lastVideos = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->orderBy(DB::raw('RAND()'))
                        ->where('view','<',50)
                        ->limit(5)
                        ->get();
                    $popularSeries = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->where('type','serie')
                        ->orderBy('view', 'DESC')
                        ->limit(5)
                        ->get();
                    $popularMovies = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->where('type','movie')
                        ->orderBy('view', 'DESC')
                        ->limit(5)
                        ->get();
                    $popularAnimes = VideoProperty::select('videoID', 'name', 'view', 'picture')
                        ->where('type','anime')
                        ->orderBy('view', 'DESC')
                        ->limit(5)
                        ->get();
                    if(!Cookie::has('video-'.$videoID)){
                        $view = view('backend.video.video',[
                            'videoProperties'   =>  $video,
                            'title'             =>  $video['name'],
                            'people'            =>  $people,
                            'subtitles'         =>  $subtitles,
                            'videoImagePath'    =>  $this->videoImagePath,
                            'comments'          =>  $comments,
                            'profilImagePath'   =>  $this->profilImagePath,
                            'lastVideos'        =>  $lastVideos,
                            'popularSeries'     =>  $popularSeries,
                            'popularMovies'     =>  $popularMovies,
                            'popularAnimes'     =>  $popularAnimes]);
                        $cookie = Cookie::make('video-'.$videoID, '1', 60*24);
                        $video->view++;
                        $video->save();
                        return response()->make($view)->withCookie($cookie);
                    }else{
                        return view('backend.video.video',[
                            'videoProperties'   =>  $video,
                            'title'             =>  $video['name'],
                            'people'            =>  $people,
                            'subtitles'         =>  $subtitles,
                            'videoImagePath'    =>  $this->videoImagePath,
                            'comments'          =>  $comments,
                            'profilImagePath'   =>  $this->profilImagePath,
                            'lastVideos'        =>  $lastVideos,
                            'popularSeries'     =>  $popularSeries,
                            'popularMovies'     =>  $popularMovies,
                            'popularAnimes'     =>  $popularAnimes]);
                    }
                }else{
                    return redirect('/');
                }
            }
        }
    }


    public function update($videoID,Request $request){
        $users  =   User::where(['username' => Session::get('user')]);
        $datas  =   $users->first();
        if ($datas) {
            $ifexits = VideoProperty::where(['type'=>Input::get('type'),'name'=>Input::get('name')])->whereNotIn('videoID', [$videoID])->first();
            if(!$ifexits){
                $validator = Validator::make(Input::all(),
                    array(
                        'type'          => 'required',
                        'name'          => 'required|max:255',
                        'turkishName'   => 'required|max:255',
                        'kind'          => 'required|max:255',
                        'imdbPoint'     => 'required|max:10',
                        'duration'      => 'required|numeric|digits_between:1,10',
                        'country'       => 'required|max:255',
                        'dateVision'    => array('required', 'date_format:"Y"',"digits:4"),
                        'abstract'      => 'required',
                        'trailer'       => 'required|url',
                        'picture'       => 'required'
                    ));
                if($validator->fails())return redirect()->back()->withErrors($validator->errors()->all());
                $videos = VideoProperty::findOrFail($videoID);
                if($videos){
                    if(Input::get('picture')!=$videos->picture){
                        $validatorUrl =Validator::make(Input::only(['picture']),
                            [   'picture'     => 'required|url'
                            ]);
                        if($validatorUrl->fails()) return redirect()->back()->withErrors($validatorUrl->errors()->all());
                        $newPictureLink=Functions::downloadPicture(Input::get('picture'),Input::get('name'),'videoImages');
                        Input::merge(array('picture' => $newPictureLink));
                    }
                    $videos->fill(Input::all());
                    if(!$videos->isDirty()) return redirect('/video/' . Functions::beGoodSeo($videos->name) . '/' . $videos->videoID);
                    $videos->save();
                    $users->update(['ipAdress' => $request->ip()]);
                    $activity = new Activity([
                        'userID'        => $datas['userID'],
                        'committedID'   => $videos->videoID,
                        'activityName'  => 'update_video',
                        'activityDate'  => Carbon::now()]);
                    $activity->save();

                    return redirect('/video/' . Functions::beGoodSeo($videos->name) . '/' . $videos->videoID);
                }else{
                    return redirect('/');
                }
            }else{
                return redirect()->back()->withErrors([trans('controllerTranslations.video_already_added')]);
            }
        }else{
            $title      =   trans('pageTranslations.user_login');
            $page       =   'login';
            $response   =   trans('controllerTranslations.please_login');
            return view('backend.user.login',['page'=>$page,'title'=>$title,'response'=>$response]);
        }
    }

    public function commentAdd($videoID,Request $request){
        $validator = Validator::make(Input::all(),['comment'=>'required']);
        $user = User::where(['username'=>Session::get('user')])->first();
        if(!$user){
            $title      =   trans('pageTranslations.user_login');
            $page       =   'login';
            $response   =   trans('controllerTranslations.please_login');
            return view('backend.user.login',['page'=>$page,'title'=>$title,'response'=>$response]);
        }
        if($validator->fails()){
            return redirect()->back()
                ->withErrors([
                    'result'    =>  'danger',
                    'for'       =>  'video',
                    'message'   =>  $validator->errors()->all()]);
        }
        else{
            $newComment = new VideoComment();
            $newComment->commenterID=$user->userID;
            $newComment->videoID=$videoID;
            $newComment->comment=Functions::addSlashAndRemoveTags($request['comment']);
            $newComment->save();
            return redirect()->back();
        }
    }

    public function videoFollow($videoName,$videoID){
        $validator = Validator::make(
            ['videoId'  =>  $videoID],
            ['videoId'  =>  'required|numeric|digits_between:1,10']);
        if($validator->fails()) {
            return redirect('/');
        }else{
            if(session('user')){
            $user = User::where(['username'=>Session::get('user')])->first();
            $video = VideoProperty::find($videoID);
            if(!$user || !$video) return redirect('/');
            $videoFollowExists = DB::table('videofollowers')->where(['followerID'=>$user->userID,'videoID'=>$videoID])->get();
            if(!$videoFollowExists){
                $videoFollower = new VideoFollower();
                $videoFollower->followerID=$user->userID;
                $videoFollower->videoID=$videoID;
                if($videoFollower->save()){
                    $activity = new Activity([
                        'userID'        => $user->userID,
                        'committedID'   => $videoID,
                        'activityName'  => 'video_follow',
                        'activityDate'  => Carbon::now()]);
                    if($activity->save()){
                        return redirect()->back()
                            ->withErrors(['result'=>'success','for'=>'video','message'=>trans('controllerTranslations.successfully_added_followed')]);
                    }else{
                        return redirect('/');
                    }
                }
            }else{
                return redirect()->back()
                    ->withErrors(['result'=>'danger','for'=>'video','message'=>trans('controllerTranslations.you_are_already_follower')]);
            }
            }else{
                return redirect('/user/login');
            }
        }
    }
}