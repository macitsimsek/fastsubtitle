<?php

namespace App\Http\Controllers;


use App\User;
use App\VideoProperty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;



class PagesController extends Controller
{
    public function index()
    {
        $videoImagePath     =   'backend/videoImages/';
        $peopleImagePath    =   'backend/peopleImages/';
        $profilImagePath    =   'backend/img/profile-pics/';
        $lastvideos = VideoProperty::select('videoID', 'name', 'type', 'turkishName', 'imdbPoint', 'abstract', 'picture')
            ->orderBy('videoID', 'DESC')
            ->paginate(15);
        $popularSeries = VideoProperty::select('videoID', 'name', 'view', 'picture')
            ->where('type','serie')
            ->orderBy('view', 'DESC')
            ->limit(5)
            ->get();
        $popularMovies = VideoProperty::select('videoID', 'name', 'view', 'picture')
            ->where('type','movie')
            ->orderBy('view', 'DESC')
            ->limit(5)
            ->get();
        $popularAnimes = VideoProperty::select('videoID', 'name', 'view', 'picture')
            ->where('type','anime')
            ->orderBy('view', 'DESC')
            ->limit(5)
            ->get();
        $newUsers = User::select('userID', 'username', 'created_at', 'profilePicture')
            ->orderBy('userID', 'DESC')
            ->limit(5)
            ->get();
        $lastComments = DB::table('videocomments')
            ->join('users', 'users.userID', '=', 'videocomments.commenterID')
            ->join('videoproperties', 'videoproperties.videoID', '=', 'videocomments.videoID')
            ->select('videocomments.comment', 'users.username', 'videocomments.created_at',
                'videoproperties.name','videoproperties.videoID')
            ->orderBy('videocomments.commentID', 'DESC')
            ->limit(5)
            ->get();
        $lastpeople = DB::table('people')->orderBy('personID', 'DESC')->paginate(20);
        $lastSeries = DB::table('subtitleready')
            ->join('users', 'users.userID', '=', 'subtitleready.uploaderID')
            ->join('videoproperties', 'videoproperties.videoID', '=', 'subtitleready.videoID')
            ->where(['videoproperties.type'=>'serie'])
            ->select('videoproperties.videoID', 'users.username', 'videoproperties.name',
                'videoproperties.type', 'videoproperties.picture',
                'subtitleready.season', 'subtitleready.part', 'subtitleready.translatingSrtPath',
                'subtitleready.fromLanguage', 'subtitleready.toLanguage', 'subtitleready.created_at',
                'subtitleready.subtitleReadyID')
            ->orderBy('subtitleready.subtitleReadyID', 'DESC')
            ->paginate(5);
        $lastMovies = DB::table('subtitleready')
            ->join('users', 'users.userID', '=', 'subtitleready.uploaderID')
            ->join('videoproperties', 'videoproperties.videoID', '=', 'subtitleready.videoID')
            ->where(['videoproperties.type'=>'movie'])
            ->select('videoproperties.videoID', 'users.username', 'videoproperties.name',
                'videoproperties.type', 'videoproperties.picture',
                'subtitleready.season', 'subtitleready.part',
                 'subtitleready.language', 'subtitleready.created_at',
                'subtitleready.subtitleReadyID')
            ->orderBy('subtitleready.subtitleReadyID', 'DESC')
            ->paginate(5);
        /*$startedSubtitles = DB::table('subtitle')
            ->join('users', 'users.userID', '=', 'subtitle.uploaderID')
            ->join('videoproperties', 'videoproperties.videoID', '=', 'subtitle.videoID')
            ->where(['subtitle.isStarted'=>1])
            ->select('videoproperties.videoID', 'users.username', 'videoproperties.name',
                'videoproperties.type', 'videoproperties.picture',
                'subtitleready.season', 'subtitleready.part', 'subtitle.translatingSrtPath','subtitleready.isStarted',
                'subtitleready.fromLanguage', 'subtitleready.language', 'subtitleready.created_at',
                'subtitleready.subtitleID')
            ->orderBy('subtitleready.subtitleReadyID', 'DESC')
            ->paginate(5);*/
        return view('backend.index', [
            'lastvideos'        =>  $lastvideos,
            'lastpeople'        =>  $lastpeople,
            'lastSeries'        =>  $lastSeries,
            'lastMovies'        =>  $lastMovies,
            'lastComments'      =>  $lastComments,
            //'startedSubtitles'  =>  $startedSubtitles,
            'videoImagePath'    =>  $videoImagePath,
            'peopleImagePath'   =>  $peopleImagePath,
            'profilImagePath'   =>  $profilImagePath,
            'popularSeries'     =>  $popularSeries,
            'popularMovies'     =>  $popularMovies,
            'popularAnimes'     =>  $popularAnimes,
            'newUsers'          =>  $newUsers]);
    }


    public function users()
    {
        $users = DB::table('users')->get();

        return view('backend.user.users', ['users' => $users]);
    }

    public function mail(){
        $values = array(
            'username'         => "macit",
            'password'         => "asdasd",
            'password_confirm' => "asdasd",
            'email'            => "macitsimsek12@hotmail.com"
        );

        $validation=$values['username'].'_'.Carbon::now()->addDay();
        $validation=Crypt::encrypt($validation);
        return view('backend.user.mail')->with([
            'username'=>$values['username'],
            'email'=>$values['email'],
            'subject'=>'Hoş Geldiniz!',
            'comment'=>'Yukarıdaki Doğrulama Linkine Tıklayarak Giriş Yapabilirsiniz. <br />İyi Günler Dileriz.',
            'validation'=>$validation]);
        Mail::send('backend.mail',[
            'username'=>$values['username'],
            'email'=>$values['email'],
            'subject'=>'Hoş Geldiniz!',
            'comment'=>'Yukarıdaki Doğrulama Linkine Tıklayarak Giriş Yapabilirsiniz. <br />İyi Günler Dileriz.',
            'validation'=>$validation],function($message) use ($values){
            $message->to($values['email'],$values['username'])->subject('Üyelik Bilgileri');
        });
    }

    public function setLanguage(Request $request,$language)
    {
        $validator = Validator::make(['language' => $language], ['language' => 'in:tr,en,es,de,it,fr']);
        if (!$validator->fails()) $request->session()->put('locale', $language);
        return redirect()->back();
    }
}
