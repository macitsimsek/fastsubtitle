<?php

namespace App\Http\Controllers;


use App\Friend;
use App\Message;
use App\People;
use App\Subtitle;
use App\SubtitleFix;
use App\SubtitleRows;
use App\SubtitleSupport;
use App\VideoComment;
use App\VideoFollower;
use App\VideoProperty;
use Illuminate\Http\Request;
use App\Activity;
use App\MessageTranslations;
use App\User;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Stichoza\GoogleTranslate\TranslateClient;
use Yandex\Translate\Translator;
use Yandex\Translate\Exception;
use Alaouy\Youtube\Facades\Youtube;


class DenemeController extends Controller
{

public function deneme(Request $request){
    //$request->session()->put('key', 'macit',60);
    //$value = session()->forget('user');
    /*$people = DB::table('people')
        ->join('videopeople', 'videopeople.personID', '=', 'people.personID')
        ->join('videoproperties', 'videoproperties.videoID', '=', 'videopeople.videoID')
        ->select('videopeople.*', 'videoproperties.*')
        ->where('videoproperties.videoID','=',5)
        ->get();
    $people1 = DB::table('people')
        ->join('videopeople',       'videopeople.personID',     '=', 'people.personID')
        ->join('videoproperties',   'videopeople.videoID',      '=', 'videoproperties.videoID')
        ->select('videoproperties.videoID', 'videoproperties.name', 'videoproperties.turkishName', 'videoproperties.picture')
        ->where('people.personID','=',26)
        ->get();
    $people = DB::table('people')
        ->where('people.personID','=',26)
        ->get();
    $img = Image::make('http://placehold.it/500x500/000/e8117f')->resize(175, 250);
    $img->save('backend/deneme.jpg');

    return $img->response('jpg');;
    $image = Image::make('backend/peopleImages/Aaron-Paul.jpg')->resize(320, 240)->insert('public/watermark.png');
    //$person = People::where(['personID'=>32])->firstOrFail();
    return $img;
    $users = DB::table('people')->paginate(5);

    return $users;
    $cookie = Cookie::make('deneme', 'asdasd', 60);
    return dd(Cookie::get('name'));

    return response()->withCookie(cookie('name', 'my value', 60));
    Cookie::make('deneme', 'asdasd', 60);
    return $request->cookie('deneme');
    $response = new Response();
    $response->withCookie(cookie('name', 'my value', 60));
    $response = new Response();
    $asd="";
    if(!$request->hasCookie('deneme3')){
        return $response->withCookie(cookie('deneme3', 'my value', 60));
        $asd="yazdım";
    }
    else{
        $asd = $request->cookie('deneme3');
    }

    return $asd;

    $support = new SubtitleSupport();
    $support->fromLanguage="turkish";
    $support->toLanguage="english";
    $support->sentence="deneme deneme deneme";
    $support->mean="deneme deneme deneme";
    $support->createTime=Carbon::now();
    $support->save();
    return 'başarılı'.$support->subtitleSupportID;

    $rowCount=SubtitleRows::where(['subtitleID'=>'410'])->get()->count();
    $translatedRows=SubtitleFix::where(['subtitleID'=>'410'])
        ->select('rowNumber')
        ->distinct()
        ->get()
        ->count();
    $progress=floor(($translatedRows*100)/$rowCount);

    return $progress;
    $deneme = SubtitleFix::all();
    foreach($deneme as $de){
        $bul = SubtitleFix::where(['subtitleID'=>$de->subtitleID,'rowNumber'=>$de->rowNumber])->get()->count();
        if($bul==1){
            $de->bestTranslate=1;
            $de->save();
        }
    }
    return $deneme;

    $deneme=SubtitleFix::leftJoin('subtitlerows','subtitlerows.rowNumber','=','subtitlefixes.rowNumber')
        ->select('subtitlerows.*','subtitlefixes.*')
        ->orderBy('subtitlerows.rowNumber','desc')
        ->limit(10)->get();
    return $deneme;
    $date =Carbon::parse(Carbon::now())->format('d/m/Y');
    $deneme = SubtitleSupport::whereDate('createTime', '=', '2016-03-26')->get();
    foreach($deneme as $denek){
        $denek->delete();
    }
    return $deneme;
    $deneme = Subtitle::all();
    foreach ($deneme as $item) {
        $item->translatingSrtPath=str_replace('backend/translatingSrt/','',$item->translatingSrtPath);
        $item->save();
    }
    $deneme = Subtitle::all();
    return $deneme;
    $subusers= SubtitleFix::where(['subtitleID'=>'410'])
        ->join('users','users.userID','=','subtitlefixes.adderID')
        ->groupBy('users.username','subtitlefixes.rowNumber')
        ->select('users.username')
        ->get();
    return $subusers;
    $selectfixes=DB::table('subtitle')
        ->join('subtitlerows', function($join)
        {
            $join->on('subtitlerows.subtitleID', '=', 'subtitle.subtitleID');
        })
        ->join('subtitlefixes', function($join)
        {
            $join->on('subtitlefixes.subtitleID', '=', 'subtitle.subtitleID');
        })
        ->where('subtitle.subtitleID', '=', 410)
        ->get();
    $selectfixes=DB::select(DB::raw('select subtitlefixes.rowNumber,subtitlefixes.newMean,subtitlerows.duration
      from subtitlefixes,subtitlerows where subtitlefixes.subtitleID=410 and subtitlerows.rowNumber=subtitlefixes.rowNumber and subtitlerows.canUpdate=1 order by subtitlefixes.rowNumber'));
    $selectfixes=DB::table('subtitlerows')
        ->join('subtitlefixes','subtitlefixes.subtitleID','=','subtitlerows.subtitleID')
        ->where(['subtitlerows.subtitleID'=>410])
        ->where(['subtitlefixes.subtitleID'=>410])
        ->get();
    $selectFixes=DB::table('subtitlerows')
        ->join('subtitlefixes','subtitlefixes.rowNumber','=','subtitlerows.rowNumber')
        ->select('subtitlefixes.rowNumber','subtitlerows.duration','subtitlefixes.newMean')
        ->where(['subtitlerows.subtitleID'=>410])
        ->where(['subtitlefixes.subtitleID'=>410])
        ->where(['subtitlefixes.bestTranslate'=>1])
        ->orderBy('subtitlerows.rowNumber')
        ->get();
    return $selectFixes;

    $denek = SubtitleSupport::all(['mean','sentence']);
    return $denek;

    $deneme = People::all();
    foreach ($deneme as $item) {
        $item->personPicture=str_replace('backend/peopleImages/','',$item->personPicture);
        $item->save();
    }
    return "";

    $deneme = SubtitleSupport::select(['sentence','mean'])->orderBy('sentence')->orderBy('mean')->get();
    return $deneme;
    $tr = new TranslateClient('en', 'tr');
    return $tr->getResponse("try");
    $comments = DB::table('videocomments')
        ->join('users', 'users.userID', '=', 'videocomments.commenterID')
        ->select('users.username', 'videocomments.comment',
            'videocomments.like', 'videocomments.unlike','videocomments.created_at')
        ->where('videocomments.videoID','=',12)
        ->paginate(10);
    return  $comments;
    $lastComments = DB::table('videocomments')
        ->join('users', 'users.userID', '=', 'videocomments.commenterID')
        ->join('videoproperties', 'videoproperties.videoID', '=', 'videocomments.videoID')
        ->select('videocomments.comment', 'users.username', 'videocomments.created_at', 'users.profilePicture',
            'videoproperties.name','videoproperties.videoID')
        ->orderBy('videocomments.commentID', 'DESC')
        ->limit(5)
        ->get();
    return $lastComments;
    $userWorkedToday = SubtitleFix::where([
        'subtitleID'=>402,
        'adderID'=>8])
        ->where('createTime', '>=', date('Y-m-d').' 00:00:00')
        ->count();
    return $userWorkedToday>0?"yazmış":"yazmamış";
    $isAddedBefore = SubtitleFix::where([//bugün ekleme yapmadıysa aktiviteye ekle
        'subtitleID'    =>  401,
        'rowNumber'     =>  223,
        'adderID'       =>  8])
        ->count();
    return $isAddedBefore;
    return redirect('/requests')
        ->withErrors(['result' => 'danger', 'message' => trans('controllerTranslations.please_login')]);
return storage_path();
    $userWorkedToday = SubtitleFix::where([//bugün ekleme yapmadıysa aktiviteye ekle
        'subtitleID'    =>  2,
        'adderID'       =>  1])
        ->where('createTime', '>=', date('Y-m-d').' 00:00:00')
        ->get()
        ->count();
    return $userWorkedToday==0?"asd":"2";
    $subtitle = DB::table('subtitle')
        ->join('videoproperties','videoproperties.videoID','=','subtitle.videoID')
        ->where(['subtitle.subtitleID'=>2])
        ->select('videoproperties.name','videoproperties.videoID','subtitle.toLanguage','subtitle.translatingSrtPath')
        ->get();
    return $subtitle;
    return $request->server("HTTP_HOST");
    $followedVideos = VideoFollower::where(['followerID'=>1])
        ->join('videoproperties','videoproperties.videoID','=','videofollowers.videoID')
        ->get();
    return $followedVideos;
    $sups = SubtitleSupport::where(['fromLanguage'=>'english','toLanguage'=>'turkish'])
        ->orWhere(['fromLanguage'=>'turkish','toLanguage'=>'english'])
        //->select(stripslashes(DB::raw('sentence')),stripslashes(DB::raw('mean')))
        ->select('sentence','mean')
        ->orderBy('sentence')
        ->orderBy('mean')
        ->distinct()
        ->get();
    return $sups;
    $message = new Message();
    $message->senderID=1;
    $message->recieverID=2;
    $message->message="Deneme mesaj";
    $message->messageDate=Carbon::now();
    $message->save();
    $str = "1-5";
    $crypt = Crypt::encrypt($str);
    return redirect(url("/send-friend-request/".$crypt));
    $videoName  =   "game of thrones";
    $input		=	$videoName." trailer";
    $input		=	str_replace(" ", "+", $input);
    $page		=	file_get_contents('https://www.youtube.com/results?search_query='.$input);

    $parcala	=	('@<a href="(.*?)" class="(.*?)" data-sessionlink="(.*?)" aria-describedby="(.*?)" rel="(.*?)" dir="(.*?)">(.*?)</a>@');
    preg_match_all($parcala,$page,$cikti);
    $linkler	=	$cikti[1];
    $name		=	$cikti[4];
    return "<pre>".print_r($cikti)."</pre>";
    $tr = new TranslateClient('en', 'de');
    foreach($language as $perword=>$key){

        echo  '\''.$perword.'\''.'=>\''.$tr->translate($key).'\',<br>';
    }
    try {
        $key ="trnsl.1.1.20161020T074041Z.d0fc72a618c51a0a.ee86ae777fb38396d3d84dfd9ad3caad13441f2c";
        $values = [
            'turkish'   =>  'tr',
            'english'   =>  'en',
            'german'    =>  'de',
            'french'    =>  'fr',
            'italian'   =>  'it',
            'spanish'   =>  'es'];
        $to     = $values["turkish"];
        $from   = $values["english"];
        $sentence = "I am running";
        $translator = new Translator($key);
        $tr = new TranslateClient($from,$to);
        $yandexTranslation = $translator->translate($sentence, $from.'-'.$to);
        $googleTranslation = $tr->translate($sentence);
        return 'Google Translate = '.$googleTranslation.'<br>Yandex Translate = '.$yandexTranslation;
    } catch (Exception $e) {
        // handle exception
    }
    $values = array(
        'username'         => "salvadorx",
        'password'         => "asdasd",
        'password_confirm' => "asdasd",
        'email'            => trim("macitsimsek12@hotmail.com")
    );
    Mail::send('backend.user.mail', [
        'username'  =>  $values['username'],
        'email'     =>  $values['email'],
        'subject'   =>  trans('pageTranslations.welcome',['username'=>'sads']),
        'comment'   =>  trans('controllerTranslations.click_link_and_login').' <br />'.trans('controllerTranslations.have_a_nice_day'),
        'validation'=>""], function($message) use ($values) {
        $message->to("macitsimsek12@hotmail.com","asdas")->from("macitsimsek12@hotmail.com","Macit Simsek")->subject(trans('controllerTranslations.user_information'));
    });

    $results = Youtube::searchVideos('Android',4);
    foreach ($results as $index=>$video){
        $links[$index]['id']=$video->id->videoId;
        $links[$index]['title'] = Youtube::getVideoInfo([$video->id->videoId])[0]->snippet->title;
    }
    return $links;https://tr.wikipedia.org/w/api.php?action=query&list=search&srsearch=The%20Big%20Bang%20Theory&utf8=&formatversion=2

    //$name =\GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&formatversion=2&titles=Tesla%20Motors'),true)['query']['pages'][0]['extract'];
    $input  =   "The+Big+Bang+Theory";
    $name =\GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?action=query&list=search&utf8=&formatversion=2&format=json&srsearch='.$input),true)['query']['search'][0]['title'];
    $name   = str_replace(' ','+',$name);
    return \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&formatversion=2&titles='.$name),true)['query']['pages'][0]['extract'];
    *//*
    $input 	    =	"Oliver Phelps";
    $input      =   str_replace(' ','+',$input);
    $name       =   \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?action=query&list=search&utf8=&formatversion=2&format=json&srsearch='.$input),true)['query']['search'][0]['title'];
    $name       =   str_replace(' ','+',$name);
    $abstract   =   \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&formatversion=2&titles='.$name),true);
    $picture    =   \GuzzleHttp\json_decode(file_get_contents('https://tr.wikipedia.org/w/api.php?format=json&action=query&prop=pageimages&pithumbsize=250&formatversion=2&titles='.$name),true);
    return ['abstract'=>isset($abstract['query']['pages'][0]['extract'])?$abstract['query']['pages'][0]['extract']:'','picture'=>isset($picture['query']['pages'][0]['thumbnail']['source'])?$picture['query']['pages'][0]['thumbnail']['source']:'backend/img/empty.jpg'];*/
    /*$videos = VideoProperty::all();
    foreach ($videos as $video){
        if(strpos($video->name,'(')){
            $video->name=trim(substr($video->name,0,strpos($video->name,'(')));
            $video->save();
        }
    }*/
}

public function trys(Request $request){
    $validator = Validator::make([
        'email'          => 'macitsimsek12@hotmail.com'], [
        'email'          => 'required|email|exists:users'], MessageTranslations::$messageTr);
    return $validator->errors()->all();
}
    public function getTrailer($video){
        $videoName  =  $video;
        $input		=	$videoName." trailer";
        $input		=	str_replace(" ", "+", $input);
        $page		=	file_get_contents('https://www.youtube.com/results?search_query='.$input);

        $parcala	=	('@<a href="(.*?)" class="(.*?)" data-sessionlink="(.*?)" aria-describedby="(.*?)" rel="(.*?)" dir="(.*?)">(.*?)</a>@');
        preg_match_all($parcala,$page,$cikti);
        $linkler	=	$cikti[1];
        return self::trailerPush($videoName,$linkler);
    }

    public function connectedBring($link) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_REFERER,"http://www.google.com.tr");
        $Curl = curl_exec($ch);
        curl_close($ch);
        return $Curl;
    }
    public function trailerPush($name,$links)
    {
        if(count($links)>0){
            echo '
    <div class="card-header">
	<h2>Trailer Seçiniz</h2>
	</div>
	<div class="col-sm-12">';
            for ($i=0; $i < 4; $i++)
            {
                echo '
    <div class="col-sm-3">
        <div class="card">
            <button class="btn bgm-red btn-block waves-effect" onclick="$(\'#trailer\').val(\'https://www.youtube.com/embed/'.str_ireplace("/watch?v=","",$links[$i]).'\')"><i class="md md-add"></i></button>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.str_ireplace("/watch?v=","",$links[$i]).'" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>';
            }
            echo '</div>';
        }
        else
        {
            $found = trans('controllerTranslations.trailer_not_found');
            echo '<button class="btn bgm-orange notfound"  onclick="getTrailer('.$name.')">'.$found.'</button>';
        }
    }
    public function deneme2() {
        $videoName  =  "john wick";
        $youtube = new Youtube("AIzaSyDXI_ld62z_vehR1qE8SYV56RWs9q9RIQk");
        return $videoList = $youtube->searchVideos('Android');
    }

    public function translate(Request $request){
        $tr = new TranslateClient('en', 'tr');
        return $tr->translate($request['text']);
    }
}