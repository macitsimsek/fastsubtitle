<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Functions;
use App\Subtitle;
use App\SubtitleFix;
use App\SubtitleReady;
use App\SubtitleRequester;
use App\SubtitleRows;
use App\SubtitleSupport;
use App\User;
use App\VideoFollower;
use App\VideoProperty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Stichoza\GoogleTranslate\TranslateClient;
use Yandex\Translate\Translator;
use Yandex\Translate\Exception;

class SubtitleController extends Controller
{
    public function showSubtitles($order=null){
        $videoImagePath     =   'backend/videoImages/';
        $query  =   'subtitleReadyID';
        $title  =   trans('pageTranslations.last_added_subtitles');
        /*if($order=='finishing') {$query='progress'; $title=trans('pageTranslations.finishing_closer_subtitles');}
        elseif($order=='mostrequested') {$query='requestCount'; $title=trans('pageTranslations.most_requested_subtitles');}*/
        $lastSubtitles = DB::table('subtitleready')
            ->join('users', 'users.userID', '=', 'subtitleready.uploaderID')
            ->join('videoproperties', 'videoproperties.videoID', '=', 'subtitleready.videoID')
            ->select('videoproperties.videoID', 'users.username', 'videoproperties.name',
                'videoproperties.type', 'videoproperties.picture',
                'subtitleready.season', 'subtitleready.part','subtitleready.srtPath',
                'subtitleready.language',
                'subtitleready.subtitleReadyID')
            ->orderBy('subtitleready.'.$query, 'DESC')
            ->paginate(25);
        //return $lastSubtitles;
        return view('backend.subtitle.lastSubtitle',[
            'lastSubtitles'     =>  $lastSubtitles,
            'title'             =>  $title,
            'videoImagePath'    =>  $videoImagePath]);
    }

    public function checkSubtitle($subtitleID){
        if(Session::has('user')){
            $validator  =   Validator::make(
                ['subtitleId' => $subtitleID],
                ['subtitleId' => 'required|numeric|digits_between:1,10']);
            if ($validator->fails()) {
                return redirect()->back();
            } else {
                $subtitle = Subtitle::find($subtitleID);
                $user = User::where(['username'=>Session::get('user')])->first();
                if (!$user || !$subtitle) return redirect()->back()
                    ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.please_login')]);

                $check      =   SubtitleRows::where(['subtitleID'=>$subtitleID])->first();
                if(!$check){
                    if($user->memberType=="normalMember" || $user->memberType=="translatorMember") return redirect()->back()
                        ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.you_are_not_a_controller_member')]);
                    return view('backend.subtitle.subtitleCheck')->with(['title' => trans('pageTranslations.subtitle_check'),'subtitle'=>$subtitle]);
                }else{
                    if($user->memberType=="normalMember" || $user->memberType=="controllerMember") return redirect()->back()
                        ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.you_are_not_a_translator_member')]);
                    return redirect('/subtitle/processing/'.$subtitleID)
                        ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.translate_processing_already_started')]);
                }

            }
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function request($videoName,$videoID){
        if(Session::has('user')){
            $validator = Validator::make(
                ['videoId' => $videoID],
                ['videoId' => 'required|numeric|digits_between:1,10']);
            if ($validator->fails()) {
                return redirect()->back();
            } else {
                $video = VideoProperty::find($videoID);
                if (!$video) return redirect()->back();
                return view('backend.subtitle.requestSubtitle')->with(['title' => $video->name,'type'=>$video->type,'videoID'=>$videoID]);
            }
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function requestPlus($subtitleID){
        if(Session::has('user')){
            $validator = Validator::make(
                ['subtitleId' => $subtitleID],
                ['subtitleId' => 'required|numeric|digits_between:1,10']);
            if ($validator->fails()) {
                return redirect('/');
            } else {
                $subtitle = Subtitle::find($subtitleID);
                $user = User::where(['username'=>Session::get('user')])->first();
                if (!$user || !$subtitle) return redirect()->back()
                    ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.please_login')]);

                $requesters = SubtitleRequester::where(['subtitleID'=>$subtitleID,'userID'=>$user->userID])->first();
                if($requesters) return redirect()->back()
                    ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.already_requested')]);

                $requester = new SubtitleRequester();
                $subtitle->requestCount++;
                $subtitle->save();
                $requester->fill(['subtitleID'=>$subtitleID,'userID'=>$user->userID]);
                $requester->save();
                $activity = new Activity([
                    'userID'        => $user->userID,
                    'committedID'   => $requester->requestID,
                    'activityName'  => 'participated_to_subtitle_requesters',
                    'activityDate'  => Carbon::now()]);
                $activity->save();
                return redirect()->back()->withErrors(['result'=>'success','message'=>trans('controllerTranslations.successfully_added_to_waiting')]);
            }
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function create($videoID){
        if(Session::has('user')){
            $user = User::where(['username'=>Session::get('user')])->first();
            $video = VideoProperty::find($videoID);
            if (!$video && !$user) return redirect('/');
            $yol = 'srts/';
            $filePath='backend/'.$yol;
            $name=Input::file('srt')->getClientOriginalName();
            $aValidate = Validator::make(
                [   'translatingSrtPath'    => $filePath.$name,
                    'fromSrtEx'             => Input::file('srt')->getClientOriginalExtension()],
                [   'translatingSrtPath'    => 'required|unique:subtitle',
                    'fromSrtEx'             => 'in:srt']);
            if($aValidate->fails()) return redirect()->back()->withErrors($aValidate->errors()->all());
            $values =[
                'uploaderID'                =>      $user->userID,
                'lastUpdaterID'             =>      $user->userID,
                'videoID'                   =>      $videoID,
                'language'                  =>      Input::get('language'),
                'srtPath'                   =>      Input::file('srt'),
                'season'                    =>      Input::get('season'),
                'part'                      =>      Input::get('part'),
                'partTrailer'               =>      Input::get('partTrailer')
            ];
            $formValidator = Validator::make($values,SubtitleReady::$rules);
            if($formValidator->fails()) return redirect()->back()->withErrors($formValidator->errors()->all());

            if (!file_exists(storage_path().'/'.$filePath)) mkdir(storage_path().'/'.$filePath, 0777, true);
            Input::file('srt')->move(storage_path().'/'.$filePath,  $name);
            $values['srtPath']=$name;
            $sub = new SubtitleReady();
            $sub->fill($values);
            if($sub->save()){
                $activity = new Activity([
                    'userID'        => $user->userID,
                    'committedID'   => $sub->subtitleReadyID,
                    'activityName'  => 'subtitle_ready_add',
                    'activityDate'  => Carbon::now()]);
                $activity->save();
                return redirect('/requests')
                    ->withErrors(['result'=>'success','message'=>trans('controllerTranslations.subtitle_added_successfully')]);
            }
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function subtitleAdd(Request $request){
        if(Session::has('user')) {
            $subtitleID =   $request['subtitleID'];
            $check      =   SubtitleRows::where(['subtitleID'=>$subtitleID])->first();
            if($check) return 'false';
            $user       =   User::where(['username'=>Session::get('user')])->first();
            if (!$user || $user->memberType=="normalMember") return 'false';
            try{
                $rowNumber=1;
                foreach($request['subtitle'] as $rows){
                    $values         =   [
                        'subtitleID'    =>  $subtitleID,
                        'rowNumber'     =>  $rowNumber,
                        'duration'      =>  strip_tags(addslashes($rows['duration'])),
                        'sentence'      =>  strip_tags(addslashes($rows['sentence']))
                    ];
                    $subtitleRow    =   new SubtitleRows();
                    $subtitleRow->fill($values);
                    $subtitleRow->save();
                    $rowNumber++;
                }
                $subtitle = Subtitle::where(['subtitleID'=>$subtitleID])->first();
                $subtitle->isStarted =1;
                $subtitle->save();
                $activity = new Activity([
                    'userID'        => $user->userID,
                    'committedID'   => $subtitleID,
                    'activityName'  => 'subtitle_rows_created',
                    'activityDate'  => Carbon::now()]);
                $activity->save();
                return 'true';
            }catch (\Exception $e){
                return $e->getMessage();
            }
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function processingSubtitle($subtitleID){
        if(Session::has('user')) {
            $validator = Validator::make(
                ['subtitleId' => $subtitleID],
                ['subtitleId' => 'required|numeric|digits_between:1,10']);
            if ($validator->fails()) {
                return redirect('/');
            } else {
                $subtitleAndVideo = Subtitle::where(['subtitleID'=>$subtitleID])
                    ->join('videoproperties','videoproperties.videoID','=','subtitle.videoID')
                    ->first();
                $user = User::where(['username' => Session::get('user')])->first();
                if (!$user || !$subtitleAndVideo) return redirect()->back()
                    ->withErrors(['result' => 'danger', 'message' => trans('controllerTranslations.please_login')]);

                $check      =   SubtitleRows::where(['subtitleID'=>$subtitleID])->first();

                if(!$check){
                    return redirect('/');
                }else{
                    if($user->memberType=="normalMember" || $user->memberType=="controllerMember") return redirect()->back()
                        ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.you_are_not_a_translator_member')]);
                }

                $subUsers= SubtitleFix::where(['subtitleID'=>$subtitleID])
                    ->join('users','users.userID','=','subtitlefixes.adderID')
                    ->select('users.username', DB::raw('count(*) as total'))
                    ->groupBy('users.username')
                    ->orderBy('total','desc')
                    ->get();
                $subtitleTranslates = DB::table('subtitlefixes')
                    ->join('subtitlerows', 'subtitlerows.rowNumber', '=', 'subtitlefixes.rowNumber')
                    ->join('users', 'users.userID', '=', 'subtitlefixes.adderID')
                    ->where(['subtitlefixes.subtitleID' => $subtitleID])
                    ->where(['subtitlerows.subtitleID' => $subtitleID])
                    ->orderBy('subtitlefixes.rowNumber')
                    ->paginate(25, ['*'], 'page_translated');
                $subtitleRows = DB::table('subtitlerows')
                    ->select('subtitlerows.rowNumber', 'subtitlerows.sentence')
                    ->where(['subtitlerows.subtitleID' => $subtitleID])
                    ->where(['subtitlerows.canUpdate' => 0])
                    ->orderBy('subtitlerows.rowNumber')
                    ->paginate(25, ['*'], 'page_translating');
                return view('backend.subtitle.translatingSubtitle')
                    ->with(['subtitleAndVideo' => $subtitleAndVideo, 'subUsers' => $subUsers, 'subtitleID' => $subtitleID, 'subtitleRows' => $subtitleRows, 'subtitleTranslates' => $subtitleTranslates]);
            }
        }else {
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }

    public function addSubRow(Request $request){
        if(Session::has('user')) {
            $validator = Validator::make(
                ['subtitleId' => $request['subtitleID']],
                ['subtitleId' => 'required|numeric|digits_between:1,10']);
            if ($validator->fails()) return trans('controllerTranslations.not_added');
            $user = User::where(['username' => Session::get('user')])->first();//user varmı
            $subtitle = Subtitle::find($request['subtitleID']);//subtitle varmı
            $checkExists = SubtitleFix::where([
                'subtitleID'    =>  $request['subtitleID'],
                'rowNumber'     =>  $request['row'],
                'newMean'       =>  $request['mean']])->first();//eklenilen anlam daha önce eklenmişmi
            if (!$user || !$subtitle || $checkExists) return trans('controllerTranslations.not_added');

            if($user->memberType=="normalMember" || $user->memberType=="controllerMember" ) return trans('controllerTranslations.you_are_not_a_translator_member');

            $updateRow = SubtitleRows::where(['subtitleID'=>$request['subtitleID'],'rowNumber'=>$request['row']])->first();
            $updateRow->canUpdate=1;//güncellenebilirliği kapa
            $updateRow->save();
            $updateFix = SubtitleFix::where(['subtitleID'=>$request['subtitleID'],'rowNumber'=>$request['row']])->get();
            if(count($updateFix)>0) {
                foreach($updateFix as $upFix){
                    $upFix->bestTranslate=0;
                    $upFix->save();
                }
            }

            $userWorkedToday = SubtitleFix::where([//bugün ekleme yapmadıysa aktiviteye ekle
                'subtitleID'    =>  $request['subtitleID'],
                'adderID'       =>  $user->userID])
                ->where('createTime', '>=', date('Y-m-d').' 00:00:00')
                ->get()
                ->count();

            $isAddedBefore = SubtitleFix::where([
                'subtitleID'    =>  $request['subtitleID'],
                'rowNumber'     =>  $request['row'],
                'adderID'       =>  $user->userID])
                ->get()
                ->count();
            $values=[
                'subtitleID'    =>$request['subtitleID'],
                'rowNumber'     =>$request['row'],
                'newMean'       =>$request['mean'],
                'adderID'       =>$user->userID,
                'createTime'    =>Carbon::now()];
            $rowFixes = new SubtitleFix();
            $rowFixes->fill($values);
            $rowFixes->save();//yeni anlamı veritabanına ekle
            $subtitle->updated_at=Carbon::now();//altyazı en son güncellemeyi güncelle
            $subtitle->lastUpdaterID=$user->userID;//altyazı en son güncelleyen kişi idsini güncelle
            if($userWorkedToday==0){
                $activity = new Activity([
                    'userID'        => $user->userID,
                    'committedID'   => $rowFixes->subtitleFixID,
                    'activityName'  => 'add_subtitle_row',
                    'activityDate'  => Carbon::now()]);
                $activity->save();
            }
            if($isAddedBefore==0){//eğer daha önce aynı altyazıya aynı satır işlemi yapmadıysa kullanıcının çeviri sayısını 1 arttır
                $user->translateCount++;
                if($user->translateCount>=100 && $user->translateCount<1000)
                    $user->position='goodTranslator';
                else if($user->translateCount>=1000 && $user->translateCount<10000)
                    $user->position='bestTranslator';
                else if($user->translateCount>=10000 && $user->translateCount<100000)
                    $user->position='professionelTranslator';
                else if($user->translateCount>=100000 && $user->translateCount<1000000)
                    $user->position='expertTranslator';
                $user->save();
            }
            $rowCount=SubtitleRows::where(['subtitleID'=>$request['subtitleID']])->get()->count();//progress hesaplanma başlangıç
            $translatedRows=SubtitleFix::where(['subtitleID'=>$request['subtitleID'],'bestTranslate'=>1])
                ->select('rowNumber')
                ->distinct()
                ->get()
                ->count();
            $progress=floor(($translatedRows*100)/$rowCount);
            $subtitle->progress=$progress;
            $subtitle->save();
            return 'true';
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function getBackRow(Request $request){
        if(Session::has('user')){
            $user = User::where(['username'=>Session::get('user')])->first();
            if (!$user) return trans('controllerTranslations.please_login');

            if($user->memberType=="normalMember" || $user->memberType=="controllerMember") return trans('controllerTranslations.you_are_not_a_translator_member');

            $subBack= SubtitleRows::where(['rowNumber'=>$request['row'],'subtitleID'=>$request['subtitleID']])->first();
            if($subBack->canUpdate==0) return trans('controllerTranslations.already_taken_back');
            $subBack->canUpdate=0;
            $subBack->save();
            $activity = new Activity([
                'userID'        => $user->userID,
                'committedID'   => $subBack->subtitleRowID,
                'activityName'  => 'get_back_row',
                'activityDate'  => Carbon::now()]);
            $activity->save();
            return 'true';
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function checkFix(Request $request){
        if(Session::has('user')){
            $user = User::where(['username'=>Session::get('user')])->first();

            if (!$user) return trans('controllerTranslations.please_login');

            if($user->memberType=="normalMember" || $user->memberType=="controllerMember") return trans('controllerTranslations.you_are_not_a_translator_member');

            $subFixCheck= SubtitleFix::where([
                'rowNumber' =>  $request['row'],
                'subtitleID'=>  $request['subtitleID']])->first();
            if($subFixCheck) return 'true';
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function lockRow(Request $request){
        if(Session::has('user')){
            try{
                $user = User::where(['username'=>Session::get('user')])->first();

                if (!$user) return trans('controllerTranslations.please_login');

                if($user->memberType=="normalMember" || $user->memberType=="controllerMember") return trans('controllerTranslations.you_are_not_a_translator_member');

                $checkExists = SubtitleFix::where([
                    'subtitleID'    =>  $request['subtitleID'],
                    'rowNumber'     =>  $request['row']])->first();
                if(!$checkExists) return trans('controllerTranslations.please_firstly_add_mean');
                $subBack= SubtitleRows::where(['rowNumber'=>$request['row'],'subtitleID'=>$request['subtitleID']])->first();
                $subBack->canUpdate=1;
                $subBack->save();
                $activity = new Activity([
                    'userID'        => $user->userID,
                    'committedID'   => $subBack->subtitleRowID,
                    'activityName'  => 'lock_row',
                    'activityDate'  => Carbon::now()]);
                $activity->save();
                return 'true';
            }catch(\Exception $e){
                return $e->getMessage();
            }
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function upToBest(Request $request){
        if(Session::has('user')){
            $user = User::where(['username'=>Session::get('user')])->first();

            if (!$user) return trans('controllerTranslations.please_login');

            if($user->memberType=="normalMember" || $user->memberType=="controllerMember") return trans('controllerTranslations.you_are_not_a_translator_member');

            $fixID = $request['FixID'];
            $findFix = SubtitleFix::findOrFail($fixID);
            if(!$findFix) return "";
            $makeGood = SubtitleFix::where(['rowNumber'=>$findFix->rowNumber,'subtitleID'=>$findFix->subtitleID])->get();
            foreach($makeGood as $subs){
                $subs->bestTranslate=0;
                $subs->save();
            }
            $findFix->bestTranslate=1;
            $findFix->save();
            $activity = new Activity([
                'userID'        => $user->userID,
                'committedID'   => $findFix->subtitleFixID,
                'activityName'  => 'up_to_best',
                'activityDate'  => Carbon::now()]);
            $activity->save();
            return "true";
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function supportSubtitlePost(){
        if(Session::has('user')){
            $user = User::where(['username'=>Session::get('user')])->first();

            if (!$user || $user->memberType!="admin") return redirect()->back();

            $values =[
                'fromSrt'           =>  Input::file('fromSrt'),
                'toSrt'             =>  Input::file('toSrt'),
                'fromLanguage'      =>  Input::get('fromLanguage'),
                'toLanguage'        =>  Input::get('toLanguage')
            ];
            $check = [
                'fromSrt'       => 'required|max:1000',
                'toSrt'         => 'required|max:1000',
                'fromLanguage'  => 'required|in:turkish,english,french,spanish,italian,german',
                'toLanguage'    => 'required|in:turkish,english,french,spanish,italian,german|different:fromLanguage'];

            $validate = Validator::make($values,$check);

            if($validate->fails()) return redirect()->back()->withErrors($validate->errors()->all());

            $checkName =[
                'fromSrtEx'     => 'in:srt',
                'toSrtEx'       => 'in:srt',
                'fromSrt'       => 'required',
                'toSrt'         => 'required|different:fromSrt'];

            $validateName = Validator::make([
                'fromSrtEx'     =>  Input::file('fromSrt')->getClientOriginalExtension(),
                'toSrtEx'       =>  Input::file('toSrt')->getClientOriginalExtension(),
                'fromSrt'       =>  Input::file('fromSrt')->getClientOriginalName(),
                'toSrt'         =>  Input::file('toSrt')->getClientOriginalName()
            ],$checkName);

            if($validateName->fails()) return redirect()->back()->withErrors($validateName->errors()->all());

            $filePath   =   'backend/srtDatas/';
            $nameFrom   =   Input::file('fromSrt')->getClientOriginalName().'-'.$values['fromLanguage'].'.srt';
            $nameTo     =   Input::file('toSrt')->getClientOriginalName().'-'.$values['toLanguage'].'.srt';

            if (!file_exists(storage_path().'/'.$filePath)) mkdir(storage_path().'/'.$filePath, 0777, true);

            Functions::asciiToUtf8(File::get(Input::file('fromSrt')),$filePath,$nameFrom);
            Functions::asciiToUtf8(File::get(Input::file('toSrt')),$filePath,$nameTo);
            $values['fromSrt']=$nameFrom;
            $values['toSrt']=$nameTo;
            return view('backend.subtitle.subtitleSupportCheck')->with(['subtitleSupport'=>$values]);
        }else{
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
        }
    }


    public function supportSubtitleGet(){
        if(Session::has('user')){
            $user = User::where(['username'=>Session::get('user')])->first();
            if (!$user or $user->memberType!="admin") return redirect()->back();
            return view('backend.subtitle.subtitleSupportAdd')->with(['title' => trans('pageTranslations.support_subtitle_page')]);
        }
        else
            return redirect()->action('UserController@pageShow', ['page'=>'login','response'=>trans('controllerTranslations.please_login')]);
    }

    public function supportSubtitleAdd(Request $request){
        if(Session::has('user')) {
            $user = User::where(['username'=>Session::get('user')])->first();
            if (!$user || $user->memberType!="admin") return redirect()->back();
            $from       = $request['from'];
            $to         = $request['to'];
            foreach($request['values'] as $means){
                $mean       = strip_tags(addslashes($means['mean']));
                $sentence   = strip_tags(addslashes($means['sentence']));
                $check= SubtitleSupport::where(['sentence'=>$sentence,'mean'=>$mean])
                    ->orWhere(['mean'=>$sentence,'sentence'=>$mean])->first();
                if($check || $mean=='' || $sentence=='') continue;
                $support = new SubtitleSupport();
                $support->fill([
                    'fromLanguage'  =>  $from,
                    'toLanguage'    =>  $to,
                    'sentence'      =>  $sentence,
                    'mean'          =>  $mean,
                    'createTime'    =>  Carbon::now()]);
                $support->save();
            }
            return 'true';
        }else {
            return trans('controllerTranslations.please_login');
        }
    }

    public function getMean(Request $request){
        if(Session::has('user')) {
            $from = $request['from'];
            $to = $request['to'];
            $sups = SubtitleSupport::where(['fromLanguage'=>$from,'toLanguage'=>$to])
                ->orWhere(['fromLanguage'=>$to,'toLanguage'=>$from])
                //->select(stripslashes(DB::raw('sentence')),stripslashes(DB::raw('mean')))
                ->select('sentence','mean')
                ->orderBy('sentence')
                ->orderBy('mean')
                ->distinct()
                ->get();
            return $sups;
        }else {
            return trans('controllerTranslations.please_login');
        }
    }
    public function yandexTranslate(Request $request){
        try {
            $key ="trnsl.1.1.20161020T074041Z.d0fc72a618c51a0a.ee86ae777fb38396d3d84dfd9ad3caad13441f2c";
            $values = [
                'turkish'   =>  'tr',
                'english'   =>  'en',
                'german'    =>  'de',
                'french'    =>  'fr',
                'italian'   =>  'it',
                'spanish'   =>  'es'];
            $to         = $values[$request['to']];
            $from       = $values[$request['from']];
            $sentence   = $request['text'];
            $translator = new Translator($key);
            $yandexTranslation = $translator->translate($sentence, $from.'-'.$to);
            return $yandexTranslation;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function googleTranslate(Request $request){
        try {
            $values = [
                'turkish'   =>  'tr',
                'english'   =>  'en',
                'german'    =>  'de',
                'french'    =>  'fr',
                'italian'   =>  'it',
                'spanish'   =>  'es'];
            $to         = $values[$request['to']];
            $from       = $values[$request['from']];
            $sentence   = $request['text'];
            $tr = new TranslateClient($from,$to);
            $googleTranslation = $tr->translate($sentence);
            return $googleTranslation;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function finishSubtitle($subtitleID){
        $selectFixes=DB::table('subtitlerows')
            ->join('subtitlefixes','subtitlefixes.rowNumber','=','subtitlerows.rowNumber')
            ->select('subtitlefixes.rowNumber','subtitlerows.duration','subtitlefixes.newMean')
            ->where(['subtitlerows.subtitleID'=>$subtitleID])
            ->where(['subtitlefixes.subtitleID'=>$subtitleID])
            ->where(['subtitlefixes.bestTranslate'=>1])
            ->orderBy('subtitlerows.rowNumber')
            ->get();
        $subtitle = Subtitle::find($subtitleID);
        $video = DB::table('subtitle')
            ->join('videoproperties','videoproperties.videoID','=','subtitle.videoID')
            ->where(['subtitle.subtitleID'=>$subtitleID])
            ->select('videoproperties.name','videoproperties.videoID')
            ->first();
        if(!$subtitle || !$video) return redirect()->back();

        $filePath   =   storage_path().'/backend/translatedSrt/'.$subtitle->toLanguage."-hizlialtyazi-".$subtitle->translatingSrtPath;
        $file = fopen($filePath, "w");
        $subtitle->translatedSrtPath=$subtitle->toLanguage."-hizlialtyazi-".$subtitle->translatingSrtPath;
        $subtitle->save();
        foreach($selectFixes as $rows){
            file_put_contents($filePath, $rows->rowNumber."\n", FILE_APPEND | LOCK_EX);
            file_put_contents($filePath, $rows->duration."\n", FILE_APPEND | LOCK_EX);
            file_put_contents($filePath, $rows->newMean."\n\n", FILE_APPEND | LOCK_EX);
        }
        fclose($file);

        $mailToFollowers = DB::table('videofollowers')
            ->where(['videoID'=>$subtitle->videoID])
            ->join('users','users.userID','=','videofollowers.followerID')
            ->select('users.username','users.email')->get();
        $videoLink = 'video/'.\App\Functions::beGoodSeo(stripslashes($video->name)).'/'.$video->videoID;
        $comment="";
        if($subtitle->season){
            $comment = trans('pageTranslations.season').' : '.$subtitle->season.' / '.trans('pageTranslations.part').' : '.$subtitle->part;
        }

        $comment.="\n".$subtitle->translatedSrtPath;
        foreach($mailToFollowers as $user){
            Mail::queue('backend.user.followerInform',[
                'username'  =>  $user->username,
                'comment'   =>  $comment,
                'videoLink' =>  $videoLink
            ],function($message) use ($user,$video){
                $message->from("noreply@fastsubtitle.com",trans('pageTranslations.domain'))->to($user->email,$user->username)->subject(trans('controllerTranslations.new_subtitle_translate_is_complated').' - '.$video->name);
            });
        }
        return redirect('/video/' . Functions::beGoodSeo($video->name) . '/' . $video->videoID);
    }

    public function downloadSubtitle($crypt){
        if(Crypt::decrypt($crypt)) {
            $return = Crypt::decrypt($crypt);
            $subtitleExp = explode('@',$return);
            $subtitle = SubtitleReady::find($subtitleExp[1]);
            if($subtitle){
                if($subtitleExp[0]=="srts"){
                    $file=storage_path('backend/'.$subtitleExp[0].'/'.$subtitle->srtPath);
                    $subtitle->downloads++;
                    $subtitle->save();
                    return Response::download($file);
                }
            }else{
                return redirect('/')
                    ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.ready_subtitle_now_found')]);
            }
        }else{
            return redirect('/')
                ->withErrors(['result'=>'danger','message'=>trans('controllerTranslations.decrypt_not_correct')]);
        }

    }
}
