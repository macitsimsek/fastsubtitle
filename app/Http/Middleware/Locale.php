<?php namespace App\Http\Middleware;

use Closure;
use Jenssegers\Date\Date;
use Session;
use App;

class Locale {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if($request->server("HTTP_HOST")=="fastsubtitle.com"){
            \App::setLocale('en');
            Date::setLocale('en');
        }else if(Session::has('locale')){
            \App::setLocale(Session::get('locale'));
            Date::setLocale(Session::get('locale'));
        }
        return $next($request);
    }

}