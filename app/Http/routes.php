<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Http\Requests;

/*
    |--------------------------------------------------------------------------
    | PagesController Fonksiyonları
    |--------------------------------------------------------------------------
    |
    |
    |
    */
Route::group(['middleware' => ['web']], function () {
    Route::auth();


    //index sayfası
    Route::get('/', 'PagesController@index');

    //login sayfası

    Route::get('/users', 'PagesController@users');

    //login sayfası

    Route::get('/lang/{language}', 'PagesController@setLanguage');

});

/*
    |--------------------------------------------------------------------------
    | UserController Fonksiyonları
    |--------------------------------------------------------------------------
    |
    |
    |
    */

Route::group(['middleware' => ['web']], function () {

    Route::get('/login', 'UserController@login');

    Route::post('/userlogin', 'UserController@userLogin');

    Route::post('/createuser', 'UserController@createUser');

    Route::post('/newpass', 'UserController@newPass');

    Route::post('/forgotpass', 'UserController@forgotPass');

    Route::post('/changepass', 'UserController@changePass');

    Route::get('/exit', 'UserController@userExit');

    Route::get('/vl{crypt}', 'UserController@result');

    Route::get('/user/{page}/{response?}', 'UserController@pageShow');

    Route::get('/users/profile/{username?}', 'UserController@profile');

    Route::get('/users', 'UserController@allProfiles');

    Route::post('/send-friend-request/{cryptedstring}', ['as' => 'send.friend.request', 'uses' => 'UserController@sendFriendRequest']);

    Route::post('/approve-friend-request/{cryptedstring}', 'UserController@approveFriendRequest');

    Route::get('/users/approve/{userID}', 'UserController@approveUpgradeUser');

    Route::get('/show/subtitle/{userID}', 'UserController@showExampleSubtitle');

    Route::get('/upgradeaccount', 'UserController@upgradeAccount');

    Route::post('/user/upgrade', ['as' => 'upgrade.account', 'uses' => 'UserController@upgradeAccountPost']);

    Route::get('/users/showupgraderequests', 'UserController@showUpgradeRequests');

    Route::get('/users/messages/{token?}', 'UserController@messages');

    Route::post('/users/messages/{token}', ['as' => 'send.message', 'uses' => 'UserController@sendMessage']);


});

/*
    |--------------------------------------------------------------------------
    | DenemeController Fonksiyonları
    |--------------------------------------------------------------------------
    |
    |
    |
*/

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    Route::get('/deneme', 'DenemeController@deneme');

    Route::get('/deneme2', 'DenemeController@deneme2');

    Route::post('/translate2', 'DenemeController@translate');
});


/*
    |--------------------------------------------------------------------------
    | VideoController Fonksiyonları
    |--------------------------------------------------------------------------
    |
    |
    |
*/
//video apis
Route::group(['middleware' => ['api']], function () {
    Route::post('/api-imdb',        'VideoController@getIMDB');
    Route::post('/api-imdb-pic',    'VideoController@getIMDBPictures');
    Route::post('/api-wiki-pic',    'VideoController@getWikiwithPictures');
    Route::post('/api-wiki-only-pic',   'VideoController@getOnlyWikiPictures');
    Route::post('/api-wiki',            'VideoController@getWiki');
    Route::post('/api-imdb-search-with-query',  'VideoController@searchIMDB');
    Route::post('/api-imdb-search-with-id',     'VideoController@searchIMDBwithID');
});

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    Route::get('/search/video', 'VideoController@videoSearch');

    Route::get('/addvideo', 'VideoController@videoFind');

    Route::post('/getfromha', 'VideoController@getFromFS');

    Route::post('/getfromimdb', 'VideoController@getFromImdb');

    Route::post('/getpagehtml', 'VideoController@getPageHtml');

    Route::post('/searchwiki', 'VideoController@getWiki');

    Route::post('/searchwikipictures', 'VideoController@getWikiwithPictures');

    Route::post('/gettrailer', 'VideoController@getTrailer');

    Route::post('/addvideo', 'VideoController@addVideo');

    Route::get('/video/{videoName}/{videoID?}', 'VideoController@showVideo');

    Route::get('/video/{videoName}/{videoID}/follow', 'VideoController@videoFollow');

    Route::get('/video/{videoName}/{videoID}/edit', 'VideoController@videoEdit');

    Route::patch('/videos/{videoID}/update', ['as' => 'videos.update', 'uses' => 'VideoController@update']);

    Route::post('/comment-add-{videoID}', ['as' => 'comment.add', 'uses' => 'VideoController@commentAdd']);

});


/*
    |--------------------------------------------------------------------------
    | PeopleController Fonksiyonları
    |--------------------------------------------------------------------------
    |
    |
    |
*/

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    Route::get('/people/{personName}/{personID}/edit', 'PeopleController@peopleEdit');

    Route::get('/people/{personName}/{personID?}', 'PeopleController@showPeople');

    Route::patch('/person/{personID}/update', ['as' => 'person.update', 'uses' => 'PeopleController@update']);

});


/*
    |--------------------------------------------------------------------------
    | SubtitleController Fonksiyonları
    |--------------------------------------------------------------------------
    |
    |
    |
*/

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    Route::get('/video/{videoName}/{videoID}/request-subtitle', 'SubtitleController@request');

    Route::post('/request-add-{videoID}', ['as' => 'request.add', 'uses' => 'SubtitleController@create']);

    Route::get('/requests/{order?}', 'SubtitleController@showSubtitles');

    Route::get('/request/{subtitleID}', 'SubtitleController@requestPlus');

    Route::get('/subtitle/check/{subtitleID}', 'SubtitleController@checkSubtitle');

    //Route::post('/subtitleadd','SubtitleController@subtitleAdd');

    Route::post('/subtitleadd', ['as' => 'sub.add', 'uses' => 'SubtitleController@subtitleAdd']);

    Route::post('/addsubrow', 'SubtitleController@addSubRow');

    Route::get('/subtitle/processing/{subtitleID}', 'SubtitleController@processingSubtitle');

    Route::post('/post/support/subtitle', ['as' => 'subsupport.check', 'uses' => 'SubtitleController@supportSubtitlePost']);

    Route::post('/post/support/subtitle/add', ['as' => 'subsupport.add', 'uses' => 'SubtitleController@supportSubtitleAdd']);

    Route::get('/support/subtitle', 'SubtitleController@supportSubtitleGet');

    Route::post('/getmean', 'SubtitleController@getMean');

    Route::get('/getmean2', 'SubtitleController@getMean');

    Route::post('/getbackrow', 'SubtitleController@getBackRow');

    Route::post('/lockrow', 'SubtitleController@lockRow');

    Route::post('/uptobest', 'SubtitleController@upToBest');

    Route::post('/checkexistsrow', 'SubtitleController@checkFix');

    Route::post('/gtranslate', 'SubtitleController@googleTranslate');

    Route::post('/ytranslate', 'SubtitleController@yandexTranslate');

    Route::get('/subtitle/finish/{subtitleID}', 'SubtitleController@finishSubtitle');

    Route::get('/subtitle/download/{crypt}', 'SubtitleController@downloadSubtitle');

});


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
