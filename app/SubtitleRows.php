<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubtitleRows extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subtitlerows';//Tablo adı

    protected $primaryKey = 'subtitleRowID';

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subtitleID',   //subtitleID'si
        'rowNumber',   //Video Id'si
        'duration',   //duration
        'sentence'];


    public static $rules = array(
        'subtitleID'    => 'required|numeric|digits_between:1,10',
        'rowNumber'     => 'required|numeric|digits_between:1,4',
        'duration'      => 'required|max:50',
        'sentence'      => 'required|max:255'
    );
}
