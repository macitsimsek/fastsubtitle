<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class Message extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';//Tablo adı

    protected $primaryKey = 'messageID';

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'senderID',     //Gönderici User Id'si
        'recieverID',   //Alıcı User Id'si
        'subject',      //Mesaj Başlığı
        'message'];     //Mesaj

    public static $rules = array(
        'senderID'          => 'required|numeric|max:10',
        'recieverID'        => 'required|numeric|max:10',
        'subject'           => 'required|alpha_dash|max:255',
        'message'           => 'required|alpha_dash'
    );
}
