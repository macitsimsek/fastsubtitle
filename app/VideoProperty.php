<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class VideoProperty extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videoproperties';

    protected $primaryKey = 'videoID';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'type',
                            'name',
                            'turkishName',
                            'kind',
                            'imdbPoint',
                            'duration',
                            'country',
                            'dateVision',
                            'abstract',
                            'trailer',
                            'picture'];

    public static $rules = array(
        'type'          => 'required|in:serie,movie,anime',
        'name'          => 'required|max:255',
        'turkishName'   => 'required|max:255',
        'kind'          => 'required|max:255',
        'imdbPoint'     => 'required|max:10',
        'duration'      => 'required|numeric|digits_between:1,10',
        'country'       => 'required|max:255',
        'dateVision'    => array('required', 'date_format:"Y"',"digits:4"),
        'abstract'      => 'required',
        'trailer'       => 'required|url',
        'picture'       => 'required|url'
    );
}
