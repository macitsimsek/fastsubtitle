<?php

namespace App;

use Illuminate\Support\Facades\Crypt;
use Intervention\Image\Facades\Image;

class Functions {
    public static $videoImagePath   = 'backend/videoImages/';
    public static $profileImagePath = 'backend/img/profile-pics/';
    public static $peopleImagePath  = 'backend/peopleImages/';
    public static $website          = 'https://www.fastsubtitle.com/';

    public static function addSlashAndRemoveTags($string){
        return addslashes(strip_tags($string));
    }

    public static function beGoodSeo($url){
        $url = trim($url);
        $url = str_replace("ö","o",$url);
        $url = str_replace("ç","c",$url);
        $url = str_replace("ş","s",$url);
        $url = str_replace("ı","i",$url);
        $url = str_replace("ğ","g",$url);
        $url = str_replace("ü","u",$url);
        $url = str_replace("Ö","O",$url);
        $url = str_replace("Ç","C",$url);
        $url = str_replace("Ş","S",$url);
        $url = str_replace("İ","I",$url);
        $url = str_replace("Ğ","G",$url);
        $url = str_replace("Ü","U",$url);
        $url = preg_replace("/[^a-zA-Z0-9]+/", "_", $url);
        return $url;
    }

    public static function downloadPicture($uzaktaki_dosya, $kayit_ismi, $yol) {
        if($uzaktaki_dosya=='backend/peopleImages/empty.jpg') return 'backend/peopleImages/empty.jpg';

        if (!file_exists(public_path().'/backend/'.$yol)) mkdir(public_path().'/backend/'.$yol, 0777, true);

        $kayit_ismi =	preg_replace('~[^a-zA-Z0-9]+~', '-', $kayit_ismi);
        $kayit_ismi	=	str_replace(" ", "_", $kayit_ismi);
        $img        =   Image::make($uzaktaki_dosya)->resize(175,250);
        $kayit_ismi	.=	".jpg";
        $yeni_dizin	=	"backend/".$yol."/".$kayit_ismi;

        $img->save(public_path().'/'.$yeni_dizin);
        return $kayit_ismi;
    }

    public static function asciiToUtf8($text,$filePath,$nameFrom){

        $text = str_replace("\xEF\xBB\xBF",'',$text);
        $text =mb_convert_encoding($text, 'UTF-8', 'ASCII');
        $text =str_replace('Þ','Ş',$text);
        $text =str_replace('Ð','Ğ',$text);
        $text =str_replace('Ý','İ',$text);
        $text =str_replace('þ','ş',$text);
        $text =str_replace('ð','ğ',$text);
        $text =str_replace('ý','ı',$text);
        file_put_contents(storage_path($filePath.$nameFrom), $text);
        return $text;
    }

    public static function cryptSubtitleAdress($string){
        $string=Crypt::encrypt($string);
        return $string;
    }

    public static function url($path){
        return self::$website.$path;
    }

    public static function makeVideoUrl($name,$id){
        return self::$website.'video/'.self::beGoodSeo(stripslashes($name)).'/'.$id;
    }

    public static function makeEditVideoUrl($name,$id){
        return self::$website.'video/'.self::beGoodSeo(stripslashes($name)).'/'.$id.'/edit';
    }

    public static function makeRequestSubtitleUrl($name,$id){
        return self::$website.'video/'.self::beGoodSeo(stripslashes($name)).'/'.$id.'/request-subtitle';
    }

    public static function makeSubtitleCheckUrl($id){
        return self::$website.'subtitle/check/'.$id;
    }

    public static function makeProcessingSubtitleUrl($id){
        return self::$website.'subtitle/processing/'.$id;
    }

    public static function makePersonUrl($name,$id){
        return self::$website.'people/'.self::beGoodSeo(stripslashes($name)).'/'.$id;
    }

    public static function makeEditPersonUrl($name,$id){
        return self::$website.'people/'.self::beGoodSeo(stripslashes($name)).'/'.$id.'/edit';
    }
}