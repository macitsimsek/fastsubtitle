<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class SubtitleFix extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subtitlefixes';

    protected $primaryKey = 'subtitleFixID';

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subtitleID',
        'rowNumber',
        'newMean',
        'adderID',
        'createTime'];

    public static $rules = array(
        'subtitleID'    => 'required|numeric|max:10',
        'rowNumber'     => 'required|numeric|max:10',
        'newMean'       => 'required|alpha_dash',
        'adderID'       => 'required|numeric|max:10'
    );
}
