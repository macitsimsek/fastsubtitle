<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubtitleRequester extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subtitlerequesters';//Tablo adı

    protected $primaryKey = 'requestID';

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subtitleID',   //Video Id'si
        'userID'];      //User Id'si


    public static $rules = array(
        'subtitleID'    => 'required|numeric|digits_between:1,10',
        'userID'        => 'required|numeric|digits_between:1,10'
    );
}
