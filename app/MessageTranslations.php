<?php

namespace App;

class MessageTranslations {

    public static $messageTr = array(
        'required'      => ':attribute alanını boş bırakmayın.',
        'same'          => ':attribute uyuşmuyor.',
        'unique'        => ':attribute kullanılıyor.',
        'email'         => ':attribute geçersiz.',
        'alpha_dash'    => ':attribute sadece harf sayı ve tire içerebilir.',
        'min'           => ':attribute :min en az karakter olabilir.',
        'max'           => ':attribute :max en fazla karakter olabilir.',
        'different'     => ':attribute alanı farklı olmalıdır.',
        'exists'        => ':attribute bulunamadı.',
        'alpha_num'     => ':attribute sadece sayı olabilir.',
        'active_url'    => ':attribute geçerli bir url değil.',
        'url'           => ':attribute geçersiz.');
}