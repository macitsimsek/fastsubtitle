<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Activity extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activities';//Tablo adı

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'activityID';

    protected $fillable = [
        'userID',           //User Id'si
        'committedID',      //Yapılan Aktivite Id'si
        'activityName',     //Aktivite İsmi
        'activityDate'];    //Aktivite Zamanı


    public static $rules = array(
        'userID'        => 'required|numeric|max:10',
        'committedID'   => 'required|numeric|max:10',
        'activityName'  => 'required|alpha_dash|max:255'
    );



}
