<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubtitleReady extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subtitleready';//Tablo adı

    protected $primaryKey = 'subtitleReadyID';

    public $timestamps = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uploaderID', //User Id'si
        'videoID',//Video Id'si
        'srtPath',
        'language',
        'season',
        'part',
        'partTrailer'
    ];


    public static $rules = array(
        'uploaderID'        => 'required|numeric|digits_between:1,10',
        'videoID'           => 'required|numeric|digits_between:1,10',
        'srtPath'           => 'required|max:1000',
        'language'          => 'required|in:turkish,english,french,spanish,italian,german',
        'season'            => 'nullable|numeric|digits_between:1,2',
        'part'              => 'nullable|numeric|digits_between:1,2',
        'partTrailer'       => 'url|nullable'
    );
}
