<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class Subtitle extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subtitle';

    protected $primaryKey = 'subtitleID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requesterID',
        'translatedSrtPath',
        'lastUpdaterID',
        'progress',
        'requestCount',
        'problem',
        'translatedDownloads',
        'isStarted',
        'toLanguage'];

    public static $rules = array(
        'requesterID'            => 'required|numeric|digits_between:1,10',
        'translatedSrtPath'     => 'max:1000',
        'lastUpdaterID'         => 'required|numeric|digits_between:1,10',
        'toLanguage'            => 'required|in:turkish,english,french,spanish,italian,german'
    );
}