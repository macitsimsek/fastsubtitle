<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class Dictionary extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dictionary';//Tablo adı

    protected $primaryKey = 'wordID';

    public $timestamp = false;//timestamp yoksa tabloda false

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'word',     //yabancı kelime
        'mean',     //kelime anlamı
        'fromLanguage',     //Hangi dilden
        'toLanguage',     //hangi dile
        'adderID'];  //User Id'si


    public static $rules = array(
        'word'      => 'required|alpha_dash|max:50', //
        'mean'      => 'required|alpha_dash|max:50', //
        'fromLanguage'          => 'required|in:turkish,english,french,spanish,italian,german',
        'toLanguage'            => 'required|in:turkish,english,french,spanish,italian,german|different:fromLanguage',
        'adderID'   => 'required|alpha_dash|max:10'
    );

}
